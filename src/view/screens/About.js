import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import banner from "../assets/page/about-banner.jpg";
import valeur from "../assets/page/valeur.jpg";
import innovationPicto from "../assets/innovation.png";
// import 'App.css';

const About = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>A propos de nous</h1>
                    </div>
                </div>
                <img alt=" banner" className="agencyAvatar" src={banner} />
            </div>
            <div className='container-fluid' id ="our-valurs">
                <div className='m-container'>
                    <div className='row reverse-mobile'>
                        <div className='col-lg-12'>
                            <h2 className='sectionTitle meduim'> 
                                
                                <span> Qui sommes Nous ?</span>
                            </h2>
                            <div>
                                <p>
                                    Abidjan Habitat est une entreprise privée opérant dans le secteur de l’immobilier en Côte d’ivoire. 
                                    Nous sommes spécialisés dans ce domaine en tant que prestataire de service avec un accent particulier sur la formation et 
                                    la mise en service d’agents (es) immobilier professionnels(les) de premier choix pour vous servir et vous apporter des résultats concrets et satisfaisants. 
                                    Nous opérons aussi dans le secteur en tant qu’investisseur, gestionnaire, constructeur et promoteur de biens immobiliers. <br/>
                                    <br/>  
                                    Avec Abidjan Habitat Académie, nous nous sommes donnés pour mission de transformer le secteur service de immobilier en développant et en délivrant un cursus exclusif à l’industrie qui forme nos agents immobiliers avec une qualification professionnelle qui vous apporte la confiance, 
                                    le professionnalisme et un savoir-faire dans le conseil pour satisfaire tous vos besoins de location, vente, investissement et gestion immobilière.
                                </p>
                                <p className="italic_text">
                                        Toujours à vos côtés pour vous accompagner durant cette période importante de votre vie.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-fluid padding'>
                <div className='contact-linkbox'>
                    <p>Contactez nous pour plus d'informations sur nos différentes solutions</p>
                    <Link to={'/contact'} className='link_text'> 
                        <button className='link_text'> 
                            Contactez-Nous
                        </button>
                    </Link>
                </div>
            </div>

            {/* <div className='container-fluid ' id="our-values">
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-4 col-md-6 col-sm-12'>
                            <div className='office-image mt-5'>
                                <img  src={banner} className="img-fluid img-left"/>
                            </div>
                        </div>
                        <div className='col-lg-8 col-md-6 col-sm-12'>
                            <div className='office-image top-image'>
                                <img  src={valeur} className="img-fluid img-rigth"/>
                            </div>

                            <h2 className='sectionTitle meduim margin-bottom mt-3' > 
                                Nos valeurs
                            </h2>
                            <div className='row'>
                                <div className='col-lg-6'>
                                    <div className="">
                                        <h4 className="values_card_title">Innovation</h4>
                                        <p>
                                            Pourquoi être ordinaire quand nous avons la possibilité d'être extra-ordinaire?
                                            Nous voulons être pragmatiques non conventionnels mais pertinents en termes de design, de technologie et d'expérience humaine.

                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Qualité</h4>
                                        <p>
                                            La norme que nous établissons se traduit par les attentes que nous avons pour 
                                            le type de services que nous offrons et les propriétés sous notre direction.
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className="">
                                        <h4 className="values_card_title">Leadership</h4>
                                        <p>
                                            Le leadership par l'autonomisation est au cœur de nos efforts et l'executions de notre travail.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Intégrité</h4>
                                        <p>
                                            C'est là que le message «Habitat pour tous» prend forme et est le fer de lance de nos actions..
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                   <div className="">
                                        <h4 className="values_card_title">Collaboration</h4>
                                        <p>
                                            Il n'y a pas de problème sans solution parfaitement adaptée.
                                            Nous sommes axés sur les solutions et nous 
                                            les encourageons  par les échanges d'idées et de compétences.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Responsabilité</h4>
                                        <p>
                                            Ce que nous faisons aujourd'hui compte pour demain.
                                            Nous soutenons notre travail avec des garanties qui vont au delà des normes.
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-6'>
                                    <div className="">
                                        <h4 className="values_card_title">Transparence</h4>
                                        <p>
                                        Nos intentions sont clairement énoncées et elles ne sont égalées que par notre engagement et 
                                        notre détermination à voir le travail accompli avec une vision à long terme.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Croissance</h4>
                                        <p>
                                            Nous voulons que notre croissance soit mesurée par les vies 
                                            que nous améliorons avec nos prestations de services.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> */}
        </div>
    )
}

export default About;