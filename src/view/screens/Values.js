import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import banner from "../assets/page/nos-valeurs.png";

// import 'App.css';

const Values = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Nos valeurs</h1>
                    </div>
                </div>
                <img alt=" banner" className="agencyAvatar" src={banner} />
            </div>

         <div className='container-fluid ' id="our-values">
                <div className='m-container'>
                    <div className='row'>
                        {/* <div className='col-lg-4 col-md-6 col-sm-12'>
                            <div className='office-image mt-5'>
                                <img  src={banner} className="img-fluid img-left"/>
                            </div>
                        </div> */}
                        <div className='col-sm-12'>
                            {/* <div className='office-image top-image'>
                                <img  src={valeur} className="img-fluid img-rigth"/>
                            </div> */}
                            <div className='row'>
                                <div className='col-lg-3'>
                                    <div className="">
                                        <h4 className="values_card_title">Innovation</h4>
                                        <p>
                                            Pourquoi être ordinaire quand nous avons la possibilité d'être extra-ordinaire?
                                            Nous voulons être pragmatiques non conventionnels mais pertinents en termes de design, de technologie et d'expérience humaine.

                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Qualité</h4>
                                        <p>
                                            La norme que nous établissons se traduit par les attentes que nous avons pour 
                                            le type de services que nous offrons et les propriétés sous notre direction.
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-3'>
                                    <div className="">
                                        <h4 className="values_card_title">Leadership</h4>
                                        <p>
                                            Le leadership par l'autonomisation est au cœur de nos efforts et l'executions de notre travail.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Intégrité</h4>
                                        <p>
                                            C'est là que le message «Habitat pour tous» prend forme et est le fer de lance de nos actions..
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-3'>
                                   <div className="">
                                        <h4 className="values_card_title">Collaboration</h4>
                                        <p>
                                            Il n'y a pas de problème sans solution parfaitement adaptée.
                                            Nous sommes axés sur les solutions et nous 
                                            les encourageons  par les échanges d'idées et de compétences.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Responsabilité</h4>
                                        <p>
                                            Ce que nous faisons aujourd'hui compte pour demain.
                                            Nous soutenons notre travail avec des garanties qui vont au delà des normes.
                                        </p>
                                    </div>
                                </div>
                                <div className='col-lg-3'>
                                    <div className="">
                                        <h4 className="values_card_title">Transparence</h4>
                                        <p>
                                        Nos intentions sont clairement énoncées et elles ne sont égalées que par notre engagement et 
                                        notre détermination à voir le travail accompli avec une vision à long terme.
                                        </p>
                                    </div>
                                    <div className="">
                                        <h4 className="values_card_title">Croissance</h4>
                                        <p>
                                            Nous voulons que notre croissance soit mesurée par les vies 
                                            que nous améliorons avec nos prestations de services.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> */
            
            <div className='container-fluid padding'>
                <div className='contact-linkbox'>
                    <p>Contactez nous pour plus d'informations sur nos différentes solutions</p>
                    <Link to={'/contact'} className='link_text'> 
                        <button className='link_text'> 
                            Contactez-Nous
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Values;