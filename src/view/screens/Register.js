import '../css/login.css';
import {
    HttpsOutlined,
    MailOutline,
    PersonOutline,
    VisibilityOffOutlined,
    VisibilityOutlined
} from "@material-ui/icons";
import {Link} from "react-router-dom";
import googleLogo from "../assets/google.png";
import UserService from "../../services/api/UserService";
import {profileColors} from "../constants/color";
import {useState} from "react";
import loader from "../assets/loader/785.svg";
import GoogleLogin from "react-google-login"; 
import FacebookLogin from 'react-facebook-login';
import {useDispatch} from "react-redux";
import {login, pushToken} from "../../core/reducer/authSlice";
import LogoImage from "../assets/logo/logo2.png";

const Register = () => {

    document.title = 'Créer mon compte - Vente et Location de bien immobilier à abidjan';

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const [emailExist, setEmailExist] = useState(false);
    const [load, setLoad] = useState(false);
    const [showPass, setShowPass] = useState(false);

    const userService = new UserService();

    let emailPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    const colorNum = Math.round(Math.random() * profileColors.length);

    const findEmail = (mail) => {
        userService.emailIsExist(mail).then((data) => {
            if(data['hydra:totalItems'] > 0){
                setMessage('Cet email est déjà associer à un compte');
                setEmailExist(true);
            }else{
                setEmailExist(false);
                setMessage('');
            }
        });
    };

    const dispatch = useDispatch();

    const registerUser = () => {
        if([name, email, password].every(x => x.length > 0)){
            if(emailPattern.test(email)){
                if(!emailExist){
                    setMessage('');
                    setLoad(true);
                    endRegister({
                        name: name,
                        email: email,
                        username: email,
                        password: password,
                        color: profileColors[colorNum],
                        isEnabled: true
                    });
                }else{
                    setLoad(false);
                    setMessage('Cet email est déjà associer à un compte');
                }
            }else{
                setLoad(false);
                setMessage('Entrer un email valide');
            }
        }else{
            setLoad(false);
            setMessage('Veillez à bien remplir le formulaire');
        }
    }

    const endRegister = (infos) => {
        userService.emailIsExist(infos.email).then((data) => {
            if(data['hydra:totalItems'] > 0){
                setMessage('Cet email est déjà associer à un compte');
                setEmailExist(true);
                setLoad(false);
            }else{
                userService.register(infos).then(data => {
                    if(data.status === 201){
                        connectUser(infos.username, infos.password);
                    }else {
                        setLoad(false);
                        setMessage('Une erreur s\'est produite, verifiez les informations');
                    }
                }).catch((error) => {
                    setLoad(false);
                });
            }
        });
    }

    const proceedRegisterWithGoogle = async (infos) => {
        setLoad(true);
        userService.emailIsExist(infos.profileObj.email).then((data) => {
            if(data['hydra:totalItems'] > 0){
                connectUser(infos.profileObj.email, infos.profileObj.googleId);
            }else{
                endRegister({
                    name: infos.profileObj.name,
                    email: infos.profileObj.email,
                    username: infos.profileObj.email,
                    password: infos.profileObj.googleId,
                    provider: "google",
                    color: profileColors[colorNum],
                    isEnabled: true
                });
            }
        });
    }

    const connectUser = (username, password) => {
        userService.connect(username, password).then(data => {
            if(!data){
                setMessage('Une erreur s\'est produite, verifiez les informations');
                setLoad(false);
            }else{
                localStorage.setItem('personnal', data.token);
                localStorage.setItem('personnal_refresh', data.refresh_token)
                userService.Provide(data.token).then(data => {
                    dispatch(login(data));
                    dispatch(pushToken({token: data.token, revalidator: data.refresh_token}));
                });
            }
        });
    }

    const responseFacebook = (response) => {
        setLoad(true);
        userService.emailIsExist(response.userID).then((data) => {
            if(data['hydra:totalItems'] > 0){
                connectUser(response.userID, response.userID);
            }else{
                endRegister({
                    name: response.name,
                    email: response.email ? response.email : ' ',
                    username: response.userID,
                    password: response.userID,
                    provider: "facebook",
                    color: profileColors[colorNum],
                    isEnabled: true
                });
            }
        });
    }

    return(
        <div className={"login-container"}>
            <div className={"split image"}>
                <Link to={""} className={"logo"}>
                    <img src='https://imapi.me/assets/logo/logo2.png' />
                </Link>
            </div>
            <div className={"split form"}>
                <Link to={""} className={"logo mobile"}><img alt=" banner" className="" src={LogoImage} /></Link>
                <div className={"fields"}>
                    <div className={"title"}>
                        S'inscrire
                        <span className={"line"} />
                    </div>
                    <div className={'message'}>
                        {message}
                    </div>
                    <div className={"box"}>
                        <PersonOutline />
                        <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} name={"nom"} placeholder={"Nom et Prénoms"} />
                    </div>
                    <div className={"box"}>
                        <MailOutline />
                        <input onBlur={findEmail} onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"email"} name={"email"} placeholder={"Email"} />
                    </div>
                    <div className={"box"}>
                        <HttpsOutlined/>
                        <input onChange={(e) => setPassword(e.currentTarget.value)} value={password} name={"password"} type={showPass ? "text" : "password"} placeholder={"Mot de passe"} />
                        {
                            showPass ?
                                <VisibilityOffOutlined onClick={() => setShowPass(!showPass)} className={'eye'} />
                            :
                                <VisibilityOutlined onClick={() => setShowPass(!showPass)} className={'eye'} />
                        }
                    </div>
                    <div>
                        En cliquant sur le bouton ci-dessous vous acceptez les <Link to={"/users-conditions"}>conditions d'utilisation</Link>
                    </div>
                    <br />
                    <button onClick={registerUser} className={"submit"}>
                        { load ? <img src={loader} alt={"Veillez patienter"} /> : '' }
                        Créer mon compte
                    </button>
                    {/*
                    <div className={"or"}> Ou </div>
                    <div className={"social"}>
                        <GoogleLogin
                            clientId={process.env.REACT_APP_GOOGLE_LOGIN_CLIENTID}
                            buttonText={"S'inscrire avec google"}
                            cookiePolicy={"single_host_origin"}
                            render={renderProps =>
                                <div onClick={renderProps.onClick} disabled={renderProps.disabled} className={"wide"}>
                                    <img src={googleLogo} alt={"google"} />
                                    S'inscrire avec google
                                </div>
                            }
                            onSuccess={proceedRegisterWithGoogle}
                        />
                        <FacebookLogin
                            appId={process.env.REACT_APP_FACEBOOK_LOGIN_APP_ID}
                            autoLoad={false}
                            size="small"
                            textButton=""
                            tag="div"
                            callback={responseFacebook}
                            icon="fa-facebook"
                        />
                    </div>
                    */}
                    <div className={"register"}>
                        Vous possedez un compte ?
                        <Link to={"/login"}>Se connecter</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register;