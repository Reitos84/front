import imgVector from '../assets/not-found.jpg';
import '../css/not-found.css';

const NotFoundPage = () => {
    return(
        <div className={"not-found"}>
            <img src={imgVector} alt={"not-found"} />
            <p>Cette page est introuvable</p>
            <button>
                Page d'acceuil
            </button>
        </div>
    )
}

export default NotFoundPage;