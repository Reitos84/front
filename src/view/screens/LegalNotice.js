import "../css/agency.css";
import "../css/investments.css";
import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import defaultImage from "../assets/cgv.jpg";
const LegalNotice = () => {

    document.title = 'Mentions légales';


    return(
        <>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Mentions légales</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className="sample-page policy">
            <p>
                Ces mentions légales sont applicables à tout contenu disponible sous le nom de domaine <br></br>
                <a href="www.abidjanhabitat.com">www.abidjanhabitat.com</a> , y compris tous les sous-domaines et toutes les sous-pages du domaine.
                Nos pages web vous proposent également des liens vers d'autres sites qui ne nous appartiennent
                pas. En accédant à un tel autre site, vous acceptez que cet accès s'effectue à vos risques et périls.
            </p>
            <div className="intro">Siège social</div>
            <p>
               Ilot 1 Lot 17 N’Dotre à cote du Complexe Prélat. Abidjan
                </p>
                
            <div className="intro">Contact</div>
            <p>
               Courriel : <a href="mailto:aide@abidjanhabitat.com">aide@abidjanhabitat.com</a> 
            </p>
                
            <div className="intro">Représentée par</div>
            <p> Serge Aoussou, PDG </p>
                
            <div className="intro">Immatriculation</div>
            <p>
                Amikan Technologies SARL (AbidjanHabitat.com) est une société constituée et immatriculée sous le
                régime des lois de la Cote d’Ivoire.
            </p>
                
            <div className="intro">Informatique et libertés</div>
            <p>
                <span> Droit d'accès, modification et suppression des données personnelles vous concernant :</span><br></br>
               <span> Courriel : <a href="mailto:aide@abidjanhabitat.com">aide@abidjanhabitat.com</a> </span>
            </p>
                
            <div className="intro">Règlement en ligne de litiges</div>
            <p>
                La Commission de l’UE a créé une plate-forme Internet pour le règlement en ligne des litiges. Il peut
                servir de point de contact pour la résolution extrajudiciaire des litiges qui surgissent des contrats de
                vente en ligne ou des contrats de service. Vous pouvez y accéder à l’adresse suivante
                : <a href="http://ec.europa.eu/consumers/odr">http://ec.europa.eu/consumers/odr </a>. Toutefois, Amikan Technologies SARL. ne participe pas aux
                procédures facultatives de résolution de litiges devant un conseil d’arbitrage de consommation en
                France. Nous ne sommes pas obligés de le faire et préférons clarifier toutes les préoccupations en
                échange direct avec nos clients
            </p>    


         


        </div>
        </>
    )
}

export default LegalNotice;