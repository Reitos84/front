import Article from '../components/Blog/Article';
import BlogCategories from '../components/Blog/BlogCategories';
import PrimaryArticle from '../components/Blog/PrimaryArticle';
import '../css/blog.css';
import { useState, useEffect } from 'react';

const Blog = () => {

    const [listing, setListing] = useState([]);
    const [page, setPage] = useState(1);
    const [totalArticle, setTotalArticle] = useState(0);

    useEffect(() => {

    }, []);

    return(
        <div className="blog">
            <div className='current'>
                <PrimaryArticle />
                <div className='article-list'>
                    <Article />
                    <Article />
                    <Article />
                    <Article />
                    <Article />
                    <Article />
                </div>
                {
                    listing.length > 0 ?
                        <div className="paginate-index">
                            <div className={"number"}>
                                Afficher { listing.length } sur { totalArticle } agences
                            </div>
                            <div className="liner">
                                <span style={{ width: `${(listing.length / totalArticle ) * 100}%` }}></span>
                            </div>
                            {
                                listing.length < totalArticle ?
                                    <div onClick={() => setPage(page + 1)} className="selected">Afficher plus</div>
                                    :
                                    ""
                            }
                        </div>
                    :
                        <></>
                }
            </div>
            <div className='split-search'>
                <div className='blocked'>
                    <div className='search'>
                        <input type={"text"} placeholder="Rechercher" />
                        <button>Rechercher</button>
                    </div>
                    <BlogCategories />
                    <div className='categories-side'>
                        <div className='title'>
                            Nous suivre
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Blog;