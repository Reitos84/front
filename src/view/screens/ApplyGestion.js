import {useState, useEffect} from "react";
import loader from "../assets/loader/785.svg";
import GestureService from "../../services/api/GestureService";
import {useHistory} from "react-router-dom";
import {CheckCircleOutlined} from "@material-ui/icons";
import TypeService from '../../services/api/TypeService';

const ApplyGestion = () => {

    document.title = "Gestion Immobilière";

    const gestureService = new GestureService();
    const typeService = new TypeService();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [contact, setContact] = useState('');
    const [city, setCity] = useState('Abidjan');
    const [types, setTypes] = useState([]);
    const [localisation, setLocalisation] = useState('');
    const [address, setAddress] = useState('');
    const [comment, setComment] = useState('');
    const [selectedType, setSelectedType] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [success, setSuccess] = useState(false);

    const history = useHistory();

    useEffect(async () => {
        let data = await typeService.getAll();
        setTypes(data['hydra:member']);
    }, []);

    const send = () => {
        if([
                name, 
                email, 
                contact,
                city,
                localisation,
                selectedType,
                comment,
                address
            ].every(x => x.length > 0)
        ){
            setLoad(true);
            gestureService.post({
                name: name,
                email: email,
                contact: contact,
                city: city,
                location: localisation,
                type: selectedType,
                comment: comment,
                address: address
            }).then((response) => {
                if(response.status === 201){
                    setSuccess(true);
                }else{
                    setErrorMessage('Une erreur s\'est produite. Essayez à nouveau');
                }
                setLoad(false);
            }).catch((error) => {
                setErrorMessage('Une erreur s\'est produite. Essayez à nouveau');
                setLoad(false);
            });
        }else{
            setErrorMessage('Veillez à bien remplir le formulaire');
            setLoad(false);
        }
    }

    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a été effectué. Nous allons vous contacter pour la suite.</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form evaluate">
                <div className={"int-title"}><h1>Gestion Immobilière</h1></div>
                <div className={"form-content"}>
                    {/* <div className="evaluate_text">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
                        </p>
                    </div> */}
                    <div className="new_form_styles">
                        <div className="form_row">
                            <div className={"box"}>
                                <label>Ville</label>
                                <input disabled={true} type="text" value={city} />
                            </div>
                            <div className={"box"}>
                                <label>Quartier</label>
                                <input onChange={(e) => setAddress(e.currentTarget.value)} value={address} type={"text"} />
                            </div>
                        </div>
                        <div className="form_row">
                            <div className={"box"}>
                                <label>Localisation</label>
                                <textarea onChange={(e) => setLocalisation(e.currentTarget.value)} value={localisation}></textarea>
                            </div>
                            <div className={"box"}>
                                <label>Description</label>
                                <textarea onChange={(e) => setComment(e.currentTarget.value)} value={comment}></textarea>
                            </div>
                        </div>

                        <div className="form_row">
                        <div className={"box"}>
                            <label>Type de propriété</label>
                            <select value={selectedType} onChange={(e) => setSelectedType(e.currentTarget.value)}>
                                <option value="">Sélectionner un type</option>
                                {
                                    types.map(data => 
                                        <option value={data['@id']}>{ data.title }</option>    
                                    )
                                }
                            </select>
                        </div>
                        <div className={"box"}>
                            <label>Nom et Prénom(s)</label>
                            <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                        </div>
                    </div>
                    <div className="form_row">
                        <div className={"box"}>
                            <label>Email</label>
                            <input onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"text"} />
                        </div>
                        <div className={"box"}>
                            <label>Contact</label>
                            <input onChange={(e) => setContact(e.currentTarget.value)} value={contact} type={"text"} />
                        </div>
                    </div>
                    <div className="form_row">
                        <div className="message">
                            { errorMessage }
                        </div>
                    </div>
                    <div className="form_row">
                        <div className="form_btn">
                            <div className={"field"}>
                                <div className={"inlined-field"}>
                                    <button onClick={send} className={"btn"}>
                                        {
                                            load ? <img src={loader} alt="Chargement" /> : ''
                                        }
                                        Envoyer votre demande
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                   

                </div>
            </div>
        </div>
    )
}

export default ApplyGestion;