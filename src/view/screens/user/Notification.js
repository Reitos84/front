import { ScheduleOutlined } from "@material-ui/icons";
import NotificationService from "../../../services/api/NotificationService";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import NotificationSkeleton from "../../components/NotificationSkeleton";
import EmptyList from "../../components/EmptyList";

const Notification = () => {

    document.title = "Notifications";

    const [listing, setListing] = useState([]);
    const [load, setLoad] = useState(true);

    const user = useSelector(state => state.auth.data);

    const notificationService = new NotificationService();

    useEffect(() => {
        notificationService.getMyNotification(user.id).then(result => {
            setListing(result['hydra:member']);
            setLoad(false);
        }).catch(() => {
            setLoad(false);
        });
    });

    return(
        <div className={"profile-content"}>
            <div className={"simple-tit"}>
               <h1>Notifications</h1> 
            </div>
            <div style={{minHeight: '300px'}} className={"note-list"}>
                {
                    load ?
                        <>
                            <NotificationSkeleton />
                        </>
                    :

                        listing.length > 0 ?
                            listing.map(note => <div key={note.id} className={"uno-note"}>
                                <div className={"title"}>{ note.title }</div>
                                <div className={"description"}>
                                    { note.content }
                                </div>
                                <div className={"date"}>
                                    <ScheduleOutlined />
                                    { note.createdAt }
                                </div>
                            </div> )
                        :
                            <EmptyList />
                }
            </div>
        </div>
    )
}

export default Notification;