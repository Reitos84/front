import '../../css/information.css';
import '../../css/profile-content.css';
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {updateInfos} from "../../../core/reducer/authSlice";
import loader from "../../assets/loader/785.svg";
import UserService from "../../../services/api/UserService";
import {CheckCircleOutlined, EditOutlined} from "@material-ui/icons";
import EditContact from "../../components/EditContact";

const Profile = () => {

    document.title = "Mes informations";

    const userService = new UserService();
    const data = useSelector(state => state.auth.data);

    const dispatch = useDispatch();

    const [name, setName] = useState(data.name);
    const [gender, setGender] = useState(data.gender ? data.gender : null);
    const [mobilephone, setMobilephone] = useState(data.mobilephone ? data.mobilephone : null);
    const [load, setLoad] = useState(false);
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [editNum, setEditNum] = useState(false);

    useEffect(() => {
        if(success){
            setTimeout(() => {
                setSuccess(false);
            }, 5000);
        }
    }, [success]);

    const handleChangeInfos = () => {
        setLoad(true);
        if(name.length > 0){
            setMessage('');
            userService.editProfile({
                name: name,
                mobilephone: mobilephone,
                gender: gender
            }, data.id).then(data => {
                if(data.id){
                    setLoad(false);
                    dispatch(updateInfos(data));
                    setSuccess(true);
                }else{
                    setLoad(false);
                    setMessage('Une erreur s\'est produite, verifiez vos informations');
                }
            })
        }else{
            setLoad(false);
            setMessage('Le champ nom et prénoms ne peut être vide');
        }
    }

    return(
        <div className={"profile-content"}>
            <EditContact show={editNum} close={() => setEditNum(false)} />
            <div className={success ? "alert-success-message active" : "alert-success-message"}>
                <div className={"icon"}>
                    <CheckCircleOutlined />
                </div>
                <div className={"texte"}>
                    Votre profil a été mise à jour
                </div>
            </div>
            <div className={"simple-tit"}>
                <h1>Informations personnelles</h1>
            </div>
            <div className={"splited-content"}>
                <div className={"wide informations"}>
                    <div className={"box"}>
                        <label>Nom et Prénoms</label>
                        <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                    </div>
                    <div className={"box"}>
                        <label>Email</label>
                        <p>
                            {data.email}
                        </p>
                    </div>
                    <div className={"box"}>
                        <label>Sexe</label>
                        <select onChange={(e) => setGender(e.currentTarget.value)} value={gender}>
                            <option>Selectionner une valeur</option>
                            <option value={"homme"}>Homme</option>
                            <option value={"femme"}>Femme</option>
                        </select>
                    </div>
                    <div className={"box"}>
                        <label>Numéro de téléphone</label>
                        <div className={"contact-shower"}>
                            {mobilephone} <EditOutlined onClick={() => setEditNum(true)} className={"pen"} />
                        </div>
                    </div>
                    <div className={"message"}>
                        {message}
                    </div>
                    <button onClick={handleChangeInfos} className={"saveBtn"}>
                        {
                            load ? <img alt={"veillez patienter"} src={loader} /> : ''
                        }
                        Sauvegarder
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Profile;