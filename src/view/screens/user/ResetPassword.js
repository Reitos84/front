import {MailOutline} from "@material-ui/icons";
import {useState} from "react";
import loader from "../../assets/loader/785.svg";
import paperplane from "../../assets/paper_plane.png";
import {Link} from "react-router-dom";
import ResetPasswordService from "../../../services/api/ResetPasswordService";

const ResetPassword = () => {

    document.title = "Mot de passe oublié";

    const [message, setMessage] = useState('');
    const [email, setEmail] = useState('');
    const [load, setLoad] = useState(false);
    const [success, setSuccess] = useState(false);
    const [retryLoad, setRetryLoad] = useState(false);

    const resetPasswordService = new ResetPasswordService();

    const ResendMail = () => {
        if(email.length > 0){
            setRetryLoad(true);
            resetPasswordService.requestEmail(email).then(res => {
                setRetryLoad(false);
            }).catch(err => {

            });
        }else {
            setMessage('Veillez entrer votre email');
        }
    }

    const requestEmail = async () => {
        if(email.length > 0){
            setLoad(true);
            resetPasswordService.requestEmail(email).then(res => {
                setLoad(false);
                if(res.success){
                    setSuccess(res.success);
                }else{
                    setMessage(res.message);
                }
            }).catch(err => {

            })
        }else {
            setMessage('Veillez entrer votre email');
        }
    }

    return(
        <div className={"login-container"}>
            <div className={"split image"}>
                <Link to={""} className={"logo"}>
                    <img src='https://imapi.me/assets/logo/logo2.png' />
                </Link>
            </div>
            <div className={"split form"}>
                <div className={"fields"}>

                    {
                        success ?
                            <>
                                <div className={"sent"}>
                                    <div className={"thumb"}>
                                        <img src={paperplane}  alt={"email envoyé"} />
                                    </div>
                                    <div className={"title"}>L'email a été envoyé</div>
                                    <div className={"exp"}>
                                        Veuillez vérifier votre boîte de réception et cliquez sur le lien reçu pour réinitialiser votre mot de passe
                                    </div>
                                    <br />
                                    <div className={"ask-question"}>
                                        Vous n'avez pas reçu le lien ?
                                        <span onClick={ResendMail} className={"click"}>
                                            { retryLoad ?
                                                 <img style={{width: '20px'}} src={loader} alt={"Veillez patienter"}/>
                                                :
                                                 ' Renvoyé'
                                            }
                                        </span>
                                    </div>
                                </div>
                            </>
                            :
                            <>
                                <div className={"title"}>
                                    Réinitialiser votre mot de passse
                                    <span className={"line"}/>
                                </div>
                                <div>
                                    Entrez votre e-mail enregistré ci-dessous pour recevoir les instructions de
                                    réinitialisation du mot de passe
                                </div>
                                <div className={"message"}>
                                    {message}
                                </div>
                                <div className={"box"}>
                                    <MailOutline/>
                                    <input value={email} name={"email"}
                                           onChange={(e) => setEmail(e.currentTarget.value)} type={"email"}
                                           placeholder={"Email"}/>
                                </div>
                                <button onClick={requestEmail} className={"submit"}>
                                    {load ? <img src={loader} alt={"Veillez patienter"}/> : ''}
                                    Envoyer
                                </button>
                                <div className={"register"}>
                                    <Link to={"/login"}>Se connecter</Link>
                                </div>
                            </>
                    }
                </div>
            </div>
        </div>
    )
}

export default ResetPassword;