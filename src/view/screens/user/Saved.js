import SavedService from '../../../services/api/SavedService';
import '../../css/saved.css';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Property from "../../components/Property";
import GoogleMapReact from "google-map-react";
import EmptyList from "../../components/EmptyList";
import PropertySkeleton from '../../components/PropertySkeleton';
import Pined from "../../components/Pined";

const Saved = () => {

    document.title = "Propriétés sauvegardées";

    const user = useSelector(state => state.auth.data);

    const savedService = new SavedService();

    const [listing, setListing] = useState([]);
    const [load, setLoad] = useState(true);
    const [page, setPage] = useState(1);
    const [totalElements, setToTalElements] = useState(0);
    const [index, setIndex] = useState(0);

    useEffect( () => {
        savedService.getAll(user.id, page).then(result => {
            if(result['hydra:member'].length > 0){
                setListing(() => {
                    return [...listing, ...result['hydra:member']]
                });
                setToTalElements(result['hydra:totalItems']);
            }
            setLoad(false);
        }).catch(() => {
            setLoad(false);
        });
    }, [page]);

    const defaultProps = {
        center: {
            lat: 5.316667,
            lng: -4.033333
        },
        zoom: 12.5,
        mapTypeId: "roadmap",
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    const removeSaved = async (index) => {
        const result = await savedService.removeSave(index);
        if(result.status === 204){
            setListing(listing.filter(save => save.id !== index));
            setToTalElements(totalElements - 1);
        }
    }

    return(
        <div className={"saved-container"}>
            <div className={"saved-list"}>
                <div className={"top"}>
                    <div className="left">
                        <span>Propriétés sauvegardées</span>
                        { totalElements }
                        { totalElements > 1 ? ' propriétés trouvées' : ' propriété trouvée' }
                    </div>
                </div>
                <div className="property-list">
                    {
                        load ?
                        <>
                            <PropertySkeleton />
                            <PropertySkeleton />
                            <PropertySkeleton />
                            <PropertySkeleton />
                        </>
                        :
                            listing.length > 0 ?

                                listing.map(me => <Property handleSave={() => removeSaved(me.id)} savedIndex={me.id} key={me.id} data={me.property} />)
                            :
                                <EmptyList />
                    }
                </div>
                {
                    listing.length > 0 ?
                        <div className="paginate-index">
                            <div className={"number"}>
                                Afficher { listing.length } sur { totalElements } agences
                            </div>
                            <div className="liner">
                                <span style={{ width: `${(listing.length / totalElements ) * 100}%` }}></span>
                            </div>
                            {
                                listing.length < totalElements ?
                                    <div onClick={() => setPage(page + 1)} className="selected">Afficher plus</div>
                                    :
                                    ""
                            }
                        </div>
                    :
                        <></>
                }
            </div>
            <div className={"map saved-prop"}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
                    center={defaultProps.center}
                    defaultZoom={defaultProps.zoom}
                    yesIWantToUseGoogleMapApiInternals={true}
                >
                    { listing.map(me => <Pined selectIndex={setIndex} index={index} draggable={false} lat={me.property.lat} lng={me.property.lng} property={me.property} />) }
                </GoogleMapReact>
            </div>
        </div>
    )
}

export default Saved;