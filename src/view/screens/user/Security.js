import loader from "../../assets/loader/785.svg";
import {useEffect, useState} from "react";
import UserService from "../../../services/api/UserService";
import {useSelector} from "react-redux";
import {
    VisibilityOffOutlined,
    VisibilityOutlined,
    CheckCircleOutlined,
    WarningOutlined
} from "@material-ui/icons";
import * as moment from "moment";
import 'moment/locale/fr';

const Security = () => {

    document.title = "Connexion";

    const userService = new UserService();

    const user = useSelector(state => state.auth.data);

    const [load, setLoad] = useState(false);
    const [password, setPassword] = useState('');
    const [validatePass, setValidatePass] = useState('');
    const [message, setMessage] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    //const [showSelectPass, setShowSelectPass] = useState(false);
    const [success, setSuccess] = useState(false);
    const [disableAlert, setDisableAlert] = useState(false);
    const [disLoad, setDisLoad] = useState(false);

    useEffect(() => {
        if(success){
            setTimeout(() => {
                setSuccess(false);
            }, 5000);
        }
    }, [success]);

    const handleChangePassword = () => {
        if(password.length > 0 && password === validatePass){
            setLoad(true);
            userService.changePassword(password).then(data => {
                if(data.success){
                    setLoad(false);
                    setPassword('');
                    setValidatePass('');
                    setSuccess(true);
                }else{
                    setMessage('Une erreur s\'est produite, essayez à nouveau');
                }
            })
        }else{
            setMessage('Les deux mots de passe ne correspondent pas.');
            setLoad(false);
        }
    }

    const disableAccount = () => {
        setDisLoad(true);
        userService.editProfile({
            isEnabled: false
        }, user.id).then((data) => {
            if(!data.isEnabled){
                setDisableAlert(false);
                setSuccess(true);
                setDisLoad(false);
            }
        });
    }

    return(
        <div className={"profile-content"}>
            <div className={disableAlert ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <WarningOutlined style={{color: 'darkorange'}} />
                    </div>
                    <div className={"tit"}>Avertissement</div>
                    <div>Vous etes sur le point de desactiver votre compte. Une fois effectuer vous ne pourrez plus vous connecter.</div>
                    <button style={{background: 'red'}}  onClick={disableAccount} className={"btn"}>
                        {
                            disLoad ?
                                <img src={loader} alt={"chargement"} />
                            :
                                <></>
                        }
                        Continuer
                    </button>
                    <button style={{marginLeft: '20px'}} onClick={() => setDisableAlert(!disableAlert)} className={"btn"}>
                        Annuler
                    </button>
                </div>
            </div>
            <div className={success ? "alert-success-message active" : "alert-success-message"}>
                <div className={"icon"}>
                    <CheckCircleOutlined />
                </div>
                <div className={"texte"}>
                    Mise a jour effectuee
                </div>
            </div>
            <div className={"simple-tit"}>
                <h1>Connexion</h1>
            </div>
            <div className={"informations no-split"}>
                {/*
                    <div className={ showSelectPass ? "account-mask show" : "account-mask" }>
                        <div className={"account-mask-content"}>
                            <CloseOutlined className={"m-close"} />
                            <h3>Ajouter un mot de passe</h3>
                            <div className={"desc"}>
                                Pour désactiver la connexion avec les réseaux sociaux, vous devez choisir un mot de passe
                            </div>
                            <div className={"box"}>
                                <input onChange={(e) => setPassword(e.currentTarget.value)} value={password} type={"password"} placeholder={"Saisir le mot passe"}/>
                                <input onChange={(e) => setValidatePass(e.currentTarget.value)} value={validatePass} type={"password"} placeholder={"Répétez le mot passe"}/>
                            </div>
                            <button onClick={handleChangePassword} className={"saveBtn"}>Enregistrer</button>
                        </div>
                    </div>
                */}
                <h2 className={"other-title"}>Mon compte</h2>
                <div style={{color: '#727272', fontSize: 14, position: 'relative'}}>
                    Inscrit { moment(user.createdAt).calendar() }
                </div>
                <div className={"box flex"}>
                    <label>Désactiver mon compte</label>
                    <span onClick={() => setDisableAlert(true)} className={"disable red"}>
                        Désactiver
                    </span>
                </div>
                {
                    user.provider ?
                        <>
                            <h2 className={"other-title"}>Réseaux sociaux</h2>
                            <div className={"box"}>
                                <div className={"social-line"}>
                                    <div>
                                        Google
                                    </div>
                                    <div className={user.provider === 'google' ? "status active" : "status"} />
                                </div>
                                <div className={"social-line"}>
                                    <div>
                                        Facebook
                                    </div>
                                    <div className={ user.provider === 'facebook' ? "status active" : "status" } />
                                </div>
                            </div>
                        </>
                        :
                        <>
                            <h2 className={"other-title"}>Connexion</h2>
                            <br />
                            <label>Modifier votre mot de passe</label>
                            <div className={"box"}>
                                {
                                    showPassword ? <VisibilityOffOutlined onClick={() => setShowPassword(!showPassword)} className={"eye"} /> : <VisibilityOutlined onClick={() => setShowPassword(!showPassword)} className={"eye"} />
                                }
                                <input onChange={(e) => setPassword(e.currentTarget.value)} value={password} type={ showPassword ? "text" : "password"} placeholder={"Nouveau mot de passe"} />
                                <input onChange={(e) => setValidatePass(e.currentTarget.value)} value={validatePass} type={showPassword ? "text" : "password"} placeholder={"Répétez le mot de passe"} />
                            </div>
                            <div className={"message"}>
                                { message }
                            </div>
                            <button onClick={handleChangePassword} className={"saveBtn"}>
                                {
                                    load ? <img src={loader} alt={"chargement"} /> : ''
                                }
                                Enregistrer
                            </button>
                        </>
                }
            </div>
        </div>
    )
}

export  default Security;