import '../css/reservation.css';
import PropertyService from "../../services/api/PropertyService";
import {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {thousandsSeparators} from 'currency-thousand-separator';
import image from "../assets/agency-image.jpg";
import {RoomOutlined} from "@material-ui/icons";

const Reservation = () => {

    const propertyService = new PropertyService();
    const [property, setProperty] = useState(null);

    const params = useParams();
    const slug = params.slug;

    useEffect(async () => {
        let data = await propertyService.getWithSlug(slug);
        setProperty(data['hydra:member'][0]);
    }, []);

    console.log(property);

    if(!property){
        return(
            <div>Hello</div>
        )
    }else {
        return (
            <>
                <div className={"reservation"}>
                    <div className={"title"}>Faire une réservation</div>
                    <div className={"reservation-content"}>
                        <div className={"body"}>
                            <div className={"step"}>
                                <div className={"step-title"}>
                                    <span>1</span>
                                    Propriété à réserver
                                </div>
                                <div className={"step-content"}>
                                    <div className={"choosed-house"}>
                                        <img src={image} alt={"house"} />
                                        <div>
                                            <div className={"ch-title"}>{ property.title }</div>
                                            <div>
                                                <RoomOutlined />
                                                { property.area }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={"step"}>
                                <div className={"step-title"}>
                                    <span>2</span>
                                    Date de la réservation
                                </div>
                                <div className={"step-content"}>

                                </div>
                            </div>
                            <div className={"step"}>
                                <div className={"step-title"}>
                                    <span>3</span>
                                    Vos informations
                                </div>
                                <div className={"step-content"}>

                                </div>
                            </div>
                            <div className={"step"}>
                                <div className={"step-title"}>
                                    <span>4</span>
                                    Paiement
                                </div>
                                <div className={"step-content"}>

                                </div>
                            </div>
                        </div>
                        <div className={"mapped-price"}>
                            <div className={'s-top'}>Montant de la réservation</div>
                            <div className={"cost"}>
                                <span className={"freq"}>{property.frequency}</span>
                                <span className={"pr"}>{thousandsSeparators(property.price)}{process.env.REACT_APP_CURRENCY}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Reservation;