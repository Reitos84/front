import { Link } from "react-router-dom";
import "../css/agency.css";
import banner from "../assets/agency-banner.jpg";
import join from "../assets/join-community.jpg";
import AgencyService  from '../../services/api/AgencyService';
import {useCallback, useEffect, useState} from "react";
import AgencyElement from "../components/AgencyElement";
import AgencySkeleton from "../components/AgencySkeleton";
import EmptyList from "../components/EmptyList";

const AgencyList = () => {

    document.title = 'Agences Immobilières - Vente et Location de bien immobilier à abidjan';

    const agencyService = new AgencyService();
    const [listing, setListing] = useState([]);
    const [page, setPage] = useState(1);
    const [totalAgency, setTotalAgency] = useState(0);
    const [searchKey, setSearchKey] = useState('');
    const [load, setLoad] = useState(true);

    const findData = useCallback(async () => {
        setLoad(true);
        var result = null;
        if(searchKey.length > 0){
            result = await agencyService.searchAgency(searchKey, page);
        }else{
            result = await agencyService.getAgency(page);
        }
        if(page === 1){
            setListing([...result['hydra:member']]);
        }else{
            setListing([...listing, ...result['hydra:member']]);
        }
        setTotalAgency(result['hydra:totalItems']);
        setLoad(false);
    });

    useEffect(findData, [page]);

    const handleInitSearch = () => {
        if(page === 1){
            findData();
        }else{
            setPage(1);
        }
    }

    return( 
        <div className="agency-page">
            <div className="agency-banner">
                <div className="gradient">
                    <div className="path">
                        <Link to={""}>Acceuil</Link> | Agences
                    </div>
                    <div className={"big-text"}>
                        <h1>Nos agences</h1>
                    </div>
                </div>
                <img alt={""} src={banner} />
            </div>
            <div className="all-agency">
                <div className="agency-list">
                    <div className="listing-side">
                        {
                            load ?
                                < div className="agency-skeletonBox">
                                    <AgencySkeleton />
                                    <AgencySkeleton />
                                    <AgencySkeleton />
                                    <AgencySkeleton />
                                    <AgencySkeleton />
                                    <AgencySkeleton />
                                </div>
                            :
                                listing.length > 0 ?
                                    listing.map(agency => <AgencyElement key={agency.id} data={agency} />)
                                :
                                    <EmptyList />
                        }
                    </div>
                    {
                        listing.length > 0 ?
                            <div className="paginate-index">
                                <div className={"number"}>
                                    Afficher { listing.length } sur { totalAgency } agences
                                </div>
                                <div className="liner">
                                    <span style={{ width: `${(listing.length / totalAgency ) * 100}%` }}></span>
                                </div>
                                {
                                    listing.length < totalAgency ?
                                        <div onClick={() => setPage(page + 1)} className="selected">Afficher plus</div>
                                        :
                                        ""
                                }
                            </div>
                        :
                            <></>
                    }
                </div>
                <div className={"utility"}>
                    <div className="filter-agency">
                        <div className="title">Rechercher une agence</div>
                        <div className="filter-box">
                            <div className="field">
                                <label>Mot clé</label>
                                <input onChange={(e) => setSearchKey(e.currentTarget.value)} value={searchKey} type={"text"} placeholder={"Nom agence"} />
                            </div>
                            <div className="button">
                                <button onClick={handleInitSearch}>Rechercher</button>
                            </div>
                        </div>
                    </div>
                    <div className={"join"}>
                        <img alt={"Rejoindre"} src={join} />
                        <div className="join-side">
                            <span>Voulez-vous rejoindre notre réseau de spécialistes de l’immobilier ?</span>
                            <a target={"_blank"} href={process.env.REACT_APP_AGENCY_LINK}>Rejoindre</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AgencyList;