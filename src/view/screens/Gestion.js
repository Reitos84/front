import {Link} from "react-router-dom";
import "../css/agency.css";
import "../css/gestion.css";
import gestion from "../assets/page/gestion-imo.jpg";
import gestionBanner from "../assets/page/property-man.jpg";



const Gestion = () => {
    
    return(
        <div className="">
            <div className="agency-header">
                <div className='gardienStyle for-agency'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}> 
                        <h1>Gestion</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={gestionBanner} />
            </div>
            <div className="main-content">
                <h2 className="sectionTitle">
                    <span className="smallTitle">Etre proprietaire d'un bien immobilier</span> 
                    <span>est le debut d'une longue histiore de management</span> 
                </h2>
                <p>
                    Pour éviter le stress et les mauvaises surprises, Confiez la gestion de vos biens immobiliers à des professionnels.
                    A Abidjan Habitat, nous travaillons avec une équipe d’experts passionnés de la gestion immobilière.
                    Qu’est-ce qu’un gestionnaire immobilier et comment peuvent-ils vous aider à atteindre vos objectifs financier en toute quiétude et sérénité. 
                    En bref, un gestionnaire joue un rôle majeur en vous aidant à séparer la gestion de vos investissements séparément de vos opérations journalières.
                    Compte tenu de l'importance de ce partenariat, il est essentiel de comprendre le rôle de votre gestionnaire de baux immobiliers, ainsi que ce qui est attendu de votre côté.
                    Comment nos gestionnaires immobiliers vous aident :
                </p>

                <div className="">
                    <div className="global-gestion-help">
                        <div className="picto-card">
                            <h3 className="section_under">Gagner du temps</h3>
                            <p>
                                Nos équipes de gestion immobilière sont sur le terrain pour gérer le quotidien, 
                                afin que vous puissiez vous concentrer sur la croissance de votre flux de revenus passifs.
                            </p>
                        </div>

                        <div className="picto-card">
                            <h3 className="section_under">Tranquillité d'esprit</h3>
                            <p>
                                En partant des réparations et de l'entretien à la communication avec les locataires, 
                                nos équipes de gestion immobilière protègent vos actifs et assurent le bon fonctionnement de tout.
                            </p>
                        </div>
                    </div>
                    <div className="">
                        <h3 className="section_under">Manager vos biens et Investir plus intelligemment</h3>
                        <p>
                            Pour les propriétaires et investisseurs immobiliers locatifs, 
                            ils vous permettent d’avoir les meilleurs rendements. 
                            Nos gestionnaires immobiliers vous permettent de posséder 
                            et maximiser le rendement de vos biens partout où ils se trouvent. 
                            Voici ce que nous pouvons faire pour vous :
                        </p>
                        <div className="container">
                            <ul className="gestion-list">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <li>Sélection des locataires et traiter les candidatures des locataires</li>
                                        <li>S'assurer que les exigences sont respectées par les locataires.</li>
                                        <li>Négocier, collecter et répartir les loyers</li>
                                        <li>Répondre à tous les besoins et préoccupations des locataires</li>
                                        <li>Gérer toutes les communications avec les locataires</li>
                                        <li>Expulsions</li>
                                        <li>Toutes les réparations</li>
                                        <li>Entretien préventif/entretien saisonnier</li>
                                    </div>
                                    <div className="col-lg-6">
                                       
                                        <li>Planification et réalisation d'inspections de routine</li>
                                        <li>Embaucher des sous-traitants tiers et fournir des devis</li>
                                        <li>Louez et commercialisez votre propriété si elle est vacante</li>
                                        <li>Répondre en temps opportun à toutes les questions du propriétaire</li>
                                        <li>Fournir des mises à jour régulières ou une déclaration mensuelle du propriétaire détaillant les profits et les pertes de la propriété</li>
                                        <li>Gérer les inspections d'emménagement/déménagement et les dépôts de garantie</li>
                                    </div>
                                </div>
                               
                               
                            </ul>
                        </div>       
                    </div>
                    
                </div><br/>
        <div className="texte-center">
                <h5 className="strong_p">CONFIEZ LA GESTION DE VOS BIENS AUX EXPERTS D’ABIDJAN HABITAT</h5>
               <Link className={"active-link"} to={"/property-management/apply"}><button className="link__btn">Contactez nous pour parler à un expert.</button></Link>
        </div>
            </div>
            
        </div>
    )
}

export default Gestion;