import {useParams, useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import PropertyService from "../../services/api/PropertyService";
import PropertyDetails from "./PropertyDetails";
import SavedService from "../../services/api/SavedService";
import { useSelector } from 'react-redux';
import LoginModal from "../components/LoginModal";
import PropertyPageSkeleton from "../components/PropertyPageSkeleton";
import UnavaillableProperty from "../components/UnavaillableProperty";
import FeatureService from "../../services/api/FeatureService";


const PropertyPage = () => {

    const user = useSelector(state => state.auth.data);

    const savedService = new SavedService();
    const featureService = new FeatureService();

    const propertyService = new PropertyService();
    const [property, setProperty] = useState(null);
    const [requestLogin, setRequestLogin] = useState(false);
    const [empty, setEmpty] = useState(false);

    const params = useParams();
    const slug = params.code;

    const search = new URLSearchParams(useLocation().search);
    const referrer = search.has('referrer') ? search.get('referrer') : '';

    if(search.has('title')){
        document.title = `${property ? property.title : search.get('title')} - Vente et Location de bien immobilier à abidjan`;
    }

    useEffect(() => {
        propertyService.getWithSlug(slug).then(data => {
            let finded = data['hydra:member'][0];
            if(finded){;
                if(finded.status === 'disponible' || referrer === process.env.REACT_APP_REFFERER_KEY){
                    setProperty(finded);
                    propertyService.setViewed(finded.id);
                }else{
                    setEmpty(true);
                }
            }else{
                setEmpty(true);
            }
        });
    }, [slug]);

    useEffect(() => {
        if(property){
            featureService.getForProperty(property.id).then(data => {
                if(data['hydra:totalItems'] > 0){
                    let finded = data['hydra:member'][0];
                    featureService.update({
                        print: finded.print ? finded.print + 1 : 1,
                        click: finded.click ? finded.click + 1 : 1
                    }, finded.id);
                }
            });
        }
    }, [property]);

    const handleSaveProperty = async () => {
        if(user){
            if(property.iSaved){
                const result = await savedService.removeSave(property.iSaved);
                if(result.status === 204){
                    setProperty({...property, iSaved: 0})
                }
            }else{
                const result = await savedService.saveProperty(property.id, user.id);
                if(result.id){
                    setProperty({...property, iSaved: result.id});
                }
            }
        }else{
            setRequestLogin(true);
        }
    }

    return(
        <>
            <LoginModal state={requestLogin} toggle={setRequestLogin} />
            {
                property ?
                <PropertyDetails referrer={referrer} handleRequestLogin={setRequestLogin} saveMe={handleSaveProperty} data={property} />              
                :
                    empty ?
                        <UnavaillableProperty /> 
                    :
                        <PropertyPageSkeleton />
            }
        </>
    )
}

export default PropertyPage;