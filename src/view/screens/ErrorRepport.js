import { useEffect, useState } from 'react';
import {useParams} from "react-router-dom";
import '../css/error-report.css';
import PropertyService from "../../services/api/PropertyService";
import ErrorReportService from "../../services/api/ErrorReportService";
import { useSelector } from 'react-redux';
import loader from "../assets/loader/785.svg";
import { Skeleton } from "react-skeleton-generator";
import { CheckCircleOutlined } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

const ErrorRepport = () => {

    document.title = 'Repporter une erreur';

    const propertyService = new PropertyService();
    const errorReport = new ErrorReportService();

    const user = useSelector(data => data.auth.data);

    const acceptMessage = [
        'Cette page ne présente pas un bien immobilier',
        'Le prix est faux',
        'Le nombre de chambre est faux',
        'Le type du bien immobilier est faux',
        'L\'emplacement du bien immobilier est faux',
        'Il y a un problème avec la photographie du bien immobilier',
        'Problème de grammaire, orthographe ou de description'
    ];

    const acceptRoles = [
        'Je suis le propriétaire de ce bien immobilier',
        'Je suis le marketteur de cette propriété',
        'Je suis un acheteur',
        'J\'ai vu une annonce sur cette propriété ailleurs'
    ];

    const [property, setProperty] = useState(null);
    const [currentPic, setCurrentPic] = useState(null);
    const [load, setLoad] = useState(false);
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [otherCases, setOtherCases] = useState('');
    const [allCases, setAllCases] = useState([]);
    const [data, setData] = useState({
        role: '',
        comment: '',
        name: user && user.name ? user.name : '',
        email: user && user.email ? user.email : ''
    });

    //Params
    const params = useParams();

    const handleInit = () => {
        setData({
            role: '',
            comment: '',
            name: user && user.name ? user.name : '',
            email: user && user.email ? user.email : ''
        });
        setAllCases([]);
        setOtherCases('');
    }

    useEffect(async () => {
        let data = await propertyService.getWithSlug(params.code);
        let finded = data['hydra:member'][0];
        if(finded){
            let shown = null;
            finded.pictures.forEach(pic => {
                if(pic.isBackground){
                    shown = pic;
                }
            });
            if(!shown){
                shown = finded.pictures[0];
            }
            setCurrentPic(shown);
            setProperty(finded);
        }
    }, []);

    const handleChangeCase = (e) =>  {
        let value = e.currentTarget.value;
        let alternateArray = [...allCases];

        if(alternateArray.length > 0){
            if(alternateArray.includes(value)){
                alternateArray = alternateArray.filter(x => x !== value);
            }else{
                alternateArray = [...alternateArray, value];
            }
        }else{
            alternateArray = [...alternateArray, value];
        }
        setAllCases(alternateArray);
    }

    const handleChangeRole = (e) =>  {
        setData({...data, role: e.currentTarget.value});
    }

    const handleChangeComment = (e) => {
        setData({...data, comment: e.target.value});
    }

    const handleSubmitElement = () => {
        setLoad(true);
        if(property && property.status === 'disponible'){
            if(Object.values(data).every(x => x) && (allCases.length > 0 || otherCases)){

                let alternate = [...allCases];

                if(otherCases.length > 0){
                    alternate = [...alternate, otherCases];
                }

                errorReport.post({
                    ...data,
                    property: property['@id'],
                    agency: property.agency['@id'],
                    message: alternate
                }).then((data) => {
                    if(data.id > 0){
                        setSuccess(true);
                        handleInit();
                    }else{
                        setMessage('Une erreur s\'est produite, vérifiez les information et réessayez');
                    }
                    setLoad(false);
                }).catch((error) => {
                    setMessage('Une erreur s\'est produite, vérifiez les information et réessayez');
                    setLoad(false);
                });
            }else{
                setMessage('Veillez bien remplir le formulaire');
                setLoad(false);
            }
        }else{
            setMessage('Cette propriété n\'est pas disponible');
            setLoad(false);
        }
    }

    const history = useHistory();

    return(
        <div className="error-repport">
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Merci pour votre contribution, nous allons vous contactez pour finiliser la correction</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="repport-title">
                Repporter une erreur
            </div>
            <div className="report-content">
                <div className={"simple-small-tit"}>Vous avez repperé une erreur sur la propriété suivante:</div>
                <br />
                {
                    property ?
                        <div className="property">
                            <img src={`${process.env.REACT_APP_BASE_PATH}${currentPic.url}`} alt={currentPic.name} />
                            <div>
                                <span>{ property.title }</span><br/>
                                { property.area }<br/>
                                { process.env.REACT_APP_CURRENCY } { property.price }{ property.frequency ? `/${property.frequency}` : ''}
                            </div>
                        </div>
                    :
                    <Skeleton.SkeletonThemeProvider color="#FAFAFA">
                        <Skeleton width="100%" height="100px" borderRadius="10px" />
                    </Skeleton.SkeletonThemeProvider>
                }
                <div className="tit">Dites-nous en plus sur cette erreur</div>
                <div>
                    Toutes les informations des propriété sur notre site ont été ajouté par des agents immobilier. Nous allons utiliser vos commentaires pour la correction. Merci d'avoir pris le temps de nous faire part de vos commentaires.
                </div>
                <div className="form-container">
                    {
                        acceptMessage.map(val => 
                            <div className="field">
                                <input checked={ allCases.includes(val) } onChange={handleChangeCase} type="checkbox" value={val} />
                                <div>
                                    { val }
                                </div>
                            </div>
                        )
                    }
                    <div className="field">
                        <label>Autre </label>
                        <textarea value={otherCases} onChange={(e) => setOtherCases(e.currentTarget.value)} placeholder="Saisir ici"></textarea>
                    </div>
                </div>
                <div className="tit">Comment savez-vous que ces informations sont incorrect ?</div>
                <div className="form-container">
                    {
                        acceptRoles.map(val => 
                            <div className="field">
                                <input checked={val === data.role} onChange={handleChangeRole} type="radio" name="role" value={val} />
                                <div>
                                    { val }
                                </div>
                            </div>
                        )
                    }
                    <div className="field">
                        <label>Autre </label>
                        <textarea onChange={handleChangeRole} placeholder="Saisir ici"></textarea>
                    </div>
                </div>
                <div className="tit">Commentaire additionnel</div>
                <div>
                    Veillez saisir toutes les informations possible, cela peut nous aider à resoudre le problème
                </div>
                <div className="form-container">
                    <div className="field">
                        <textarea value={data.comment} onChange={handleChangeComment} className="wide"></textarea>
                    </div>
                </div>
                <div className="tit">Vos informations</div>
                <div>Nous allons utiliser ces informations pour vous contacter à propos de votre commentaire</div>
                <div className="form-container">
                    <div className="field">
                        <label>Email </label>
                        <input value={data.email} onChange={(e) => setData({...data, email: e.currentTarget.value})}  type='text' name="email" />
                    </div>
                    <div className="field">
                        <label>Nom complet </label>
                        <input value={data.name} onChange={(e) => setData({...data, name: e.currentTarget.value})} type='text' name="name" />
                    </div>
                </div>
                <div className="message">
                    {message}
                </div>
                <button onClick={load ? () => {} : handleSubmitElement} className="btn">
                    {
                        load ? <img alt={"veillez patienter"} src={loader} /> : ''
                    }
                    Envoyer
                </button>
            </div>
        </div>
    )
}

export default ErrorRepport;