import {useEffect, useState} from "react";
import TypeService from "../../services/api/TypeService";
import {acceptedRoom} from "../constants/utils";
import {useLocation, useParams} from "react-router-dom";
import Geopify from "../../services/places/Geopify";

const Search = () => {

    const params = useParams();
    const search = new URLSearchParams(useLocation().search);

    const [types, setTypes] = useState([]);
    const [selectedType, setSelectedType] = useState('');
    const [category, setCategory] = useState( params.category === 'sale' || params.category === 'rent' ? params.category : '' );
    const [price, setPrice] = useState(null);
    const [pieces, setPieces] = useState(null);
    const [boundary, setBoundary] = useState('');
    const [location, setLocation] = useState(search.has('location') ? search.get('location') : '');

    const typeService = new TypeService();
    const geopify = new Geopify();

    useEffect (() => {
        typeService.getAll().then(data => {
            setTypes(data['hydra:member']);
        });
    }, []);

    const handleChangeCategory = (value) => {
        setCategory(value === 'rent' || value === 'sale' ? value : '');
    }

    return(
        <>
            <div className={"interest-page"}>
                <div className="form">
                    <div className={"int-title"}><h1>Spécifier votre recherche</h1></div>
                    <form className={"form-content"} action={`/properties/${category}`}>
                        <div className="new_form_styles">
                            <div className="form_row">
                                <div className={"field"}>
                                    <label>Je voudrais </label>
                                    <div className={"inlined-field"}>
                                        <div className={"checkbox"}>
                                            <input required={true} onChange={() => handleChangeCategory('sale')} checked={category === 'sale'} name={"category"} type={"radio"}  /> Acheter
                                        </div>
                                        <div className={"checkbox"}>
                                            <input required={true} onChange={() => handleChangeCategory('rent')} checked={category === 'rent'} name={"category"} type={"radio"}  /> Louer
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="form_row">
                                <div className={"box"}>
                                    <label>Emplacement</label>
                                    <input name={"location"} type="text" value={location} />
                                </div>
                                {
                                    search.has('p') ?
                                        <input style={{display: 'none'}} name={"serves"} value={true} />
                                    :
                                    <></>
                                }

                                <div className={"box"}>
                                    <label>Type de propriété</label>
                                    <select value={selectedType} onChange={(e) => setSelectedType(e.currentTarget.value)}>
                                        <option value="">Tout</option>
                                        {
                                            types.map(type => {
                                                if(!type.category || type.category === selectedType){
                                                    return <option value={type.slug}>{type.title}</option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <div className="form_row">
                                <div className={"box"}>
                                    <label>Nombre de pieces</label>
                                    <select name={"bedroom"} value={pieces} onChange={(e) => setPieces(e.currentTarget.value)}>
                                        <option value={''}>Non definie</option>
                                        {
                                            acceptedRoom.map(x => <option value={x.value}>{x.label}</option>)
                                        }
                                    </select>
                                </div>
                                <div className={"box"}>
                                    <label>Budget Maximale</label>
                                    <input name="priceMax" type="number" value={price} onChange={(e) => setPrice(e.currentTarget.value)} />
                                </div>
                            </div>
                            <div className="form_row">
                                <div className={"field"}>
                                    <div className={"inlined-field"}>
                                        <button type={"submit"} className={"btn"}>
                                            Rechercher maintenant
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {/*
                <div className={"pub-side-long"}>
                    Publicite
                </div>
            */}
        </>
    )
}

export default Search;