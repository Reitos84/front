import {Link, useHistory} from "react-router-dom";
import defaultImage from "../assets/agency-image.jpg";
import '../css/investor.css';
import loader from "../assets/loader/785.svg";
import {useState} from "react";
import {investCalendar, qualifier, experiences, prizes} from "../constants/utils";
import InvestorService from "../../services/api/InvestorService";
import {CheckCircleOutlined} from "@material-ui/icons";

const investorService = new InvestorService();

const Investor = () => {

    document.title = "Devenir investisseur immobilier"

    const [sendLoad, setSendLoad] = useState(false);
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [data, setData] = useState({
        name: '',
        email: '',
        contact: '',
        country: '',
        experience: '',
        qualify: '',
        calendar: '',
        budget: ''
    });

    const sendValues = () => {
        if(Object.values(data).every(x => x.length > 0)){
            setMessage('');
            setSendLoad(true);
            investorService.post(data).then(result => {
                if(result.status === 201){
                    setSendLoad(false);
                    setSuccess(true);
                }else{
                    setMessage('Veillez à bien remplir le formulaire');
                }
            }).catch((err) => {
                setMessage('Une erreur s\'est produite essayez à nouveau');
            })
        }else{
            setMessage('Veillez à bien remplir le formulaire');
        }
    }

    const history = useHistory();

    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a été envoyer nous allons vous contact pour plus de détails</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form">
                <div className={"int-title"}>
                    <h1>Devenir investisseur</h1>
                </div>
                <div className={"form-content"}>
                    <p>
                        Le club des Investisseurs d'Abidjan Habitat vous donne accès à des informations clé en main, des stratégies et des outils d’analyse pour faciliter vos décisions d'investissement avec plus d’assurance et de confiance. 
                        Remplissez ce formulaire pour bénéficier de notre solution pour ne pas juste être un autre investisseur dans immobilier … mais plutôt un investisseur qui a du succès dans l’immobilier!
                    </p>
                    <div className="new_form_styles">
                        <div className="form_row">
                            <div className={"box inline servey"}>
                            <label>Quelle est votre expérience en investissement immobilier locatif ?</label>
                            {
                                experiences.map(x => <div>
                                    <input checked={data.experience === x} onChange={(e) => setData({
                                        ...data,
                                        experience: x
                                    }) } name={"experience"} type={"radio"} value={x} /> {x}
                                </div>)
                            }
                            </div>
                            <div className={"box inline servey"}>
                                <label>Êtes-vous un investisseur qualifié ?</label>
                                {
                                    qualifier.map(x => <div>
                                        <input checked={data.qualify === x} onChange={(e) => setData({
                                            ...data,
                                            qualify: x
                                        }) }  type={"radio"} name={"qualify"} value={x} /> {x}
                                    </div>)
                                }
                            </div>
                        </div>
                        <div className="form_row">
                            <div className={"box inline servey"}>
                            <label>Votre calendrier préféré d'investissement ?</label>
                            {
                                investCalendar.map(x => <div>
                                    <input checked={data.calendar === x} onChange={(e) => setData({
                                        ...data,
                                        calendar: x
                                    }) }  type={"radio"} name={"calendar"} value={x} /> {x}
                                </div>)
                            }
                            </div>
                            <div className={"box inline servey"}>
                                <label>Votre Budget initial  d'investissement ?</label>
                                {
                                    prizes.map(x => <div>
                                        <input checked={data.budget === x} onChange={(e) => setData({
                                            ...data,
                                            budget: x
                                        }) }  type={"radio"} name={"budget"} value={x} /> {x}
                                    </div>)
                                }
                            </div>
                        </div>
                        <div className="form_row">
                            <div className={"box"}>
                                <label>Nom et Prénom(s)</label>
                                <input value={data.name} onChange={(e) => setData({
                                    ...data,
                                    name: e.currentTarget.value
                                }) } type={"text"} />
                            </div>
                            <div className={"box"}>
                                <label>Email</label>
                                <input value={data.email} onChange={(e) => setData({
                                    ...data,
                                    email: e.currentTarget.value
                                }) } type={"text"} />
                            </div>
                        </div>
                        <div className="form_row">
                            <div className={"box"}>
                                <label>Contact</label>
                                <input value={data.contact} onChange={(e) => setData({
                                    ...data,
                                    contact: e.currentTarget.value
                                }) } type={"text"} />
                            </div>
                            <div className={"box"}>
                                <label>Pays</label>
                                <input value={data.country} onChange={(e) => setData({
                                    ...data,
                                    country: e.currentTarget.value
                                }) } type={"text"} />
                            </div>
                        </div>
                        <div className="form_row">
                        <p className="message">{ message }</p>
                        </div>
                        <div className="form_row">
                            <div className={"field"}>
                                <div className={"inlined-field"}>
                                    <button onClick={ sendLoad ? () => {} : sendValues} className={"btn"}>
                                        {
                                            sendLoad ? <img alt={"veillez patienter"} src={loader} /> : ''
                                        }
                                        S'enregistrer maintenant
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Investor;