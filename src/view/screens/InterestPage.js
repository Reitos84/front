import "../css/interest.css";
import {useLocation, useParams, useHistory, Link} from "react-router-dom";
import {updateInfos} from "../../core/reducer/authSlice";
import {useEffect, useState} from "react";
import PropertyService from "../../services/api/PropertyService";
import InterestService from "../../services/api/InterestService";
import UserService from "../../services/api/UserService";
import { useSelector, useDispatch } from "react-redux";
import loader from "../assets/loader/785.svg";
import InterestProperty from '../components/InterestProperty';
import InterestPropertySkeleton from '../components/InterestPropertySkeleton';
import {CheckCircleOutlined, EditOutlined, WarningOutlined} from "@material-ui/icons";
import CountryService from "../../services/api/CountryService";
import EditContact from "../components/EditContact";

const InterestPage = () => {

    const propertyService = new PropertyService();
    const interestService = new InterestService();
    const userService = new UserService();
    const countryService = new CountryService();

    const user = useSelector(state => state.auth.data);

    const [property, setProperty] = useState(null);
    const [moreInfos, setMoreInfos] = useState(true);
    const [visit, setVisit] = useState(false);
    const [name, setName] = useState(user && user.name ? user.name : '');
    const [comment, setComment] = useState('');
    const [contact, setContact] = useState(user && user.mobilephone ? user.mobilephone : '');
    const [email, setEmail] = useState(user && user.email ? user.email : '');
    const [country, setCountry] = useState(user && user.country ? user.country : 'Côte d\'Ivoire');
    const [success, setSuccess] = useState(false);
    const [message, setMessage] = useState('');
    const [sendLoad, setSendLoad] = useState(false);
    const [countryList, setCountryList] = useState([]);
    const [editNum, setEditNum] = useState(false);

    const dispatch = useDispatch();

    const urlParams = new URLSearchParams(useLocation().search);

    const params = useParams();
    const code = params.code;
    const agencyName = params.agency;
    const title = urlParams.get('title');

    document.title = 'Intérêt - Vente et Location de bien immobilier à abidjan';

    useEffect(async () => {
        const data = await propertyService.getWithSlug(code);
        const countriesData = await countryService.get();
        setCountryList(countriesData.data);
        setProperty(data['hydra:member'][0]);
    }, []);

    const submitInterest = async () => {
        if([moreInfos, visit].some(x => x)){
            if([name, comment, contact, email, country].every(x => x.length > 0)){
                setSendLoad(true);
                setMessage('');
                let current = await interestService.iAlreadySendInterest(property.id, email);
                if(current.data['hydra:totalItems'] > 0){
                    setMessage('Vous avez déjà une demande en cours pour cette propriété');
                    setSendLoad(false);
                }else{
                    interestService.postInterest({
                        property: property['@id'],
                        agency: property.agency['@id'],
                        comment: comment,
                        visit: visit,
                        name: name,
                        contact: contact,
                        email: email,
                        country: country,
                        moreDetails: moreInfos
                    }).then(data => {
                        if(data.status === 201){
                            setSendLoad(false);
                            setSuccess(true);
                        }
                    }).catch(() => {
                        setSendLoad(false);
                        setMessage('Une erreur s\'est produite, vérifiez votre connexion internet et rééssayez');
                    });
                    if(user && user.id){
                        const userData = await userService.editProfile({
                            name: name,
                            contact: contact,
                            country: country,
                        }, user.id);
                        if(userData.id){
                            dispatch(updateInfos(userData));
                        }
                    }
                }
            }else{
                setMessage('Veillez bien remplir le formulaire');
            }
        }else{
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    const history = useHistory();

    return(
        <div className={"interest-page"}>
            {
                !user ?
                <div class={"success-process display"}>
                    <div className={"succes-process-content"}>
                        <div className={"icon"}>
                            <WarningOutlined style={{color: '#f55d42'}} />
                        </div>
                        <div>Pour pouvoir envoyer des intérêt vous devez être membre</div>
                        <Link to={'/login'} className={"btn"}>Se connecter</Link>
                    </div>
                </div>
                :
                ''
            }
            <EditContact show={editNum} close={() => setEditNum(false)} />
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre message a bien été envoyé, nous allons vous contactez très bient&ocirc;t</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form">
                <div className={"int-title"}><h1>Contactez { agencyName }</h1></div>
                <div className={"form-content"}>
                    <div className={"field"}>
                        <label>Contactez par rapport à </label>
                        <div className={"inlined-field"}> { property ? property.title : title } </div>
                    </div>
                    <div className="new_form_styles">
                    <div className={"field"}>
                        <label>Je voudrais</label>
                        <div className={"inlined-field"}>
                            <div className={"checkbox"}>
                                <input name={"more"} onChange={() => setMoreInfos(!moreInfos)} checked={moreInfos} type={"checkbox"}  /> Plus d'informations
                            </div>
                            <div className={"checkbox"}>
                                <input name={"visit"} onChange={() => setVisit(!visit)} checked={visit} type={"checkbox"}  /> Visiter la propriété
                            </div>
                        </div>
                    </div>
                    <div className={"box"}>
                        <label>Nom et Prénom(s)</label>
                        <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                    </div>
                    <div className={"box"}>
                        <label>Votre message</label>
                        <textarea onChange={(e) => setComment(e.currentTarget.value)} value={comment}></textarea>
                    </div>
                    <div className={"box"}>
                        <label>Téléphone</label>
                        <div className={"contact-shower"}>
                            {contact} <EditOutlined onClick={() => setEditNum(true)} className={"pen"} />
                        </div>
                    </div>
                    <div className={"box"}>
                        <label>Email</label>
                        <input onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"text"} />
                    </div>
                    <div className={"box"}>
                        <label>Pays</label>
                        <select onChange={(e) => setCountry(e.currentTarget.value)} value={country}>
                            {
                                countryList.map((x, key) =>
                                    <option key={key} value={x.name}>{ x.name }</option>
                                )
                            }
                        </select>
                    </div>
                    <div className={"field"}>
                        <div className="message">
                            { message }
                        </div>
                    </div>
                    <div className={"field"}>
                        <label></label>
                        <div className={"inlined-field"}>
                            <button onClick={sendLoad ? () => {} : submitInterest} className={"btn"}>
                                {
                                    sendLoad ? <img alt={"veillez patienter"} src={loader} /> : ''
                                }
                                Envoyer le message à <span>{ agencyName }</span>
                            </button>
                        </div>
                    </div>
                    </div>
                   
                </div>
                <br />
            </div>
            {
                property ?
                    <InterestProperty property={property} />
                :
                    <InterestPropertySkeleton />
            }
        </div>
    )
}

export default InterestPage;