import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import defaultImage from "../assets/handshake.png";
// import 'App.css';

const ProjectMarketing = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Commercialisation de Projet</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className='container-fluid bg-color'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <h2 className='sectionTitle'> 
                                <span className='smallTitle'> Solutions marketing et publicitaire </span>
                                <span>pour promoteurs immobiliers</span>
                            </h2>
                            <div>
                                <p>
                                    L'immobilier est l'un des outils les plus puissants de création de richesse. Il existe, bien sûr, plusieurs stratégies qui peuvent vous permettre d’atteindre vos objectifs d'investissement. 
                                    <br/> Que vous recherchiez des informations, des opportunités, 
                                    des projets d'investissement immobilier, des propriétés à acheter ou autre chose, vous pouvez les trouver sur notre plateforme.
                                    <br/>
                                    Bien que les consommateurs et les entreprises ivoiriens soient désireux d'acheter des biens immobiliers dans certains des endroits les plus convoités du pays, tous ne sont pas prêts et capables de le faire.
                                </p>
                                <p>
                                    La plupart des acheteurs commencent aujourd'hui leurs recherches en ligne. <br/>
                                    Nos services de marketing pour les promoteurs immobiliers comme vous les aident à capturer et à entretenir des acheteurs potentiel, à renforcer votre image de marque et vous positionner sur le marché, et à louer/vendre vos propriétés avec plus de confiance (même sur plans)
                                    Pour être plus spécifique, nos solutions de marketing vous aident à:
                                </p>
                                <ul className='club-list'>
                                    <li>
                                        <p className='strong-span'> Atteindre démographie ciblée. </p>
                                        L'immobilier est un achat important que tout le monde n'est pas en mesure de faire. 
                                        Le marketing numérique vous aide à cibler et à atteindre des clients potentiels ayant à la fois les moyens et l'intention d'acheter.
                                    </li>
                                    <li>
                                    <p className='strong-span'> Développer la notoriété de votre compagnie. </p> Les plus grands promoteurs immobiliers ont cultivé une marque forte qui enthousiasme leurs clients pour le type de biens immobiliers qu'ils savent  attendre de ces entreprises. Le marketing numérique vous aide à construire votre marque de la même manière, 
                                    en mettant en évidence les aspects de votre travail qui rendent vos projets uniques et adaptés à ceux-ci.
                                    </li>

                                    <li>
                                    <p className='strong-span'>  Raccourcir les délais d'achat. </p>  
                                    Parce que l'immobilier est si coûteux, de nombreux acheteurs potentiels mettent beaucoup de temps à décider s'ils veulent vraiment aller de l'avant avec la transaction. Le marketing numérique vous 
                                    aide à entretenir activement vos prospects et à garder vos projets de développement à l'esprit jusqu'à ce que leur décision soit prise.
                                    </li>
                                    <li>
                                    <p className='strong-span'> Sécurisez les clients le plus tôt possible.</p> 
                                     Le succès d’une promotion immobilière  
                                    dépend du nombre de ventes qui peuvent être conclus avant même la fin du projet (achat sur plan), fournissant le flux de trésorerie nécessaire pour financer un nouveau projet. Le marketing numérique peut vous aider à attirer des prospects et à capter leur intérêt dès le début du processus de création, garantissant ainsi ces premiers revenus vitaux.
                                    </li>
                    
                                </ul>

                            </div>

                            <Link to={{pathname: '/contact', search: `subject=${encodeURI('commercialisation de projet')}`}}> 
                                <button className='link_text mt-2'> 
                                    Contactez-Nous Maintenant
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-fluid'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <h2 className='sectionTitle'> 
                               <span className='smallTitle'> Nos solutions </span>
                               <span>de marketing numériques</span> 
                            </h2>
                            <p>
                            {/* transforment les efforts des promoteurs immobiliers en une force qui vise à vous aider a
                                réduir vos frais de marketing et de publicité. */}
                                Le marketing numérique est la stratégie de génération de leads la plus sous-utilisée dans l'industrie du bâtiment. Pendant des décennies, la plupart des promoteurs immobiliers ont annoncé leurs propriétés en utilisant des tactiques de marketing de masse traditionnelles telles que la télévision, la radio et les panneaux d'affichage. C'étaient autrefois d'excellents moyens de faire passer votre message, mais ils ont tous un gros inconvénient. 
                                Il n'y a aucun moyen de mesurer avec précision la part de votre budget de marketing qui se traduit réellement par une vente.
                            </p>
                            <p>
                                Le marketing numérique change tout cela en créant des données mesurables sur la provenance de vos prospects et de vos ventes. Chaque centime que vous dépensez est traçable 
                                sur l'ensemble du pipeline de vente afin que vous sachiez effectivement combien vous coûte la vente d'une maison ou d'un appartement.
                            </p>

                            <h2 className='sectionTitle meduim '> 
                                <span className='smallTitle'>SUIVRE VOS CLIENTS EN LIGNE INTELLIGEMMENT AVEC DES CAMPAGNES DE RETARGETING</span> 
                            </h2>
                            <p>
                                Bien que tout le monde ne soit pas prêt à acheter ou à se renseigner sur une propriété lorsqu'ils atterrissent sur votre site Web ou votre page Facebook, et bien qu'ils ne commencent pas le processus d'achat immédiatement, 
                                en ce qui concerne la génération de prospects, cela ne signifie pas qu'ils ne le seront jamais clients. 
                            </p>

                            <p>
                                En installant des trackers de reciblage sur votre site Web 
                                (que nous allons construire pour votre projet), 
                                nous pouvons lancer des campagnes de reciblage en utilisant les plateformes publicitaires Google et Facebook.
                            </p>
                            <p>
                                <i>
                                    *Le reciblage est une stratégie marketing intelligente qui permet aux annonces de votre entreprise d'apparaître devant vos clients potentiels lorsqu'ils naviguent sur le Web. Cela fonctionne si bien parce qu'il dirige vos publicités vers des personnes qui ont déjà manifesté de l'intérêt pour votre développement, 
                                    vous savez donc que vous dépensez judicieusement votre budget de publicité et de marketing.
                                </i>
                            </p>

                            <h2 className='sectionTitle meduim  small'> 
                                Développement de site web
                            </h2>
                            <p>
                                Pour présenter votre projet en ligne et permettre aux acheteurs potentiels 
                                de manifester leurs intérêts, nos équipes de conception graphique et de conception Web vont 
                                collaborer avec vous pour concevoir et créer un site Web entièrement mobile qui comprend  des infographies personnalisées, 
                                des plans d'étage et des cartes mettant en évidence les commodités locales, et informations clés.
                            </p>

                            <h2 className='sectionTitle meduim small'> 
                                Impression
                            </h2>
                            <p>
                                Nous pouvons fournir une suite complète d'actifs imprimés, y compris des dossiers, des 
                                brochures et des bannières pour aider à consolider 
                                davantage votre marque en tant que promoteur immobilier de premier ordre sur le marché. 
                            </p>

                            <h2 className='sectionTitle meduim small'> 
                                Construire une base de « FAN »
                            </h2>
                            <p>
                                Pour atteindre le public cible, nous pouvons concevoir une présence en ligne sur les réseaux sociaux 
                                avec des campagnes publicitaires géographiquement ciblées qui peuvent atteindre votre audience  cible de manière durable et rentable. 
                            </p>

                            <p>
                                    Faite vous découvrir par plus de personnes et réalisez plus de ventes lorsque vous utilisez nos services de marketing numérique. 
                                    Nous faisons TOUT pour vous afin que vous puissiez obtenir de meilleurs résultats tout en économisant du temps et de l'argent
                            </p>


                        </div>
                        <div className='col-lg-12 mt-5'>
                            <div className='text-center'>
                                <p className='bold-text'>
                                    Commencez dès aujourd'hui en nous contactant pour planifier une consultation gratuite pour votre  projet de promotion immobilière.
                                </p>
                                <Link to={{pathname: '/contact', search: `subject=${encodeURI('commercialisation de projet')}`}}> 
                                    <button className='link_text'> 
                                        {/* OUI! Je suis prêt à atteindre ma liberté financière grâce aux investissements immobiliers aujourd’hui  */}
                                        Contactez-Nous Maintenant
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProjectMarketing;