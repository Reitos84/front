import {useState} from "react";
import loader from "../assets/loader/785.svg";
import ContactService from '../../services/api/ContactService';
import {useHistory, useLocation} from "react-router-dom";
import {CheckCircleOutlined} from "@material-ui/icons";

const ContactPage = () => {

    document.title = "Contact";
    const contactService = new ContactService();

    const params = new URLSearchParams(useLocation().search);

    const [load, setLoad] = useState(false);
    const [person, setPerson] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [subject, setSubject] = useState(params.get('subject') ? params.get('subject') : '' );
    const history = useHistory();


    const send = () =>{
        if([
            person,
            name,
            email,
            subject,
            message
        ].every(x => x.length > 0)
        ){
            setLoad(true);
            contactService.post({
                person: person,
                name: name,
                email: email,
                subject: subject,
                message: message
            }).then((response) => {
                if(response.status ===201){
                    setSuccess(true);
                }else{
                    setErrorMessage('Une erreur s\'est produite. Essayez à nouveau');
                }
                setLoad(false);
            }).catch((error) =>{
                setErrorMessage('Une erreur s\'est produite. Essayez à nouveau');
                setLoad(false);
            });
        }else{
            setErrorMessage('Veillez à bien remplir le formulaire');
            setLoad(false);
        }
    }

    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a été effectué. Nous allons vous contacter pour la suite.</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form">
                <div className={"int-title"}><h1>Contactez-nous</h1></div>
                <div className={"form-content"}>
                    <div className="new_form_styles">
                        <div className="">
                            <div className="b-radios">
                                <p className="form-intro">Je suis un : </p>
                                <div className="radios-container">
                                    <div className="radio__box">
                                        <input onChange={(e) => setPerson(e.currentTarget.value)} type="radio" id="proprietaire" name="contact" value="proprietaire"/>
                                        <label>Propriétaire (Vendre ou Louer mon bien)</label>
                                    </div>
                                    <div className="radio__box">
                                    <input onChange={(e) => setPerson(e.currentTarget.value)} type="radio" id="acheteur"
                                        name="contact" value="acheteur"/>
                                        <label>Un acheteur</label>
                                    </div>
                                    <div className="radio__box">
                                        <input onChange={(e) => setPerson(e.currentTarget.value)} type="radio" id="locataire" name="contact" value="locataire"/>
                                        <label>Un  locataire</label>
                                    </div>
                                    <div className="radio__box">
                                        <input onChange={(e) => setPerson(e.currentTarget.value)} type="radio" id="agent" name="contact" value="agent"/>
                                        <label>Je suis un agent / je veux devenir agent</label>
                                    </div>
                                    <div className="radio__box">
                                        <input onChange={(e) => setPerson(e.currentTarget.value)} type="radio" id="agent" name="contact" value="particulier"/>
                                        <label>Un particulier</label>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div className="form_row">
                            <div className={"box"}>
                                <label>Sujet</label> 
                                <input onChange={(e) => setSubject(e.currentTarget.value)} value={subject} type="text"/>
                            </div>
                            <div className={"box"}>
                                <label>Nom</label> 
                                <input onChange={(e) => setName(e.currentTarget.value)} value={name} type="text"/>
                            </div>
                            <div className={"box"}>
                                <label>Email</label>
                                <input onChange={(e) => setEmail(e.currentTarget.value)} value={email}  type={"email"}/>
                            </div>
                        </div>
                        <div className="form_row">
                            <div className={"box full"}>
                                <label>Saisir un message</label>
                                <textarea onChange={(e) => setMessage(e.currentTarget.value)} value={message} className='textarea-message'></textarea>
                            </div>
                        </div>
                        <div className="form_row">
                            <div className="message">
                                { errorMessage }
                            </div>
                        </div>

                        <div className="form_btn">
                            <div className="field">
                                <div className="inlined-field">
                                    <button onClick={send} className="btn">
                                    { load ? <img src={loader} alt={"Veillez patienter"} /> : '' }
                                    Envoyer
                                    </button>
                                </div>
                            </div>
                        </div>
                   

                    </div>

                </div>
            </div>
        </div>
    )
}

export default ContactPage;