import {
    BathtubOutlined,
    CalendarTodayOutlined,
    RoomOutlined,
    SingleBedOutlined, WeekendOutlined
} from "@material-ui/icons";
import {thousandsSeparators} from "currency-thousand-separator";
import basepath from "../constants/basepath";
import {GOOGLE_MAPS} from "@constants/google_maps_key";
import GoogleMapReact from "google-map-react";
import pined from "@assets/icon/pined.png";
import {useEffect, useState} from "react";
import { WHATWORD_API_KEY } from "@constants/what3word_key";
import poolIcon from "@assets/icon/pool.png";
import balconyIcon from "@assets/icon/balcony.png";
import elevatorIcon from "@assets/icon/elevator.png";
import escalatorIcon from "@assets/icon/escalator.png";
import fountainIcon from "@assets/icon/balcony.png";
import garageIcon from "@assets/icon/garage.png";
import gardenIcon from "@assets/icon/garden.png";
import generatorIcon from "@assets/icon/generator.png";
import libraryIcon from "@assets/icon/library.png";
import officeIcon from "@assets/icon/office.png";
import stepperIcon from "@assets/icon/stepper.png";

const whatApi = require('@what3words/api');
whatApi.setOptions({ key: WHATWORD_API_KEY });

const PropertyScreen = ({data}) => {

    const [coords, setCoords] = useState(null);

    let background = data.pictures[0];
    data.pictures.forEach(property => {
        if(property.isBackground){
            background = property;
        }
    });

    useEffect(() => {
        if(data.what3words){
            whatApi.convertToCoordinates(data.what3words)
                .then(data => setCoords(data.coordinates));
        }
    }, []);

    const defaultProps = {
        center: {
            lat: Number(data.lat),
            lng: Number(data.lng)
        },
        zoom: 17,
        mapTypeId: "roadmap",
        scrollwheel: true,
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    return(
        <div className={"houses-content"}>
            <div className={"house-header"}>
                <div className={"house-path"}>
                    Home &gt; { data.category.title } &gt; { data.type.title }
                </div>
                <div className={"house-title"}>
                    { data.title }
                </div>
                <div className={"house-sub-divisions"}>
                    <div>
                        <div className={"house-address"}>
                            <RoomOutlined />
                            { data.area } { data.address } { data.city }
                        </div>
                        <div className={"sub-divisions-elements"}>
                            <div className={"sub-division"}>
                                <SingleBedOutlined /> { data.bedroom } chambres
                            </div>
                            <div className={"sub-division"}>
                                <BathtubOutlined /> { data.shower } Douche
                            </div>
                            <div className={"sub-division"}>
                                <WeekendOutlined /> { data.saloon } Salon
                            </div>
                            <div className={"sub-division"}>
                                <CalendarTodayOutlined /> Contruit en { data.buildAt }
                            </div>
                        </div>
                    </div>
                    <div className={"house-price"}>
                        <div className={"cost"}>{ thousandsSeparators(data.price) } xof</div>
                        { data.frequency ? <div className={"frequency"}>Location/{ data.frequency }</div> : <></> }
                    </div>
                </div>
            </div>
            <div className={"house-content"}>
                <div className={"house-galery"}>
                    { data.pictures.length > 5 ? <span className={"plus"}>{data.pictures.length - 5}</span> : '' }
                    <div className={"house-picture-big"}>
                        <img src={ basepath + background.url } alt={ background.name } />
                    </div>
                    <div className={"house-pictures-list"}>
                        {data.pictures.map((picture, index) => {
                            if(picture.id !== background.id && index < 5){
                                return <img key={picture.id} src={ basepath + picture.url } alt={ picture.name } />
                            }
                        }) }
                    </div>
                </div>
                <div className={"house-details"}>
                    <div className={"house-title-list"}>
                        <div className={"title active"}>Description</div>
                    </div>
                    <div className={"house-desc"}>
                        { data.description }
                    </div>
                    <div className={"house-title-list"}>
                        <div className={"title"}>Localisation</div>
                    </div>
                    <div className={"map-container"}>
                        { coords ?
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: GOOGLE_MAPS }}
                                center={coords}
                                defaultZoom={defaultProps.zoom}
                            >
                                <img lat={coords.lat} lng={coords.lng} className={"pined"} src={pined} alt={"Property Location"} />
                            </GoogleMapReact> : ''}
                    </div>
                    <div className={"location-details"}>
                        <div className={"sub-details"}>
                            Address
                            <span>{ data.area } { data.address }</span>
                        </div>
                        <div className={"sub-details"}>
                            Ville
                            <span>{ data.city }</span>
                        </div>
                        <div className={"sub-details"}>
                            Commune / Quartier
                            <span>{ data.address }</span>
                        </div>
                        <div className={"sub-details"}>
                            Pays
                            <span>{ data.country }</span>
                        </div>
                    </div>
                    <div className={"house-elements"}>
                        <h3>Détails de la propriété</h3>
                        <div className={"location-details"}>
                            <div className={"sub-details"}>
                                Identifiant
                                <span>H{ data.id }</span>
                            </div>
                            <div className={"sub-details"}>
                                Prix
                                <span>{ thousandsSeparators(data.price) } xof</span>
                            </div>
                            <div className={"sub-details"}>
                                Superficie
                                <span>Cocody</span>
                            </div>
                            <div className={"sub-details"}>
                                Construit le
                                <span>{ data.buildAt }</span>
                            </div>
                            <div className={"sub-details"}>
                                Chambres
                                <span>{ data.bedroom }</span>
                            </div>
                            <div className={"sub-details"}>
                                Salon
                                <span>{ data.saloon }</span>
                            </div>
                            <div className={"sub-details"}>
                                Cuisine
                                <span>{ data.kitchen }</span>
                            </div>
                        </div>
                    </div>
                    <div className={"house-title-list"}>
                        <div className={"title"}>Equipements</div>
                    </div>
                    <div className={"house-elements"}>
                        <div className={"location-details"}>
                            <div style={{display: data.pool ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de piscine"} src={poolIcon} />
                                Piscine
                            </div>
                            <div style={{display: data.balcony ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de balcon"} src={balconyIcon} />
                                Balcon
                            </div>
                            <div style={{display: data.gymnasium ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de gymnase"} src={stepperIcon} />
                                Gymnase
                            </div>
                            <div style={{display: data.garden ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de jardin"} src={gardenIcon} />
                                Jardin
                            </div>
                            <div style={{display: data.generator ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de groupe électronique"} src={generatorIcon} />
                                Groupe électrogène
                            </div>
                            <div style={{display: data.fountain ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de fontaine"} src={fountainIcon} />
                                Fontaine
                            </div>
                            <div style={{display: data.garage ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de Garage"} src={garageIcon} />
                                Garage
                            </div>
                            <div style={{display: data.office ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de bureau"} src={officeIcon} />
                                Bureau
                            </div>
                            <div style={{display: data.stair ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone d'escalier"} src={escalatorIcon} />
                                Escalier
                            </div>
                            <div style={{display: data.elevator ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone d'ascenceur"} src={elevatorIcon} />
                                Ascenceur
                            </div>
                            <div style={{display: data.library ? "block" : "none"}} className={"l-details-equip"}>
                                <img alt={"Icone de bibliothèque"} src={libraryIcon} />
                                Bibliothèque
                            </div>
                        </div>
                    </div>
                    <div className={"house-title-list"}>
                        <div className={"title"}>Voisinage</div>
                    </div>
                    <div className={"house-elements"}>
                        <div className={"location-details"}>
                            <div className={"voisin"}>
                                <div>Ecole Jule Ferry à 300m</div>
                            </div>
                            <div className={"voisin"}>
                                <div>Hopital Polyclinique sacré coeur à 500m</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export  default PropertyScreen;