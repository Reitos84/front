import {CheckCircleOutlined} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {useState} from "react";
import loader from "../assets/loader/785.svg";
import {useSelector} from "react-redux";
import MeetupService from "../../services/api/MeetupService";

const Meetup = () => {

    const user = useSelector(state => state.auth.data);

    const [success, setSuccess] = useState(false);
    const [name, setName] = useState(user ? user.name : null);
    const [email, setEmail] = useState(user ? user.email : null);
    const [contact, setContact] = useState(user ? user.mobilephone : null);
    const [reason, setReason] = useState(null);
    const [load, setLoad] = useState(false);
    const [message, setMessage] = useState('');

    const meetupService = new MeetupService();

    const handleSend = () => {
        if([name, email, contact, reason].every(x => x)){
            setMessage('');
            setLoad(true);
            meetupService.post({
                name: name,
                email: email,
                reason: reason,
                contact: contact
            }).then(result => {
                if(result.status === 201){
                    setSuccess(true);
                    setLoad(false);
                }else{
                    setMessage('Une erreur s\'est produite, essayez a nouveau');
                }
            }).catch(err => {
                setMessage('Une erreur s\'est produite, essayez a nouveau');
            })
        }else{
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a été effectué. Nous allons vous contacter pour la suite.</div>
                    <Link to={''} className={"btn"}>Continuer</Link>
                </div>
            </div>
            <div className="form">
                <div className={"int-title"}>Demander un rendez-vous</div>
                <div className={"form-content"}>
                    <p>
                        Ici vous pouvez demander une rencontre avec nos experts immobilier. Veillez a bien renseigner le motif pour nous aider a repondre a votre preocupation.
                    </p>
                    <div className={"box"}>
                        <label>Motif de la visite</label>
                        <textarea onChange={(e) => setReason(e.currentTarget.value)} value={reason}></textarea>
                    </div>
                    <div className={"box"}>
                        <label>Nom et Prénom(s)</label>
                        <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                    </div>
                    <div className={"box"}>
                        <label>Email</label>
                        <input onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"text"} />
                    </div>
                    <div className={"box"}>
                        <label>Contact</label>
                        <input onChange={(e) => setContact(e.currentTarget.value)} value={contact} type={"text"} />
                    </div>
                    <div className={"field"}>
                        <label></label>
                        <div className={"inlined-field"}>
                            <div className="message">
                                { message }
                            </div>
                        </div>
                    </div>
                    <div className={"field"}>
                        <label></label>
                        <div className={"inlined-field"}>
                            <button onClick={handleSend} className={"btn"}>
                                {
                                    load ? <img src={loader} alt="Chargement" /> : ''
                                }
                                Envoyer votre demande
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Meetup;