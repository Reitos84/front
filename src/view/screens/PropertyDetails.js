import {
    BathtubOutlined,
    RoomOutlined,
    SingleBedOutlined, 
    WeekendOutlined,
    SchoolOutlined,
    LocalPharmacyOutlined,
    HealingOutlined,
    LocalGroceryStoreOutlined,
    ShareOutlined,
    FavoriteBorderOutlined,
    CloseOutlined,
    FlagOutlined
} from "@material-ui/icons";
import {thousandsSeparators} from "currency-thousand-separator";
import GoogleMapReact from "google-map-react";
import {useEffect, useState} from "react";
import poolIcon from "../assets/icon/pool.png";
import balconyIcon from "../assets/icon/balcony.png";
import elevatorIcon from "../assets/icon/elevator.png";
import escalatorIcon from "../assets/icon/escalator.png";
import fountainIcon from "../assets/icon/balcony.png";
import garageIcon from "../assets/icon/garage.png";
import gardenIcon from "../assets/icon/garden.png";
import generatorIcon from "../assets/icon/generator.png";
import libraryIcon from "../assets/icon/library.png";
import officeIcon from "../assets/icon/office.png";
import stepperIcon from "../assets/icon/stepper.png";
import armchairIcon from "../assets/icon/armchair.png";
import blenderIcon from "../assets/icon/blender.png";
import fridgeIcon from "../assets/icon/fridge.png";
import microwaveIcon from "../assets/icon/microwave.png";
import internetIcon from "../assets/icon/internet.png";
import fan from "../assets/icon/fan.png";
import security from "../assets/icon/home_safety.png";
import cook from "../assets/icon/cook.png";
import what3word from "../assets/logo/what3word.png";
import Geopify from "../../services/places/Geopify";
import {Link} from "react-router-dom";
import '../css/detail.css';
import '@splidejs/splide/dist/css/themes/splide-skyblue.min.css';
import facebookIcon from '../assets/social/facebook.png';
import EmailIcon from '../assets/social/e-mail.png';
import WhatsappIcon from '../assets/social/whatsapp.png';
import LinkedinIcon from '../assets/social/linkedin.png';
import TwitterIcon from '../assets/social/twitter.png';
import TelegramIcon from '../assets/social/telegram.png';
import loader from "../assets/loader/786.gif";
import {
    EmailShareButton,
    FacebookShareButton,
    LinkedinShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,
} from "react-share";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import SlideShow from "../components/SlideShow";
import SimilarProperties from "../components/SimilarProperties";
import nl2br from "react-nl2br";

const geopify = new Geopify();

const PropertyDetails = ({referrer, data, saveMe, handleRequestLogin}) => {

    const [neightborhood, setNeighborhood] = useState('healthcare.clinic_or_praxis');
    const [neighborList, setNeighbordList] = useState([]);
    const [shareModal, setShareModal] = useState(false);
    const [nLoad, setNLoad] = useState(true);
    const [showGalery, setShowGalery] = useState(false);
    const [copied, setCopied] = useState(false);

    let background = data.pictures[0];
    data.pictures.forEach(property => {
       if(property.isBackground){
           background = property;
       }
    });

    useEffect(() => {
        if(data.lat && data.lng){
            setNLoad(true);
            geopify.neightborhood(data.lat, data.lng, neightborhood).then(result => {
                setNeighbordList(result.features);
                setNLoad(false);
            }).catch(() => {
                setNLoad(false);
            })
        }
    }, [neightborhood]);

    const defaultProps = {
        center: {
            lat: Number(data.lat),
            lng: Number(data.lng)
        },
        zoom: 15,
        mapTypeControl: false,
        overviewMapControl: false,
        zoomControl: false,
        draggable: false,
        mapTypeId: "roadmap",
        scrollwheel: false,
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    let params = new URLSearchParams();
    params.append('title', data.title);
    params.append('price', data.price);
    params.append('address', data.area);

    const shareLink = `${process.env.REACT_APP_SHARE_PROPERTY_LINK}${data.code}`;

    const toggleSlide = () => {
        setShowGalery(!showGalery);
    }

    return(
        <div className={"houses-content"}>
            {
                showGalery ?
                    <SlideShow toggleSlide={toggleSlide} images={data.pictures} />
                    :
                    ""
            }
            <div className={shareModal ? "account-mask show" : "account-mask"}>
                <div className={"account-mask-content"}>
                    <CloseOutlined onClick={() => {setShareModal(false); setCopied(false);}} className="m-close" />
                    <h3>Partager avec</h3>
                    <div className="share-side">
                        <WhatsappShareButton 
                            title={"Salut, j'ai vu cette annonce sur Abidjan Habitat.\nElle pourrait vous plaire.\n"}
                            url={shareLink}
                            >
                            <img alt="share with facebook" src={WhatsappIcon} />
                        </WhatsappShareButton>
                        <EmailShareButton
                            subject={data.title} 
                            body={
                                "Salut, j'ai vu cette annonce sur Abidjan Habitat.\nElle pourrait vous plaire.\n"
                            }
                            url={shareLink}
                            >
                            <img alt="share with facebook" src={EmailIcon} />
                        </EmailShareButton>
                        <FacebookShareButton 
                            url={shareLink}
                            quote={data.title}
                            hashtag={`#AbidjanHabitat`}
                            >
                            <img alt="share with facebook" src={facebookIcon} />
                        </FacebookShareButton>
                        <TwitterShareButton 
                            title={data.title}
                            via={'AbidjanHabitat'}
                            hashtags={['AbidjanHabitat', 'immobilier', `${data.type.title}`]}
                            url={shareLink}
                            >
                            <img alt="share with facebook" src={TwitterIcon} />
                        </TwitterShareButton>
                        <LinkedinShareButton
                            title={data.title}
                            summary={'Immobilier'}
                            source={'AbidjanHabitat'}
                            url={shareLink}
                            >
                            <img alt="share with facebook" src={LinkedinIcon} />
                        </LinkedinShareButton>
                        <TelegramShareButton 
                            url={shareLink}
                            title={
                                "Salut, j'ai vu cette annonce sur Abidjan Habitat.\nElle pourrait vous plaire.\n"
                            } 
                            >
                            <img alt="share with facebook" src={TelegramIcon} />
                        </TelegramShareButton>
                    </div>
                    <div className="share-input">
                        <input type={"text"} value={shareLink} />
                        <CopyToClipboard text={shareLink} onCopy={() => setCopied(true)} >
                            <button className={copied ? "copy" : ""}>
                                {
                                    copied ? "Copié" : "Copier"
                                }
                            </button>
                        </CopyToClipboard>
                    </div>
                </div>
            </div>
            <div className={"house-header"}>
                <div className={"path"}>
                    Propriété |
                    <Link to={{pathname: `/properties/${data.category.slug}`}}> { data.category.title }</Link> |
                    <Link to={{pathname: `/properties/${data.category.slug}?type=${data.type.slug}`, search: ''}}> { data.type.title }</Link>
                </div>
                <div className={"house-sub-divisions"}>
                    <div>
                        <div className={"house-title"}>
                            { data.title }
                        </div>
                        <div className={"house-address"}>
                            <RoomOutlined />
                            { data.area } { data.address } { data.city }
                        </div>
                        <div className={"sub-divisions-elements"}>
                            {
                                data.bedroom ?
                                    <div className={"sub-division"}>
                                        <SingleBedOutlined /> 
                                        { data.bedroom } chambre{ data.bedroom > 1 ? 's' : '' }
                                    </div>
                                :
                                    ""
                            }
                            {
                                data.shower ?
                                    <div className={"sub-division"}>
                                        <BathtubOutlined /> 
                                        { data.shower } douche{ data.shower > 1 ? 's' : '' }
                                    </div>
                                :
                                    ""
                            }
                            {
                                data.saloon ?
                                    <div className={"sub-division"}>
                                        <WeekendOutlined /> 
                                        { data.saloon } salon{ data.saloon > 1 ? 's' : '' }
                                    </div>
                                :
                                    ""
                            }
                        </div>
                    </div>
                    <div className={"house-price"}>
                        <div className="event">
                            <div className="what3word">
                                <a rel="noreferrer" target={"_blank"} href={`https://w3w.co/${data.what3words}`}>
                                    <img alt="localisation-exacte" src={what3word} />
                                    <span>{ data.what3words }</span>
                                </a>
                            </div>
                            <div className="event-element">
                                <div className="icon">
                                    <span onClick={() => setShareModal(true)}>
                                        <ShareOutlined />
                                    </span>
                                     Partager
                                </div>
                                <div className={data.iSaved ? "icon liked" : "icon"}>
                                    <span onClick={saveMe}>
                                        <FavoriteBorderOutlined />
                                    </span>
                                     Sauvegarder
                                </div>
                            </div>
                        </div>
                        <div className={"cost"}>{ thousandsSeparators(data.price) } xof</div>
                        { data.frequency ? <div className={"frequency"}>Location/{ data.frequency }</div> : <></> }
                    </div>
                </div>
            </div>
            <div className={"house-content"}>
                {
                    data.pictures.length > 0 ?
                        <div className={"house-galery"}>
                            { data.pictures.length > 5 ? <span onClick={toggleSlide} className={"plus"}>{data.pictures.length - 5}</span> : '' }
                            <div className={"house-picture-big"}>
                                <img onClick={toggleSlide} src={ process.env.REACT_APP_BASE_PATH + background.url } alt={ background.name } />
                            </div>
                            <div className={"house-pictures-list"}>
                                {data.pictures.map((picture, index) => {
                                    if(picture.id !== background.id && index < 5){
                                        return <div><img onClick={toggleSlide} key={picture.id} src={ process.env.REACT_APP_BASE_PATH + picture.url } alt={ picture.name } /></div>
                                    }
                                    return false;
                                }) }
                            </div>
                        </div>
                        :
                        <></>
                }
                <div className={"house-details"}>
                    <div className="house-body">
                        <div className={"house-title-list"}>
                            <div className={"title selected"}>Description</div>
                        </div>
                        <div className={"house-desc"}>
                            { nl2br(data.description) }
                        </div>
                        <div className={"house-title-list"}>
                            <div className={"title"}>Localisation et voisinage</div>
                        </div>
                        <div className={"map-container"}>
                            <span className="approx">Localisation appriximative</span>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
                                center={defaultProps.center}
                                defaultZoom={defaultProps.zoom}
                            >
                                <span draggable={false} lat={data.lat} lng={data.lng} className={"pined"} >
                                    <span />
                                </span>
                            </GoogleMapReact>
                        </div>
                        <div className="voisinage">
                            <div className="type">
                                <span onClick={() => setNeighborhood('healthcare.clinic_or_praxis')} className={neightborhood === 'healthcare.clinic_or_praxis' ? 'selected' : ''}>
                                    <HealingOutlined />
                                    <span className="svg_none">Hopital</span>
                                </span>
                                <span onClick={() => setNeighborhood('healthcare.pharmacy')} className={neightborhood === 'healthcare.pharmacy' ? 'selected' : ''}>
                                    <LocalPharmacyOutlined />
                                    <span className="svg_none">Pharmacie</span>
                                </span>
                                <span onClick={() => setNeighborhood('education.school')} className={neightborhood === 'education.school' ? 'selected' : ''}>
                                    <SchoolOutlined />
                                    <span className="svg_none">Education</span>
                                </span>
                                <span onClick={() => setNeighborhood('commercial.supermarket')} className={neightborhood === 'commercial.supermarket' ? 'selected' : ''}>
                                    <LocalGroceryStoreOutlined />
                                    <span className="svg_none"> Supermarché </span>
                                </span>
                            </div>
                            <div className="neighbord-list">
                                {
                                    nLoad ?
                                        <>
                                            <img alt={"chargement"} className={"loader"} src={loader} />
                                        </>
                                    :
                                        neighborList.map(location => location.properties.name ? <div>
                                            <span>{ location.properties.name }</span>
                                            <span>{ location.properties.distance }m</span>
                                        </div> : '')
                                }
                            </div>
                        </div>
                        <div className={"location-details"}>
                            <div className={"sub-details"}>
                                Address
                                <span>{ data.area } { data.address }</span>
                            </div>
                            <div className={"sub-details"}>
                                Ville
                                <span>{ data.city }</span>
                            </div>
                            <div className={"sub-details"}>
                                Commune / Quartier
                                <span>{ data.address }</span>
                            </div>
                            <div className={"sub-details"}>
                                Pays
                                <span>{ data.country }</span>
                            </div>
                        </div>
                        <div className={"house-elements"}>
                            <h3>Détails de la propriété</h3>
                            <div className={"location-details"}>
                                <div className={"sub-details"}>
                                    Identifiant
                                    <span>{ data.code }</span>
                                </div>
                                <div className={"sub-details"}>
                                    Prix
                                    <span>{ thousandsSeparators(data.price) } xof</span>
                                </div>
                                {
                                    data.floor ?
                                        <div className={"sub-details"}>
                                            Nombre d'étage
                                            <span>{ data.floor }</span>
                                        </div>
                                    :
                                        <></>
                                }
                                {
                                    data.appartements ?
                                        <div className={"sub-details"}>
                                            Nombre d'appartement
                                            <span>{ data.appartements }</span>
                                        </div>
                                        :
                                        <></>
                                }
                                {
                                    data.officePosition ?
                                        <div className={"sub-details"}>
                                            Position du bureau
                                            <span>{ data.officePosition === "down" ? "Au rez de chaussée" : "A l'étage"  }</span>
                                        </div>
                                        :
                                        <></>
                                }
                                {
                                    data.space ?
                                        <div className={"sub-details"}>
                                            Superficie
                                            <span>{ data.space }m<sup>2</sup></span>
                                        </div>
                                        :
                                        <></>
                                }
                                {
                                    data.buildAt ?
                                        <div className={"sub-details"}>
                                            Construit en
                                            <span>{ data.buildAt }</span>
                                        </div>
                                    :
                                        <></>
                                }
                                {
                                    data.landStatus ?
                                        <div className={"sub-details"}>
                                            Type de document
                                            <span>{ data.landStatus }</span>
                                        </div>
                                    :
                                        <></>
                                }
                                {
                                    data.type.slug === 'appartment' ||
                                    data.type.slug === 'villa' ||
                                    data.type.slug === 'house' ?
                                    <>
                                        <div className={"sub-details"}>
                                            Chambres
                                            <span>{ data.bedroom }</span>
                                        </div>
                                        <div className={"sub-details"}>
                                            Salon
                                            <span>{ data.saloon }</span>
                                        </div>
                                        <div className={"sub-details"}>
                                            Cuisine
                                            <span>{ data.kitchen }</span>
                                        </div>
                                    </>
                                    :
                                    <></>
                                }

                            </div>
                        </div>
                        <div className={"house-title-list"}>
                            <div className={"title"}>Caractéristiques</div>
                        </div>
                        <div className={"house-elements"}>
                            <div className={"location-details"}>
                                <div style={{display: data.pool ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de piscine"} src={poolIcon} />
                                    Piscine
                                </div>
                                <div style={{display: data.balcony ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de balcon"} src={balconyIcon} />
                                    Balcon
                                </div>
                                <div style={{display: data.gymnasium ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de gymnase"} src={stepperIcon} />
                                    Gymnase
                                </div>
                                <div style={{display: data.garden ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de jardin"} src={gardenIcon} />
                                    Jardin
                                </div>
                                <div style={{display: data.fan ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de jardin"} src={fan} />
                                    Ventilation
                                </div>
                                <div style={{display: data.cook ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de jardin"} src={cook} />
                                    Cuisiniere
                                </div>
                                <div style={{display: data.security ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de jardin"} src={security} />
                                    Security
                                </div>
                                <div style={{display: data.generator ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de groupe électronique"} src={generatorIcon} />
                                    Groupe électrogène
                                </div>
                                <div style={{display: data.fountain ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de fontaine"} src={fountainIcon} />
                                    Fontaine
                                </div>
                                <div style={{display: data.garage ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de Garage"} src={garageIcon} />
                                    Garage
                                </div>
                                <div style={{display: data.office ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de bureau"} src={officeIcon} />
                                    Bureau
                                </div>
                                <div style={{display: data.stair ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone d'escalier"} src={escalatorIcon} />
                                    Escalier
                                </div>
                                <div style={{display: data.elevator ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone d'ascenceur"} src={elevatorIcon} />
                                    Ascenceur
                                </div>
                                <div style={{display: data.library ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de bibliothèque"} src={libraryIcon} />
                                    Bibliothèque
                                </div>
                            </div>
                        </div>
                        {
                            data.type.slug === 'rent' ?
                            <>
                                <div className={"house-title-list"}>
                                    <div className={"title"}>Equipements</div>
                                </div>
                                <div style={{display: data.refrigerator ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de refrigerateur"} src={fridgeIcon} />
                                    Refrigérateur
                                </div>
                                <div style={{display: data.microwave ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de micro-onde"} src={microwaveIcon} />
                                    Micro-onde
                                </div>
                                <div style={{display: data.internet ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone d'internet"} src={internetIcon} />
                                    Accès Internet
                                </div>
                                <div style={{display: data.armchair ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de fautueil"} src={armchairIcon} />
                                    Armchair
                                </div>
                                <div style={{display: data.blender ? "block" : "none"}} className={"l-details-equip"}>
                                    <img alt={"Icone de mixeur"} src={blenderIcon} />
                                    Mixeur
                                </div>
                            </>
                                :
                            <></>
                        }
                        <SimilarProperties handleRequestLogin={handleRequestLogin} data={data} />
                    </div>
                    <div className="agent-contact">
                        <div className="agent-block">
                            <div className="agency-pointed">
                                {
                                    data.agency.avatarUrl ?
                                        <img alt={""} src={process.env.REACT_APP_BASE_PATH + data.agency.avatarUrl} />
                                    :
                                        <div style={{background: data.agency.color}} className="avatar">
                                            {
                                                data.agency.name.charAt(0)
                                            }
                                        </div>
                                }
                                <div>
                                    <span className="name">{ data.agency.name }</span>
                                    <span>Agence Immobilière</span>
                                </div>
                            </div>
                            <div>
                                { data.agency.localisation }
                            </div>
                            <Link to={`/agency/${data.agency.name}/${data.agency.id}`}>Plus de propriétés pour cet agence </Link>
                            {
                                !referrer ?
                                <>
                                    <a className="link call" href={`tel:${data.agency.mobilephone}`}>Contacter maintenant</a>
                                    <Link className="link" to={{pathname: `/interest/${data.code}/${data.slug}/${data.agency.name}`, search: params.toString()}}>Envoyer un intérêt</Link>
                                </>
                                :
                                <></>
                            }
                        </div>
                        {/*
                         <div className="ads">
                            Publicité
                        </div>
                         */}
                        <div className="error-report">
                            <Link to={{pathname: `/error-report/${data.code}/${data.slug}`, search: params.toString()}}><FlagOutlined /> Reporter une erreur avec cette propriété</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export  default PropertyDetails;