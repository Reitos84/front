import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import imageTwoo from "../assets/page/represntation-vente.png";
import defaultImage from "../assets/page/inves-banner.jpg";
// import 'App.css';

const SellerRepresentation = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Représentations de ventes</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className='container-fluid'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-4'>
                            <img alt="investisement banner"  className="img-fluid inves-img none img-heigth" src={imageTwoo }/>
                        </div>
                        <div className='col-lg-8'>
                            <div>
                                <p>
                                   Lorsque vous décidez d'engager les services d’Abidjan Habitat pour vendre votre propriété, 
                                   vous nous encourager à entrer dans une relation de représentation de vendeur. 
                                   C’est en cette capacité que nous pouvons vous offrir la forme d'obligations la plus élevée, 
                                   c'est-à-dire un devoir de diligence dans tout ce que nous faisons pour vous, ainsi que des
                                    obligations générales et fiduciaires, et de ce fait mieux protéger   vos intérêts.

                                </p>
                                <p>
                                    Plus important encore, comme vous le verrez ci-dessous, 
                                    le contrat d'inscription fournit des autorisations spécifiques 
                                    du vendeur qui nous permettent d'utiliser les informations sur la propriété et les outils 
                                    de marketing pour vendre la propriété. Sans autorisation, 
                                    nous ne serions pas en mesure de commercialiser et de vendre avec succès un bien.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-fluid  bg-color'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <h2 className='sectionTitle'> 
                                <span className='smallTitle'>Objectifs</span>
                                <span>Abidjan habitat</span> 
                            </h2>
                            <p>
                                Cette relation est formalisée dans une convention d'inscription. 
                                Le but principal de cette entente est de conférer à la société immobilière 
                                le pouvoir d'agir en votre nom pour offrir une propriété à vendre selon les termes et conditions prévus pour la représentation.
                                La convention d'inscription remplit plusieurs fonctions
                                <br/>
                                Établit la relation (juridique) vendeur/agence immobilière <br/>
                                Les Grandes lignes:
                            </p>
                            <ul className='club-list'>
                                <li>Services en cours d'exécution.</li>
                                <li>Obligations des parties.</li>
                                <li>Limitations définies de l'autorité de l'agent.</li>
                                <li>Fournit les spécificités des propriétés pour la distribution papier et électronique.</li>
                                <li>Réalisez un bon retour sur investissement de vos transactions immobilières</li>
                                <li>Fournit des informations pour les négociations et la rédaction des offres. </li>
                            </ul>
                        </div>
                        <div className='col-lg-12'>
                            <p className='mt-3'>
                                Lorsque l'on conclut un accord de représentation du vendeur, l'autorité peut être exclusive ou non.
                                Un accord exclusif : <br/>
                                &nbsp;&nbsp;-Implique de donner à Abidjan Habitat le droit exclusif de vendre la propriété décrite selon les termes de l'accord de représentation.<br/>
                                &nbsp;&nbsp;-Si la propriété est vendue, l’agence immobilière reçoit une commission et elle a aussi la possibilité de coopérer avec d'autres agences immobilières.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-fluid'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <div className='text-center'>
                                <h2 className='sectionTitle'> 
                                    <span className='smallTitle texte-center'>Les éléments clés du contrat </span> 
                                    <span>de représentation du vendeur</span>
                                </h2>
                            </div>
                        </div>
                        
                    </div>
                    <div className='element-container mt-3'>
                        <div className='ele-card'>
                            <h4>Autorité</h4>
                            <p>
                                Accordée et signée au nom de l’agence immobilière.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Durée</h4>
                            <p>
                                Dates et heures de début et de fin du contrat.
                                Initiales du vendeur impose si le contrat s'étend sur plus de 6 mois
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Prix de vente.</h4>
                            <p>
                            Cette clause est mise en gras pour mettre une emphase, car cette garantie confirme que la propriété n'est actuellement pas répertoriée auprès d'autres agences
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Représentation du vendeur/Garantie</h4>
                            <p>
                                Accordée et signée au nom de l’agence immobilière.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Commission</h4>
                            <p>
                                Le vendeur accepte de payer une commission totale qui peut être un pourcentage, un montant convenu ou une combinaison des deux, plus les taxes applicables.
                                Le vendeur paiera, sur demande, toute insuffisance de commission et les taxes dues sur cette commission.
                                Le vendeur paiera une commission, même si la vente n'aboutit pas en raison d'un manquement ou d'une négligence du vendeur.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Rétention</h4>
                            <p>
                                Les conditions de paiement de la commission s'appliquent après l'expiration de l'accord de représentation pour la durée de la période de retenue (60 à 90 jours), à moins que le vendeur ne conclue un nouvel accord de représentation avec une autre agence immobilière, le montant de la commission serait partage et payable à l’agence représentant l’acheteur.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Représentation</h4>
                            <p>
                                Fixe le montant de la commission que le vendeur autorise la maison 
                                de courtage à verser à une société de co-opération avec toute 
                                autre maison de courtage enregistrée.
                                Diverses divulgations et procédures connexes concernant la représentation, 
                                la représentation multiple et le service à la clientèle.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Renvoi des demandes</h4>
                            <p>
                                Le vendeur doit informer immédiatement le courtage de toutes les demandes de renseignements et de toutes les offres d'achat de quelque source que ce soit pendant la durée du contrat.
                                Toute demande de renseignements pendant la période d'inscription entraînant une offre d'achat acceptée pendant l'inscription ou la période d'attente entraînera le paiement par le vendeur de la commission de courtage dans les 5 jours.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Commercialisation</h4>
                            <p>
                                L’agence est autorisée à commercialiser la propriété et à permettre aux acheteurs potentiels d'inspecter entièrement la propriété.
                            </p>
                            <ul>
                                <li>.Peut placer des panneaux «À vendre» et «Vendu» sur la propriété.</li>
                                <li>.Peut identifier la propriété dans la publicité.</li>
                                <li>. Droit unique et exclusif de prendre toutes les décisions publicitaires</li>
                                <li>. Indemnisation pour les actes ou omissions dans la publicité autres que la négligence grave/l'acte délibéré</li>
                            </ul>
                        </div>
                        <div className='ele-card'>
                            <h4>Garantie</h4>
                            <p>
                                Le vendeur a tous les pouvoirs nécessaires pour signer le contrat.
                                Le vendeur a divulgué des intérêts extérieurs / tiers concernant la propriété (par exemple, le premier droit de refus).

                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Indemnisation et assurance</h4>
                            <p>
                                Le vendeur ne tiendra pas l’agence responsable de la perte ou des dommages à la propriété ou au contenu, sauf s'ils ont été causés par une négligence grave ou un acte délibéré du courtier inscripteur.
                                Le vendeur dégagera l’agence immobilière de toute « perte » causée par le vendeur.
                                En outre, le vendeur confirme qu'il a une assurance pour couvrir toute blessure ou tout dommage matériel pouvant survenir.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Indemnisation et assurance</h4>
                            <p>
                                Le vendeur ne tiendra pas l’agence responsable de la perte ou des dommages à la propriété ou au contenu, sauf s'ils ont été causés par une négligence grave ou un acte délibéré du courtier inscripteur.
                                Le vendeur dégagera l’agence immobilière de toute « perte » causée par le vendeur.
                                En outre, le vendeur confirme qu'il a une assurance pour couvrir toute blessure ou tout dommage matériel pouvant survenir.
                            </p>
                    
                        </div>
                        <div className='ele-card'>
                            <h4>Vérification des informations</h4>
                            <p>
                                L’agence immobilière est autorisé à vendre et le vendeur accepte de fournir 
                                d'autres autorisations qui peuvent être nécessaires pour obtenir 
                                des informations de toute autorité de réglementation, gouvernements, 
                                créanciers hypothécaires ou autres, et lesdites autorités sont autorisées 
                                à divulguer toutes les informations nécessaire a l’agence.
                            </p>
                        </div>
                        <div className='ele-card'>
                            <h4>Utilisation et diffusion des informations</h4>
                            <p>
                                Autorisation d'utiliser des informations personnelles et relatives à la propriété pour le marketing, 
                                la publicité et l'inscription sur les plateformes de publicités en tenant compte des lois sur la confidentialité.
                            </p>
                        </div>
                    </div>
                    <div className='center-btn'>
                        <Link to={{pathname: '/contact', search: `subject=${encodeURI('représentations de ventes')}`}}> 
                            <button className='text-center bouton link_text'> 
                                Contactez-nous !
                            </button>
                        </Link>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    )
}

export default SellerRepresentation;