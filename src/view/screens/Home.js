import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Banner from "../components/Banner";
import FeatureSection from "../components/FeatureSection";
import ServesProperties from "../components/ServesProprerties";
import Video from "../components/Video";
import Faq from "../components/Faq";
import Footer from "../components/Footer";
import StepInHome from "../components/StepInHome";

const Home = () => {
    return(
        <div>
            <Banner/>
            <StepInHome />
            <FeatureSection/>
            <ServesProperties/>
            <Video/>
            <Faq/>
            <Footer/>
        </div>    
    )
}

export default Home;