import {useState, useEffect} from "react";
import loader from "../assets/loader/785.svg";
import TypeService from '../../services/api/TypeService';
import EvaluateService from '../../services/api/EvaluateService';
import { useHistory } from "react-router-dom";
import { CheckCircleOutlined } from "@material-ui/icons";
import {useSelector} from "react-redux";

const Evaluate = () => {

    document.title = "Evaluer votre bien immobilier";

    const user = useSelector(state => state.auth.data);

    const [name, setName] = useState(user ? user.name : null);
    const [email, setEmail] = useState(user ? user.email : null);
    const [contact, setContact] = useState(user ? user.mobilephone : null);
    const [city, setCity] = useState('Abidjan');
    const [types, setTypes] = useState([]);
    const [localisation, setLocalisation] = useState('');
    const [address, setAddress] = useState('');
    const [comment, setComment] = useState('');
    const [selectedType, setSelectedType] = useState('');
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [success, setSuccess] = useState(false);

    const typeService = new TypeService();
    const evaluateService = new EvaluateService();

    const history = useHistory();

    useEffect(async () => {
        let data = await typeService.getAll();
        setTypes(data['hydra:member']);
    }, []);

    const sendEvaluate = () => {
        let data = {
            name: name,
            email: email,
            contact: contact,
            city: city,
            localisation: localisation,
            type: selectedType,
            comment: comment,
            address: address
        };

        if(Object.values(data).every(x => x)){
            setLoad(true);
            setMessage('');
            evaluateService.post(data).then(response => {
                setLoad(false);
                if(response.status === 201){
                    setSuccess(true);
                }else{
                    setMessage('Une erreur s\'est produite, essayez à nouveau');
                }
            }).catch(error => {
                setMessage('Une erreur s\'est produite, essayez à nouveau');
                setLoad(false);
            })
        }else{
            setLoad(false);
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    
    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Votre demande a été effectué. Nous allons vous contacter pour la suite.</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form evaluate">
                <div className={"int-title"}><h1>Evaluer votre bien immobilier</h1></div>
                <div className={"form-content evaluate-content"}>
                    <div className="evaluate_text">
                        <h2 className="sectionTitle">
                            <span className="smallTitle">Vous voulez connaitre </span>
                            <span>la valeur marchande de votre bien ?</span>
                        </h2>
                        <p>
                            Il y a une tendance naturelle pour les propriétaires à vouloir travailler avec des agents immobiliers qui présentent
                            une estimation de biens plus élevée que la réalité dans le seul but d’obtenir un contrat de représentation de vente.
                            C'est un piège qu’il faut éviter à tous prix. <br></br>
                            Si quelque chose semble trop beau pour être vrai, c'est généralement parce que c'est le cas. <br></br>
                             Ne vous laissez pas berner par des agents véreux.
                            Si votre maison n'est pas bien évaluée et commercialisée ;
                            vous risquez de perdre l'intérêt des acheteurs potentiels et ne pas vendre ou de finir par accepter
                            une offre vraiment inférieure à ce que vous auriez souhaité. <br></br>
                            Abidjan Habitat, vous proposes une estimation de bien GRATUITE
                            suivant une méthode experte et rigoureuse qui vous permet
                            d’entrer sur le marché avec un prix qui vous donne plus de confiance,
                            de sécurité et d’assurance. <br></br>
                            Bénéficiez de notre offre
                            gratuitement en remplissant le formulaire ci joint dans la mesure de vos possibilités. 
                        </p>
                    </div>
                    {/* <div className="form_element ">
                        <div className="form_part">
                            <div className={"box"}>
                                <label>Ville</label>
                                <input disabled={true} type="text" value={city} />
                            </div>
                            <div className={"box"}>
                                <label>Quartier</label>
                                <input onChange={(e) => setAddress(e.currentTarget.value)} value={address} type={"text"} />
                            </div>
                            <div className={"box"}>
                                <label>Localisation</label>
                                <textarea onChange={(e) => setLocalisation(e.currentTarget.value)} value={localisation}></textarea>
                            </div>
                            <div className={"box"}>
                                <label>Description</label>
                                <textarea onChange={(e) => setComment(e.currentTarget.value)} value={comment}></textarea>
                            </div>
                        </div>
                        <div className="form_part">
                            <div className={"box"}>
                            <label>Type de propriété</label>
                            <select value={selectedType} onChange={(e) => setSelectedType(e.currentTarget.value)}>
                                <option value="">Sélectionner un type</option>
                                {
                                    types.map(data => 
                                        <option value={data['@id']}>{ data.title }</option>    
                                    )
                                }
                            </select>
                            </div>
                            <div className={"box"}>
                                <label>Nom et Prénom(s)</label>
                                <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                            </div>
                            <div className={"box"}>
                                <label>Email</label>
                                <input onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"text"} />
                            </div>
                            <div className={"box"}>
                                <label>Contact</label>
                                <input onChange={(e) => setContact(e.currentTarget.value)} value={contact} type={"text"} />
                            </div>
                            <div className={"field"}>
                                <label></label>
                                <div className={"inlined-field"}>
                                    <div className="message">
                                        { message }
                                    </div>
                                </div>
                            </div>
                            <div className={"field"}>
                                <label></label>
                                <div className={"inlined-field"}>
                                    <button onClick={sendEvaluate} className={"btn"}>
                                        {
                                            load ? <img src={loader} alt="Chargement" /> : ''
                                        }
                                        Envoyer votre demande
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div> */}
                </div>
                <div className="new_form_styles">
                    <div className="form_row">
                        <div className={"box"}>
                            <label>Ville</label>
                            <input disabled={true} type="text" value={city} />
                        </div>
                        <div className={"box"}>
                            <label>Quartier</label>
                            <input onChange={(e) => setAddress(e.currentTarget.value)} value={address} type={"text"} />
                        </div>
                    </div>
                    <div className="form_row">
                        <div className={"box"}>
                            <label>Localisation</label>
                            <textarea onChange={(e) => setLocalisation(e.currentTarget.value)} value={localisation}></textarea>
                        </div>
                        <div className={"box"}>
                            <label>Description</label>
                            <textarea onChange={(e) => setComment(e.currentTarget.value)} value={comment}></textarea>
                        </div>
                    </div>
                    <div className="form_row">
                        <div className={"box"}>
                            <label>Type de propriété</label>
                            <select value={selectedType} onChange={(e) => setSelectedType(e.currentTarget.value)}>
                                <option value="">Sélectionner un type</option>
                                {
                                    types.map(data => 
                                        <option value={data['@id']}>{ data.title }</option>    
                                    )
                                }
                            </select>
                        </div>
                        <div className={"box"}>
                            <label>Nom et Prénom(s)</label>
                            <input onChange={(e) => setName(e.currentTarget.value)} value={name} type={"text"} />
                        </div>
                    </div>

                    <div className="form_row">
                        <div className={"box"}>
                            <label>Email</label>
                            <input onChange={(e) => setEmail(e.currentTarget.value)} value={email} type={"text"} />
                        </div>
                        <div className={"box"}>
                            <label>Contact</label>
                            <input onChange={(e) => setContact(e.currentTarget.value)} value={contact} type={"text"} />
                        </div>
                    </div>
                    <div className="form_row">
                        <div className={"field"}>
                            <div className={"inlined-field"}>
                                <div className="message">
                                    { message }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="form_btn">
                        <div className={"field"}>
                            <div className={"inlined-field"}>
                                <button onClick={sendEvaluate} className={"btn"}>
                                    {
                                        load ? <img src={loader} alt="Chargement" /> : ''
                                    }
                                    Envoyer votre demande
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
        </div>
    )
}

export default Evaluate;