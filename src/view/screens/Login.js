import '../css/login.css';
import {HttpsOutlined, MailOutline, VisibilityOffOutlined, VisibilityOutlined} from "@material-ui/icons";
import {Link} from "react-router-dom";
import googleLogo from "../assets/google.png";
import GoogleLogin from "react-google-login";
import {login, pushToken} from "../../core/reducer/authSlice";
import UserService from "../../services/api/UserService";
import {useState} from "react";
import {useDispatch} from "react-redux";
import loader from "../assets/loader/785.svg";
import FacebookLogin from 'react-facebook-login';
import LogoImage from "../assets/logo/logo2.png";

const Login = () => {

    document.title = 'Accédez à votre compte - Vente, location et gestion de bien immobilier à abidjan';

    const [load, setLoad] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const [showPass, setShowPass] = useState(false);

    const userService = new UserService();
    const dispatch = useDispatch();

    const proceedLoginWithGoogle = (infos) => {
        setLoad(true);
        connectUser(infos.profileObj.email, infos.profileObj.googleId);
    }

    const proceedLoginWithFacebook = (infos) => {
        setLoad(true);
        connectUser(infos.userID, infos.userID);
    }

    const handleConnect = () => {
        if(email.length > 0 && password.length) {
            connectUser(email, password);
        }else{
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    const isAgency = (roles) => {
        return roles.indexOf("ROLE_AGENCY") > -1;
    }

    const connectUser = (username, password) => {
        setLoad(true);
        userService.connect(username, password).then(result => {
            if(result.error){
                setMessage('Votre email ou mot de passe est incorrect');
                setLoad(false);
            }else{
                userService.Provide(result.token).then(data => {
                    if(data.isEnabled !== false && !isAgency(data.roles)){
                        localStorage.setItem('personnal', result.token);
                        localStorage.setItem('personnal_refresh', result.refresh_token);
                        dispatch(login(data));
                        dispatch(pushToken({token: data.token, revalidator: data.refresh_token}));
                    }else{
                        setLoad(false);
                        setMessage('Votre email ou mot de passe est incorrect');
                    }
                });
            }
        });
    }
    
    return(
        <div className={"login-container"}>
            <div className={"split image"}>
                <Link to={""} className={"logo"}>
                    <img src='https://imapi.me/assets/logo/logo2.png' />
                </Link>
            </div>
            <div className={"split form"}>
                <Link to={""} className={"logo mobile"}>
                    <img alt=" banner" className="" src={LogoImage} />
                </Link>
                <div className={"fields"}>
                    <div className={"title"}>
                        Se connecter
                        <span className={"line"} />
                    </div>
                    <div className={"message"}>
                        {message}
                    </div>
                    <div className={"box"}>
                        <MailOutline />
                        <input value={email} name={"email"} onChange={(e) => setEmail(e.currentTarget.value)} type={"email"} placeholder={"Email"} />
                    </div>
                    <div className={"box"}>
                        <HttpsOutlined/>
                        <input value={password} name={"password"} onChange={(e) => setPassword(e.currentTarget.value)} type={showPass ? "text" : "password"} placeholder={"Mot de passe"} />
                        {
                            showPass ?
                                <VisibilityOffOutlined onClick={() => setShowPass(!showPass)} className={'eye'} />
                                :
                                <VisibilityOutlined onClick={() => setShowPass(!showPass)} className={'eye'} />
                        }
                    </div>
                    <Link to={"/reset-password"} className={"forgot"}>
                        Mot de passe oublié ?
                    </Link>
                    <button onClick={handleConnect} className={"submit"}>
                        { load ? <img src={loader} alt={"Veillez patienter"} /> : '' }
                        Se connecter à mon compte
                    </button>
                    {/*
                    <div className={"or"}> Ou </div>
                    <div className={"social"}>
                        <GoogleLogin
                            clientId={process.env.REACT_APP_GOOGLE_LOGIN_CLIENTID}
                            buttonText={"S'inscrire avec google"}
                            cookiePolicy={"single_host_origin"}
                            render={renderProps =>
                                <div onClick={renderProps.onClick} disabled={renderProps.disabled} className={"wide"}>
                                    <img src={googleLogo} alt={"google"} />
                                    <span>Se connecter avec google</span>
                                </div>
                            }
                            onSuccess={proceedLoginWithGoogle}
                        />
                        <FacebookLogin
                            appId={process.env.REACT_APP_FACEBOOK_LOGIN_APP_ID}
                            autoLoad={false}
                            fields="name,email"
                            size="small"
                            textButton=""
                            tag="div"
                            callback={proceedLoginWithFacebook}
                            icon="fa-facebook"
                        />
                    </div>
                    */}
                    <div className={"register"}>
                        Vous n'avez pas de compte ?
                        <Link to={"/register"}>S'inscrire</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;