import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import "../css/service.css"
import { Link } from "react-router-dom";
import defaultImage from "../assets/service-banner.jpg";
import commercialisation  from "../assets/icon/commercialisation-globale.png";
import relocalisation from "../assets/icon/camion-de-demenagement.png";
import investisement from "../assets/icon/investissement.png";
import gestion from "../assets/icon/gestion-de-projet.png";
import vente from "../assets/icon/ventes.png";
// import 'App.css';

const Services = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Nos services</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className='text-center services'>
                <h2 className='sectionTitle  margin-bottom'>
                        Abidjan Habitat vous propose des services divers,
                </h2>
                
                <div className='service_text'>
                    <p>
                        mis en place spécialement pour vous assister et répondre à vos besoins durant toutes vos transactions immobilières. N’hésitez surtout pas à nous contacter si vous avez des questions ou suggestions. 
                    </p>
                </div>
                    
               </div>
            <div className="main-content">
                <div className='services_box'>
                    <div className='services_items'>
                        <img alt='commercialisation' src={commercialisation } className="service-picto"/>
                        <h3>Commercialisation de Projet</h3>
                         <p>
                            L'immobilier est l'un des outils les plus puissants de création ...
                         </p>

                         <Link to="/project-marketing"><button className='service-link'>Savoir plus</button></Link>
                    </div>
                    <div className='services_items'>
                    <img alt='commercialisation' src={relocalisation} className="service-picto"/>
                        <h3>Services de relocalisation</h3>
                         <p>
                            Le déménagement peut être une période stressante pour tout le monde...
                         </p>

                         <Link to="/relocation-service"><button className='service-link'>Savoir plus</button></Link>
                    </div>
                    <div className='services_items'>
                    <img alt='investissement' src={investisement} className="service-picto"/>
                        <h3>Investissement Immobilier</h3>
                         <p>
                            L'immobilier est l'un des outils les plus puissants de création de richesse...
                         </p>

                         <Link to="/investissements"><button className='service-link'>Savoir plus</button></Link>
                    </div>
                    <div className='services_items'>
                    <img alt='investissement' src={gestion} className="service-picto"/>
                        <h3>Gestion immobilière</h3>
                         <p>
                         Pour éviter le stress et les mauvaises surprises, Confiez la gestion de vos biens immobiliers...
                         </p>

                         <Link to="/property-management"><button className='service-link'>Savoir plus</button></Link>
                    </div>
                    <div className='services_items'>
                    <img alt='investissement' src={vente} className="service-picto"/>
                        <h3>Représentations de ventes</h3>
                         <p>
                            Lorsque vous décidez d'engager les services d’Abidjan Habitat pour vendre...
                         </p>

                         <Link to={"/representation-sales"} ><button className='service-link'>Savoir plus</button> </Link>
                    </div>
                </div>
                <Link to={'/contact'} className="services_contact"> 
                    <button className='link_text'> 
                        Contactez-Nous Maintenant
                    </button>
                </Link>
            </div>
        </div>
    )
}

export default Services;