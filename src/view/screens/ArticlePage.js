import '../css/article-page.css';
import BlogCategories from '../components/Blog/BlogCategories';
import { Schedule } from '@material-ui/icons';
import { Link } from 'react-router-dom';

const ArticlePage = () => {
    return(
        <div className="blog">
            <div className='current'>
                <div className='article-content'>
                    <img src='https://images.pexels.com/photos/1732414/pexels-photo-1732414.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2' />
                    <div className='titles'>
                        <div className="title">
                            Which green features do homes with high EPC ratings have?
                        </div>
                        <div className='t-bottom'>
                            <div>
                                <Link to={''} className='tags'>
                                    Proprietes de reve
                                </Link>
                                <div className="date">
                                    <Schedule />
                                    25 janvier 2022
                                </div>
                            </div>
                            <div className='shares'>
                                <div className='s-tit'>
                                    Partager
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='article-body'>
                    <p>Lorem ipsum dolor sit amet. Non minima accusamus ex fugit fugiat sed ipsam iste! Est modi provident eos consequatur modi est adipisci velit. Ea earum esse ut error natus eum enim quibusdam. Sit quibusdam molestiae non incidunt autem et quae molestiae qui voluptate eligendi non unde eaque. </p>
                    <p>Eos excepturi saepe vel harum odio et dicta velit. Non natus magnam aut quas omnis in explicabo esse et quidem odio  molestiae autem. </p>
                    <p>A voluptatem voluptatem aut libero error et porro cupiditate est odit vero! Et quidem quia id libero voluptatem quo officia autem eos maiores perspiciatis At neque delectus. In quod quia ea ullam iusto ea fugit autem hic quas voluptas 33 dolores aspernatur. Qui voluptatem magnam a deserunt culpa aut enim obcaecati est quia laboriosam. </p>
                    <p>A voluptatem voluptatem aut libero error et porro cupiditate est odit vero! Et quidem quia id libero voluptatem quo officia autem eos maiores perspiciatis At neque delectus. In quod quia ea ullam iusto ea fugit autem hic quas voluptas 33 dolores aspernatur. Qui voluptatem magnam a deserunt culpa aut enim obcaecati est quia laboriosam. </p>
                    <p>A voluptatem voluptatem aut libero error et porro cupiditate est odit vero! Et quidem quia id libero voluptatem quo officia autem eos maiores perspiciatis At neque delectus. In quod quia ea ullam iusto ea fugit autem hic quas voluptas 33 dolores aspernatur. Qui voluptatem magnam a deserunt culpa aut enim obcaecati est quia laboriosam. </p>
                    </div>
                </div>
            </div>
            <div className='split-search'>
                <div className='blocked'>
                    <div className='search'>
                        <input type={"text"} placeholder="Rechercher" />
                        <button>Rechercher</button>
                    </div>
                    <BlogCategories />
                    <div className='categories-side'>
                        <div className='title'>
                            Nous suivre
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ArticlePage;