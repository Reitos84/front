import {useEffect, useRef, useState} from "react";
import ResetPasswordService from "../../services/api/ResetPasswordService";
import {Link, useLocation} from "react-router-dom";
import {HttpsOutlined} from "@material-ui/icons";
import loader from "../assets/loader/785.svg";
import bigLoader from "../assets/loader/786.gif";
import expired from "../assets/expired.png";
import LogoImage from "../assets/logo/logo2.png";

export function useQuery(url) {
    return new URLSearchParams(url);
}

const Password = () => {

    document.title = 'Nouveau mot de passe - Vente, location et gestion de bien immobilier à abidjan';

    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [error, setError] = useState(false);
    const [bigLoad, setBigLoad] = useState(true);
    const [successMessage, setSuccessMessage] = useState('');

    const resetPasswordService = new ResetPasswordService();

    const params = useQuery(useLocation().search);

    useEffect(() => {
        resetPasswordService.verifyParams({
            resetkey: params.get('hash'),
            selector: params.get('u')
        }).then(data => {
            if(!data.success){
                setError(true);
            }
            setBigLoad(false);
        })
    }, []);

    const handleChangePassword = (event) => {
        setPassword(event.currentTarget.value);
    }

    const handleChangeConfirmPassword = (event) => {
        setConfirmPassword(event.currentTarget.value);
    }

    const loginRef = useRef();

    const changePassword = () => {
        if(password === confirmPassword){
            setMessage('');
            setLoad(true);
            resetPasswordService.changePassword({
                password: password,
                resetkey: params.get('hash'),
                selector: params.get('u')
            }).then(data => {
                if(data.success){
                    setSuccessMessage('Votre mot de passe a été modifier');
                    setLoad(false);
                    setTimeout(() => {
                        loginRef.current.click();
                    }, 5000);
                }
            })
        }else{
            setMessage('Les mots de passe ne correspondent pas')
        }
    }

    return(
        <div className={"login-container"}>
            <div className={"split image"}>
                <Link to={""} className={"logo"}>
                    <img src='https://imapi.me/assets/logo/logo2.png' />
                </Link>
            </div>
            <div className={"split form"}>
                <Link to={""} className={"logo mobile"}>
                    <img alt="banner" className="" src={LogoImage} />
                </Link>
                {
                    bigLoad ?
                        <img className={"load-centered"} src={bigLoader} alt={"verification"} />
                        :
                        error ?
                            <div className={"fields"}>
                                <div className={"sent"}>
                                    <div className={"thumb"}>
                                        <img src={expired}  alt={"lien expiré"} />
                                    </div>
                                    <div className={"title"}>Votre lien est expiré</div>
                                    <div className={"exp"}>
                                        Le lien le réinitialisation est valable pour une heure, clicquez sur le bouton ci-dessous pour obtenir un autre lien
                                    </div>
                                    <Link to={"/reset"} style={{textDecoration: 'none'}}>
                                        <button onClick={changePassword} className="btn-connect">
                                            Mot de passe oublié
                                        </button>
                                    </Link>
                                    <Link to="/" className="sad">
                                        Se connecter
                                    </Link>
                                </div>
                            </div>
                            :
                            <div className={"fields"}>
                                <div className={"title"}>
                                    Réinitialiser votre mot de passse
                                    <span className={"line"}/>
                                </div>
                                <div className={"message"}>
                                    { message }
                                </div>
                                <div className={"message success"}>
                                    { successMessage }
                                </div>
                                <div className={"box"}>
                                    <HttpsOutlined/>
                                    <input value={password} name={"password"} onChange={handleChangePassword} type={"password"} placeholder={"Nouveau mot de passe"} />
                                </div>
                                <div className={"box"}>
                                    <HttpsOutlined/>
                                    <input value={confirmPassword} name={"password"} onChange={handleChangeConfirmPassword} type={"password"} placeholder={"Confirmer le mot de passe"} />
                                </div>
                                <button onClick={changePassword} className={"submit"}>
                                    {load ? <img src={loader} alt={"Veillez patienter"}/> : ''}
                                    Envoyer
                                </button>
                                <div className="register">
                                    <Link ref={loginRef} to="/login">
                                        Se connecter
                                    </Link>
                                </div>
                            </div>

                }
            </div>
        </div>
    )
}

export default Password;