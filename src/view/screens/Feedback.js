import loader from "../assets/loader/785.svg";
import {useState} from "react";
import {CheckCircleOutlined} from "@material-ui/icons";
import {useHistory} from "react-router-dom";
import Rate from "../components/Rate";
import FeedbackService from "../../services/api/FeedbackService";
import {useSelector} from "react-redux";

const Feedback = () => {

    const user = useSelector(state => state.auth.data);

    const [load, setLoad] = useState(false);
    const [message, setMessage] = useState('');
    const [success, setSuccess] = useState(false);
    const [note, setNote] = useState(0);
    const [comment, setComment] = useState('');
    const [name, setName] = useState(user ? user.name : null);
    const [email, setEmail] = useState(user ? user.email : null);

    const feedbackService = new FeedbackService();

    const history = useHistory();

    const sendFeedBack = () => {
        if(note && comment){
            setLoad(true);
            setMessage('');
            feedbackService.post({
                rate: note,
                comment: comment,
                name: name,
                email: email
            }).then(result => {
                setLoad(false);
                if(result.status === 201){
                    setSuccess(true);
                }else{
                    setMessage('Une erreur s\'est produite, essayez a nouveau');
                }
            }).catch(err => {
                setMessage('Une erreur s\'est produite, essayez a nouveau');
            });
        }else{
            setMessage('Veillez indiquer une note et un commentaire');
        }
    }

    return(
        <div className={"interest-page"}>
            <div className={success ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Bravo, votre avis a bien ete envoyer. Merci pour votre contribution.</div>
                    <button onClick={() => history.goBack()} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className="form">
                <div className={"int-title"}>
                    <h1>Donner votre avis</h1>
                </div>
                <div className={"form-content"}>
                    <p>
                        Votre avis compte, aidez-nous a ameliorer notre services. Seul les champs avec un * sont obligatoires.
                    </p>
                    <div className={"box"}>
                        <label>Note *</label>
                        <div className={"stars"}>
                            <Rate rate={note} setRate={setNote} />
                        </div>
                    </div>
                    <div className={"box"}>
                        <label>Commentaire *</label>
                        <textarea value={comment} onChange={(e) => setComment(e.currentTarget.value)} className={""} />
                    </div>
                    <div className={"box"}>
                        <label>Nom et Prenom(s)</label>
                        <input value={name} onChange={(e) => setName(e.currentTarget.value)} type="text" />
                    </div>
                    <div className={"box"}>
                        <label>Email</label>
                        <input value={email} onChange={(e) => setEmail(e.currentTarget.value)} type="text" />
                    </div>
                    <div className={"field"}>
                        <label></label>
                        <div className={"inlined-field"}>
                            <div className="message">
                                { message }
                            </div>
                        </div>
                    </div>
                    <div className={"field"}>
                        <label></label>
                        <div className={"inlined-field"}>
                            <button onClick={sendFeedBack} className={"btn"}>
                                {
                                    load ? <img src={loader} alt="Chargement" /> : ''
                                }
                                Envoyer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Feedback;