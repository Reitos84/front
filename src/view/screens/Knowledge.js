import '../css/faqs.css';
import FaqService from "../../services/api/FaqService";
import {useEffect, useState} from "react";
import Question from "../components/Question";
import loader from "../assets/loader/786.gif";

const faqService = new FaqService();

const Knowledge = () => {

    document.title = 'Centre d\'aide - Vente et Location de bien immobilier à abidjan';

    const [page] = useState(1);
    const [index, setIndex] = useState(null);
    const [searchkey, setSearchKey] = useState(null);
    const [listing, setListing] = useState([]);
    const [categories, setCategories] = useState([]);
    const [load, setLoad] = useState(true);

    const handleSearch = () => {
        setLoad(true);
        faqService.search(searchkey, page).then((data) => {
            setListing(data['hydra:member']);
            setLoad(false);
        });
    };

    useEffect(() => {
        faqService.getCategory().then(res => {
            setCategories(res['hydra:member']);
        });
    }, []);

    useEffect( () => {
        //Find for page
        if(searchkey){
            handleSearch();
        }else{
            faqService.getAll(page).then(data => {
                setListing(data['hydra:member']);
                setLoad(false);
            });
        }
    }, [searchkey]);


    const handleSelect = (id) => {
        if(index === id){
            setIndex(null);
        }else{
            setIndex(id);
        }
    }

    return(
        <div className={"faqs"}>
            <div className='f_top'>
                <div className={"faqs-banner"}>
                    <div className={"f-title"} >
                        <h1 className='sectionTitle center'>
                            <span className='smallTitle'>FAQs</span>
                            <span>Questions fréquemments posées</span>
                        </h1>
                        <div className={"simple"}>
                            Vous avez des questions ? Nous sommes là pour vous aider.
                        </div>
                    </div>
                    <div className={"faq-categories"}>
                        {
                            categories.map(c => <span key={c.id} onClick={() => setSearchKey(c.id)} className={c.id === searchkey ? "f-cat active" : "f-cat"}>
                            { c.title }
                        </span>)
                        }
                    </div>
                    {/*
                        <div className={"finder"}>
                            <input onChange={(e) => setSearchKey(e.target.value) } value={searchkey} placeholder={"Rechercher"} />
                            <button onClick={handleSearch} >
                                <SearchOutlined />
                            </button>
                        </div>
                    */}
                </div>
            </div>
            
            <div className={"faq-content"}>
                <div className="help-listing">
                    {
                        load ?
                            <img className={"faq-load"} alt={"Veillez patienter"} src={loader} />
                        :
                            listing.map(faq => <Question handleSelect={() => handleSelect(faq.id)} index={index} key={faq.id} faq={faq} />)
                    }
                </div>
            </div>
        </div>
    )
}

export default Knowledge;