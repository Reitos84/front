import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import defaultImage from "../assets/page/relocalisation.jpg";
// import 'App.css';

const Relocation = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Services de relocalisation</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className="main-content">
                <p>
                    Le déménagement peut être une période stressante pour tout le monde.
                    Déménager dans un autre pays peut être une tâche encore plus ardue.
                    Si vous avez choisi de vous installer en Côte d'Ivoire et en particulier dans sa ville principale en plein essor, Abidjan, pour des raisons professionnelles ou personnelles, vous pouvez être assuré que notre équipe est à votre disposition pour vous aider à régler vos besoins en matière de logement.
                    Que ce soit pour une solution temporaire ou permanente, l’équipe d’Abidjan Habitat vous aidera à naviguer les vagues pour vous trouver une solution de logement convenable pour vous et votre famille.

                   <br></br> 
                </p>
                <p className='strong_p'>
                    Contactez-nous dès aujourd'hui pour une consultation gratuite et sans engagement.<br></br>
                    NOUS CONNAISSONS ABIDJAN, ALORS PROFITEZ-EN !
                </p>


               <Link to={"/contact?subject=Relocalisation"}><button className="link__btn">Contactez nous</button></Link>
            </div>
        </div>
    )
}

export default Relocation;