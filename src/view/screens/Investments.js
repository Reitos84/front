import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import defaultImage from "../assets/page/inves.png";
import imageTwoo from "../assets/page/inves2.jpg";
import invesClub from "../assets/page/group.png";
// import 'App.css';

const investments = () =>{
    return(
        <div>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                       <h1>Investissement</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={imageTwoo} />
            </div>
            <div className='container-fluid bg-color'>
                <div className='m-container'>
                    <div className='row'>
                        {/* <div className='col-lg-6'>
                            <img alt="investisement banner" className="img-fluid inves-img none" src={defaultImage}/>
                        </div> */}
                        <div className='col-lg-12'>
                            <h2 className='sectionTitle meduim'> 
                                <span className='smallTitle'>Créez et construisez de la richesse</span> 
                                <span>  avec l'investissement immobilier</span>
                            </h2>
                            <div>
                                <p>
                                    L'immobilier est l'un des outils  les plus puissants de création de richesse.
                                    Il existe, bien sûr, plusieurs stratégies qui peuvent vous permettre d’atteindre vos objectifs  d'investissement. Que vous recherchiez des informations, des opportunités, des projets d'investissement immobilier, des propriétés à acheter ou autre chose, vous pouvez les trouver sur notre plateforme.

                                </p>
                                <p>
                                    Un des moyens le plus courant pour les investisseurs de gagner de l'argent dans l'immobilier est de devenir propriétaire d'un bien locatif.
                                    Un autre moyen pour les personnes plus expérimentées et plus tolérant au risque ; <br/> c’est d’acheter des biens immobiliers sous-évalués, les réparer pour ajouter plus de valeur et ensuite les revendre pour gagner une bonne marge bénéficiaire. 
                                    Pour les moins expérimentés ou même ceux qui n’ont pas le temps, un des moyens le plus sûr de gagner de l’argent dans l’immobilier et se construire un portfolio qui génère de l’argent ; <br/> est de joindre un groupe d'investissement immobilier et confier la recherche et la structuration des opportunités ainsi que la gestion des biens a des professionnels du métier.

                                </p>
                            </div>

                            <Link to={'/investor'} > 
                                <button className='link_text mb-4'> 
                                    OUI! Je suis prêt à  investir 
                                </button>
                            </Link>
                        </div>
                        <div className='col-lg-12 mt-2'>
                            <div>
                                <p>
                                    Permettez-moi alors de vous poser une question...
                                    Seriez-vous ravi d’avoir à votre disposition les informations, les moyens et les outils d’analyse nécessaire pour faciliter vos décisions d'investissement avec plus d’assurance et de confiance au lieu d’avoir l'impression de chercher une aiguille dans une botte de foin.  ? <br/>
                                    Si vous pouviez avoir tout cela en place, cela signifierait que vous pourriez gagner du temps et trouver de bonnes opportunités de manière constante et avec succès ! (Ceci serait fantastique n’est-ce pas ?) <br/>
                                    Il est si facile de se laisser entraîner dans des projets et des soit disant opportunités remplient de conjectures qui peuvent vous paralyser.  

                                </p>
                            </div>
                           
                        </div>
                        <div className='col-lg-4'>
                            <img alt="investisement banner" className="img-fluid inves-img" src={defaultImage}/>
                        </div>

                        <div className='col-lg-8'>
                            <div className='mt-3'>
                                <p>
                                    Il est si facile de se laisser entraîner dans des projets et des soit disant opportunités remplient de conjectures qui peuvent vous paralyser.  
                                    Une dernière question... <br/>
                                    Vous arrive-t-il parfois de penser : "le marché est simplement trop saturé et les agents disent ou font n'importe quoi pour faire des ventes"... ou "les gens ne veulent pas entendre la vérité, et ils choisissent simplement d’écouter celui qui leur dit ce qu'ils veulent entendre"... ou "tout le monde semble être en mesure de trouver de bonnes opportunités sur le marché sauf moi" ? <br/>
                                    Vous n’êtes pas seul… je l'ai fait aussi ! <br/><br/>
                                    En effet, je sais exactement ce que vous ressentez car ça m’est arrivé ... plus d'une fois !
                                    Je suppose qu’en moment, vous voulez vraiment atteindre une plus grande liberté financière grâce à des investissements immobiliers, passer plus de temps avec votre famille et vos amis 
                                    au lieu de travailler tout le temps, et enfin vous aimerez vous sentir au contrôle de votre vie et de vos finances. Oui?
                                    Si cela semble similaire à votre situation, alors je voudrais vous inviter à découvrir...
                                </p>
                            </div>

                            <Link to={'/investor'} > 
                                <button className='link_text'> 
                                    OUI! Je suis prêt à  investir 
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-fluid'>
                <div className='m-container'>
                    <div className='row'>
                        <div className='col-lg-6'>
                            <h2 className='sectionTitle'> 
                                <span className='smallTitle'>Le Club des</span> 
                                <span> Investisseurs d'Abidjan Habitat</span>
                            </h2>
                            <p>
                                Le Club des Investisseurs d'Abidjan Habitat vous donne accès à des informations clé en main, des stratégies et des outils d’analyse pour faciliter vos décisions d'investissement avec plus d’assurance <br></br>
                                <span className=''>NE SOYEZ PAS JUSTE UN INVESTISSEUR EN IMMOBILIER... MAIS PLUTOT UN INVESTISSEUR EN IMMOBILIER QUI A DU SUCCES !</span>
                            </p>

                            <h2 className='sectionTitle'> 
                                <span className='smallTitle'>Avec Le Club des Investisseurs d'Abidjan Habitat</span> 
                            </h2>
                            <ul className='club-list'>
                                <li>Trouvez de bonnes opportunités de manière consistante et cohérente</li>
                                <li>Evaluer des analyses d’investissement immobilier faites sur mesure</li>
                                <li> Identifier les opportunités à faible risque et haute performance</li>
                                <li>Eviter de faire des investissements à haut risque qui rapportent peu ou vous font perdre</li>
                                <li>Réalisez un bon retour sur investissement de vos transactions immobilières</li>
                                <li> Profiter des avantages d'un revenu passif </li>
                                <li>Atteignez plus de liberté financière grâce à vos investissements immobiliers pour votre bien-être et celui de votre famille</li>
                            </ul>
                        </div>
                        <div className='col-lg-6'>
                            <div className='image-box'>
                                <img alt="investisement banner" className="img-fluid inves-img" src={invesClub}/>
                            </div>
                            
                        </div>
                        <div className='col-lg-12 mt-5'>
                            <div className='text-center'>
                                <p className='bold-text strong_p'>
                                    REJOIGNEZ LE CLUB DES INVESTISSEURS D’ABIDJAN HABITAT…C’EST GRATUIT ! 
                                </p>
                                <Link to={'/investor'} > 
                                    <button className='link_text'> 
                                        {/* OUI! Je suis prêt à atteindre ma liberté financière grâce aux investissements immobiliers aujourd’hui  */}
                                        OUI! Je suis prêt à  investir 
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default investments;