import "../css/agency.css";
import "../css/investments.css";
import React from 'react';
import "../css/agency.css";
import "../css/investments.css";
import { Link } from "react-router-dom";
import defaultImage from "../assets/cgv.jpg";
const Policy = () => {

    document.title = 'Politique de confidentialités';


    return (
        <>
            <div className="agency-header">
                <div className='gardienStyle'>
                    <div className="path">
                        <Link to={"/website"}>Accueil</Link>
                    </div>
                    <div className={"big-text"}>
                        <h1>Conditions générales</h1>
                    </div>
                </div>
                <img alt="investisement banner" className="agencyAvatar" src={defaultImage} />
            </div>
            <div className="sample-page policy">
                <p>
                    Les présentes conditions générales régissent l’utilisation de ce site www.abidjanhabitat.com.
                    Ce site appartient et est géré par Amikan Technologies SARL
                    En utilisant ce site, vous indiquez que vous avez lu et compris les conditions d’utilisation et
                    que vous acceptez de les respecter en tout temps.
                </p>
                <div className="intro"> <u>Propriété intellectuelle</u></div>
                <p>
                    Tout contenu publié et mis à disposition sur ce site est la propriété de Amikan Technologies
                    SARL et de ses créateurs. Cela comprend, mais n’est pas limité aux images, textes, logos,
                    documents, fichiers téléchargeables et tout ce qui contribue à la composition de ce site.
                </p>

                <div className="intro"> <u>Contributions d'utilisateur</u></div>
                <p>Les utilisateurs peuvent publier les informations suivantes sur notre site :</p>

                <ul>
                    <li>• Articles à vendre</li>
                    <li>• Photos</li>
                    <li>• Vidéos</li>
                    <li>• Biens immobiliers</li>
                </ul>
                <p>
                    En affichant publiquement sur notre site, vous acceptez de ne pas agir illégalement ou violer
                    les conditions d’utilisation acceptable énumérées dans ce document.
                </p>

                <div className="intro"><u>Comptes</u></div>
                <p>
                    Lorsque vous créez un compte sur notre site, vous acceptez ce qui suit :

                </p>

                  <ul>
                    <li>
                        1. que vous êtes seul responsable de votre compte et de la sécurité et la confidentialité
                        de votre compte, y compris les mots de passe ou les renseignements de nature
                        délicate joints à ce compte,
                    </li>
                    <li>
                        2. que tous les renseignements personnels que vous nous fournissez par l’entremise de
                        votre compte sont à jour, exacts et véridiques et que vous mettrez à jour vos
                        renseignements personnels s’ils changent
                    </li>
                    <li>
                        Nous nous réservons le droit de suspendre ou de résilier votre compte si vous utilisez notre
                        site illégalement ou si vous violez les conditions d’utilisation acceptable.
                    </li>
                </ul>
                <div className="intro"><u>Ventes des biens et services</u></div>
                <p>
                    Les biens que nous offrons comprennent : Immobilier
                </p>

                <p>
                    Les services que nous offrons comprennent :
                </p>
                <ul>
                    <li>• Location</li>
                    <li>• Vente</li>
                    <li>• Marketing</li>
                    <li>• Gestion</li>
                    <li>• Conseils</li>
                </ul>
                <p>
                    Les biens et services liés à ce document sont les biens et services qui sont affichés sur notre
                    site au moment où vous y accédez. Cela comprend tous les produits énumérés comme étant en
                    rupture de stock. Toutes les informations, descriptions ou images que nous fournissons sur
                    nos biens et services sont décrites et présentées avec la plus grande précision possible.
                    Cependant, nous ne sommes pas légalement tenus par ces informations, descriptions ou
                    images car nous ne pouvons pas garantir l’exactitude de chaque produit ou service que nous
                    fournissons. Vous acceptez d’acheter ces biens et services à vos propres risques <br></br>
                    <i>Vendus par des tiers</i>
                </p>
                <p>
                    Notre site peut offrir des biens et services de sociétés tierces. Nous ne pouvons pas garantir la
                    qualité ou l’exactitude des biens et services mis à disposition par des tiers sur notre site.
                </p>

                <p>
                    <i>Vendus par des utilisateurs</i> <br></br>
                    Notre site permet aux utilisateurs de vendre des biens et services. Bien que nous permettions
                    aux utilisateurs de vendre des biens et services sur notre site, nous n’assumons aucune
                    responsabilité pour ces biens et services. Nous ne pouvons pas garantir la qualité ou
                    l’exactitude des produits vendus par nos utilisateurs sur notre site. Cependant, si nous sommes
                    mis au courant qu’un utilisateur viole nos normes d’utilisation acceptable, nous nous
                    réservons le droit de suspendre ou d’interdire à l’utilisateur de vendre des produits sur notre
                    site.
                </p>

                <div className="intro"><u>Paiements</u></div>
                <p>Nous acceptons les modes de paiement suivants sur ce site :</p>

                 <ul>
                    <li>• Carte bancaire</li>
                    <li>• PayPal</li>
                    <li>• Prélèvement automatique</li>
                    <li>• Orange Money</li>
                    <li>• Moov Money</li>
                    <li>• MTN Money</li>
                    <li>• Wave</li>
                </ul>

                <p>
                    Lorsque vous nous fournissez vos renseignements de paiement, vous nous confirmez que vous
                    avez autorisé l’utilisation et l’accès à l’instrument de paiement que vous avez choisi d’utiliser.
                    En nous fournissant vos détails de paiement, vous confirmez que vous nous autorisez à
                    facturer le montant dû à cet instrument de paiement.
                </p>
                <p>
                    Si nous estimons que votre paiement a violé une loi ou l’une de nos conditions d’utilisation,
                    nous nous réservons le droit d’annuler votre transaction.

                </p>
                <p>
                    <i>Services</i> <br></br>
                    Les services seront facturés en totalité à la commande du service.
                </p>

                <p>
                    <i>Abonnements</i> <br></br>
                    
                </p>
                <p>
                    Tous nos abonnements récurrents seront automatiquement facturés et renouvelés jusqu’à ce
                    que nous recevions d’avis que vous souhaitez annuler l’abonnement.
                    Si vous souhaitez annuler vos abonnements, veuillez suivre ces étapes : <br></br>
                    Frais d'inscription <br></br>
                    Le compte du titulaire de la carte de crédit est automatiquement débité à la même date que la
                    date de transaction initiale de chaque mois correspondant ou à la dernière date du mois s'il n'y
                    a pas de date correspondante (l'abonnement du 31 janvier 2022 se renouvelle le 28 février
                    2022). <br></br>

                    Remises, remises ou autres offres spéciales uniquement valables pour la durée initiale ; les
                    abonnements sont renouvelés aux tarifs d'abonnement complet alors en vigueur.
                    AbidjanHabitat.com peut résilier l'abonnement et les présentes conditions s'il n'est pas en
                    mesure de renouveler l'abonnement sur la base d'informations de carte de crédit inexactes ou
                                        obsolètes. <br></br>
                                        
                    Le droit d'accès accordé en vertu des présentes Conditions n'est effectif qu'au moment du
                    paiement des frais d'abonnement.


                </p>

                <p>
                    <i>Compte d'annulation</i> <br></br> 
                </p>
                <p>
                    Vous pouvez annuler votre abonnement à partir de la page de profil de votre compte.
                    La seule méthode valable pour annuler votre plan est via le lien d'annulation fourni sur votre
                    page "tableau de bord" utilisateur, accessible après vous être connecté au site Web
                    AbidjanHabitat.com. Les demandes d'annulation par e-mail ou par téléphone ne sont pas
                    prises en compte et ne réalisent pas l'annulation.
                    Une fois que vous avez annulé votre abonnement, vous ne perdrez pas l'accès immédiatement.
                    Votre abonnement se poursuivra jusqu'à la fin de votre cycle de charge actuel. Par exemple :
                    si votre carte de crédit est débitée le 15 du mois et que vous annulez le 25 avril, vous ne
                    perdrez pas l'accès avant le 15 mai.

                </p>

                <p>
                    <i>Remboursements</i> <br></br> 
                </p>
                <p>
                    Les frais d'abonnement ne sont pas remboursables ; sauf que vous pouvez annuler un
                    abonnement renouvelé en contactant AbidjanHabitat.com dans les deux (2) jours civils .
                    suivant la date de renouvellement et recevoir un remboursement complet des nouveaux frais d'abonnement.
                </p>

                <p>
                    <i>Essai gratuit</i> <br></br> 
                </p>

                <p>
                    Nous offrons un essai gratuit de 14 jours lorsque les utilisateurs s'inscrivent à un nouveau
                    compte. L'essai gratuit comprend un accès illimité à l'application web ainsi que tous les
                    documents disponibles sur le site. <br></br>
                    Une fois l'essai gratuit terminé, vous serez automatiquement facturé à notre taux
                    d'abonnement mensuel ou annuel selon votre préférence. Si vous ne voulez pas être facture,
                    votre annulation doit se faire avant la fin de votre essai gratuit.
                </p>

                <div className="intro"><u>Limitation de responsabilité</u></div>
                <p>
                    Amikan Technologies SARL ou l’un de ses employés sera tenu responsable de tout problème
                    découlant de ce site. Néanmoins, Amikan Technologies SARL et ses employés ne seront pas
                    tenus responsables de tout problème découlant de toute utilisation irrégulière de ce site.
                </p>

                <div className="intro"><u>Indemnité</u></div>
                <p>
                    En tant qu’utilisateur, vous indemnisez par les présentes Amikan Technologies SARL de
                    toute responsabilité, de tout coût, de toute cause d’action, de tout dommage ou de toute
                    dépense découlant de votre utilisation de ce site ou de votre violation de l’une des dispositions
                    énoncées dans le présent document.
                </p>

                <div className="intro"><u>Lois applicables</u></div>
                <p>
                    Ce document est soumis aux lois applicables en France et vise à se conformer à ses règles et
                    règlements nécessaires. Cela inclut la réglementation à l’échelle de l’UE énoncée dans le
                    RGPD.
                </p>

                 <div className="intro"><u>Modifications</u></div>
                <p>
                    Ces conditions générales peuvent être modifiées de temps à autre afin de maintenir le respect
                    de la loi et de refléter tout changement à la façon dont nous gérons notre site et la façon dont
                    nous nous attendons à ce que les utilisateurs se comportent sur notre site. Nous
                    recommandons à nos utilisateurs de vérifier ces conditions générales de temps à autre pour
                    s’assurer qu’ils sont informés de toute mise à jour. Au besoin, nous informerons les
                    utilisateurs par courriel des changements apportés à ces conditions ou nous afficherons un
                    avis sur notre site.

                </p>

                 <div className="intro"><u>Contact</u></div>
                <p>
                    Veuillez communiquer avec nous si vous avez des questions ou des préoccupations. Nos
                    coordonnées sont les suivantes :
                   <a href="mailto:info@abidjanhabitat.com">info@abidjanhabitat.com</a> 


                </p>
            </div>
        </>

    )
}

export default Policy;