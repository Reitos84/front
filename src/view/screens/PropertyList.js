import React,{useEffect, useState} from 'react';
import LoginModal from "../components/LoginModal";
import Listing from "../components/Listing.js";
import '../css/listingForm.css';
import '../css/listing.css';
import PropertyService from '../../services/api/PropertyService.js';
import TypeService from '../../services/api/TypeService.js';
import CategoryService from '../../services/api/CategoryService.js';
import {HomeOutlined, LocationSearchingOutlined, FilterListOutlined} from "@material-ui/icons";
import {Link, useParams, useLocation} from "react-router-dom";
import { useSelector } from "react-redux";
import { NumberOfRoom } from "../constants/utils";
import Geopify from "../../services/places/Geopify";
import {FormatListBulleted, RoomOutlined} from "@material-ui/icons";
import { AcceptedOrder } from "../constants/utils";
import ShowMap from '../components/ShowMap';
import MapListing from "../components/MapListing";
import {rentRange, saleRange} from "../constants/utils";
import {thousandsSeparators} from "currency-thousand-separator";
import SavedService from "../../services/api/SavedService";

const PropertyList = () =>{
    //services
    const propertyService = new PropertyService(); 
    const typeService = new TypeService();
    const categoryService = new CategoryService();
    const geopify = new Geopify();
    const savedService = new SavedService();

    const user = useSelector(data => data.auth.data);

    const [load, setLoad] = useState(true);
    const [listing, setListing] = useState([]);
    const [totalProperties, setTotalProperties] = useState(0);
    const [page, setPage] = useState(1);
    const [typesList, setTypesList] = useState([]);
    const [category, setCategory] = useState(null);
    const [showAdvanced, setShowAdvanced] = useState(false);
    const [requestLogin, setRequestLogin] = useState(false);
    const [addresses, setAdresses] = useState([]);
    const [type, setType] = useState(null);
    const [findNow, setFindNow] = useState(false);

    const [indexedSide, setIndexedSide] = useState('list');

    //Url paramaeters
    let params = useParams();
    let queryString = new URLSearchParams(useLocation().search);
    let priceRange = queryString.has('prices') ? queryString.get('prices').split(',') : [];
    let selectedType = queryString.has('type') ? queryString.get('type') : '';
    let priceMin = queryString.has('priceMin') ? queryString.get('priceMin') : '';
    let priceMax = queryString.has('priceMax') ? queryString.get('priceMax') : '';
    let bedroom = queryString.has('bedroom') ? queryString.get('bedroom') : '';
    let saloon = queryString.has('saloon') ? queryString.get('saloon') : '';
    let kitchen = queryString.has('kitchen') ? queryString.get('kitchen') : '';
    let shower = queryString.has('shower') ? queryString.get('shower') : '';
    let boundary = queryString.has('boundary') && queryString.get('boundary').length > 0 ? JSON.parse(queryString.get('boundary')) : null;


    if(params.categorySlug === "sale"){
        document.title = "Propriétés à vendre à Abidjan";
    }else{
        document.title = "Propriétés à louer à Abidjan";
    }

    //Filters
    const [finder, setFinder] = useState({
        category: null,
        location: queryString.has('location') ? queryString.get('location') : null,
        type: null,
        space: null,
        priceMin: priceRange.length === 2 && Number(priceRange[0]) > 1 ? priceRange[0] : priceMin,
        priceMax: priceRange.length === 2 && Number(priceRange[1]) > 1 ? priceRange[1] : priceMax,
        bedroom: bedroom,
        saloon: saloon,
        kitchen: kitchen,
        order: null,
        shower: shower,
        boundary: boundary ? {lat: boundary[1], lng: boundary[0]} : null
    });

    useEffect(() => {
        if(params.categorySlug){
            categoryService.getWithSlug(params.categorySlug).then(i_fetch => {
                if(i_fetch['hydra:totalItems']){
                    setCategory(i_fetch['hydra:member'][0]);
                    typeService.getAll().then(res => {
                        let k = res['hydra:member'].find((t) => t.slug === selectedType );
                        if(k){
                            setType(k.id);
                        }
                        setTypesList(res['hydra:member']);
                        setFindNow(true);
                    })
                }
            })
        }
    }, []);

    const calculateLongitude = (longitude) => {
        if(longitude > 0){
            return [(longitude - 0.02), longitude + 0.02];
        }else{
            return [(longitude - 0.02), longitude + 0.02];
        }
    }

    const fetchProperties = async () => {
        if(category && findNow){
            setLoad(true);
            if(finder.location){
                let locale = null;
                if(!finder.boundary) {
                    let places = await geopify.locationSearch(finder.location);
                    places.features.forEach(elem => {
                        if(!locale){
                            if (elem.properties.name === 'locale') {
                                locale = elem.properties;
                            } else if (elem.properties.address_line1.indexOf(finder.location)) {
                                locale = elem.properties;
                            }
                        }
                    });
                    if(!locale){
                        locale = places.features[0].properties;
                    }
                    locale = {
                        lat: parseFloat(locale.lat.toFixed(3)),
                        lng: parseFloat(locale.lon.toFixed(3))
                    };
                }else{
                    locale = {
                        lat: parseFloat(finder.boundary.lat.toFixed(3)),
                        lng: parseFloat(finder.boundary.lng.toFixed(3))
                    };
                }
                if(locale){
                    setFinder({...finder, boundary: locale});
                    let locationLngResult = await propertyService.filterProperty(category.id, {...finder, type: type}, page, null, calculateLongitude(locale.lng));
                    let total = 0;
                    let allResult = locationLngResult['hydra:member'];
                    total += locationLngResult['hydra:totalItems'];
                    if(page <= 1){
                        setListing([...allResult]);
                        setTotalProperties(total);
                    }else{
                        setListing([...listing, ...allResult]);
                    }
                    setLoad(false);
                }else{
                    let propertiesResult = null;
                    propertiesResult = await propertyService.filterProperty(category.id, {...finder, type: type}, page);
                    setTotalProperties(propertiesResult['hydra:totalItems']);
                    if(page <= 1){
                        setListing(propertiesResult['hydra:member']);
                    }else{
                        setListing([...listing, ...propertiesResult['hydra:member']]);
                    }
                    setLoad(false);
                }
            }else{
                let propertiesResult = null;
                propertiesResult = await propertyService.filterProperty(category.id, {...finder, type: type}, page);
                setTotalProperties(propertiesResult['hydra:totalItems']);
                if(page <= 1){
                    setListing(propertiesResult['hydra:member']);
                }else{
                    setListing([...listing, ...propertiesResult['hydra:member']]);
                }
                setLoad(false);
            }
        }
    };

    useEffect(() => {
        fetchProperties().then(() => {
            setFindNow(false);
        });
    }, [findNow, page]);


    const findArea = async (key) => {
        if(key.length > 0){
            const result = await geopify.locationSearch(key);
            setAdresses(result.features);
        }
    };

    const handleSelectArea = (element) => {
        setFinder({
            ...finder,
            location: element.properties.address_line1,
            boundary: {
                lat: element.properties.lat,
                lng: element.properties.lon
            }
        });
        setFindNow(true);
    }

    const handleLoseIndex = () => {
        setTimeout(() => {
            setAdresses([]);
        }, 1500);
    }

    const handleSave = async (property) => {
        if(user){
            if(property.iSaved){
                const result = await savedService.removeSave(property.iSaved);
                if(result.status === 204){
                    setListing(listing.filter(x => {
                        if(x.id === property.id){
                            x.iSaved = 0;
                        }
                        return x;
                    }));
                }
            }else{
                const result = await savedService.saveProperty(property.id, user.id);
                if(result.id){
                    setListing(listing.filter(x => {
                        if(x.id === property.id){
                            x.iSaved = result.id;
                        }
                        return x;
                    }));
                }
            }
        }else{
            setRequestLogin(true);
        }
    }

    return( 
        <div className="propertyList">
            <LoginModal state={requestLogin} toggle={setRequestLogin} />
            <form method="get" action="" className="sticky-top">
                <div className="listingForm">
                    <div className="formBox">
                        <div className="listing-side-input">
                            <div className={"listing-side-elements"}>
                                <div className="fieldBorder wide">
                                    <input onBlur={handleLoseIndex} value={finder.location} onChange={(e) => { findArea(e.currentTarget.value); setFinder({...finder, location: e.currentTarget.value}); }} type="text" name="location" placeholder="Enter une adresse, ville, commune"/>
                                    <LocationSearchingOutlined/>
                                    <div className={addresses.length > 0 ? "location-result long" : "location-result"}>
                                        {
                                            addresses.map((elem, index) => <span onClick={() => handleSelectArea(elem) } key={index}>{elem.properties.address_line1}</span>)
                                        }
                                    </div>
                                </div>
                                <div className="fieldBorder type-desktop">
                                    <HomeOutlined/>
                                    <select value={type} onChange={(e) => {setType(e.currentTarget.value); setFindNow(true);}} name="type">
                                        <option value={''}>Type de propriete</option>
                                        {
                                            typesList.map(elem => {
                                                if(!elem.category || elem.category === category.slug){
                                                    return <option key={elem.id} value={elem.id}>{elem.title}</option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                                <div className="fieldBorder medium price-field">
                                    <strong>{process.env.REACT_APP_CURRENCY}</strong>
                                    <select value={finder.priceMin} onChange={(e) => {setFinder({...finder, priceMin: e.target.value}); setFindNow(true); }}>
                                        <option value={''}>Prix Min</option>
                                        {
                                            params.categorySlug === 'rent' ?
                                                rentRange.map((x, key) => <option key={key} value={x.value[0]}>{ thousandsSeparators(x.value[0]) }</option>)
                                            :
                                                saleRange.map((x, key) => <option key={key} value={x.value[0]}>{ thousandsSeparators(x.value[0]) }</option>)
                                        }
                                    </select>
                                </div>
                                <div className="fieldBorder medium">
                                    <strong>{process.env.REACT_APP_CURRENCY}</strong>
                                    <select value={finder.priceMax} onChange={(e) => {setFinder({...finder, priceMax: e.target.value}); setFindNow(true)}} >
                                        <option value={''}>Prix Max</option>
                                        {
                                            params.categorySlug === 'rent' ?
                                                rentRange.map(x => <option value={x.value[1]}>{ thousandsSeparators(x.value[1]) }</option>)
                                                :
                                                saleRange.map(x => <option value={x.value[1]}>{ thousandsSeparators(x.value[1]) }</option>)
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div onClick={() => setShowAdvanced(!showAdvanced)} className="advanced">
                            <FilterListOutlined />
                           <span>Filtres</span><span className='filter-advanced'> avancés</span> 
                        </div>
                    </div>
                </div>
                <div className={ showAdvanced ?  "collapse-order show" : "collapse-order"}>
                    <div className='other-filter mobile'>
                        <select value={type} onChange={(e) => {setType(e.currentTarget.value); setFindNow(true);}} name="type">
                            <option value={''}>Type de propriete</option>
                            {
                                typesList.map(elem => {
                                    if(!elem.category || elem.category === category.slug){
                                        return <option key={elem.id} value={elem.id}>{elem.title}</option>
                                    }
                                })
                            }
                        </select>
                        <select value={finder.priceMin} onChange={(e) => {setFinder({...finder, priceMin: e.target.value}); setFindNow(true); }}>
                            <option value={''}>Prix Min</option>
                            {
                                params.categorySlug === 'rent' ?
                                    rentRange.map((x, key) => <option key={key} value={x.value[0]}>{ thousandsSeparators(x.value[0]) }</option>)
                                :
                                    saleRange.map((x, key) => <option key={key} value={x.value[0]}>{ thousandsSeparators(x.value[0]) }</option>)
                            }
                        </select>
                        <select value={finder.priceMax} onChange={(e) => {setFinder({...finder, priceMax: e.target.value}); setFindNow(true)}} >
                            <option value={''}>Prix Max</option>
                            {
                                params.categorySlug === 'rent' ?
                                    rentRange.map(x => <option value={x.value[1]}>{ thousandsSeparators(x.value[1]) }</option>)
                                    :
                                    saleRange.map(x => <option value={x.value[1]}>{ thousandsSeparators(x.value[1]) }</option>)
                            }
                        </select>
                    </div>
                    <div className="other-filter">
                        <label>Nombre de chambre:</label>
                        <select value={finder.bedroom} onChange={(e) => { setFinder({...finder, bedroom: e.currentTarget.value}); setFindNow(true); }} name="bedroom" >
                            <option value={""}>Choisir</option>
                            {
                                NumberOfRoom.map(x => <option key={x} selected={x === finder.bedroom} value={x}>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="other-filter">
                        <label>Nombre de douche:</label>
                        <select name="shower" value={finder.shower} onChange={(e) => { setFinder({...finder, shower: e.currentTarget.value}); setFindNow(true); }}>
                            <option value="">Choisir</option>
                            {
                                NumberOfRoom.map(x => <option key={x} selected={x === finder.shower} value={x}>{x}</option>)
                            }
                        </select>
                    </div>
                    <button onClick={(e) => { e.preventDefault(); setShowAdvanced(!showAdvanced) }}>Rechercher</button>
                </div>
            </form>
        {
            indexedSide === 'list' ?
            <>
                <div className="listing">
                    <div className="mainContent">
                        <div className="PropertiesListe">
                            <div className={"second-bar"}>
                                <div className={"first"}>
                                    <div className={"path"}>
                                        {
                                            params.categorySlug === 'sale' ?
                                                <Link to={"/properties/sale"}>Propriétés à vendre à Abidjan</Link>
                                            :
                                                <Link to={"/properties/rent"}>Propriétés à louer à Abidjan</Link>
                                        }
                                    </div>
                                </div>
                                <div className={"other-bar"}>
                                    <div className={"result"}>
                                        <span className='mobile_displayNone'>Environ</span> <span>{ totalProperties }</span> <span className='result_span'>résultat{ totalProperties > 1 ? 's' : '' }</span> 
                                    </div>
                                    <div className={"cote"}>
                                        <div className={"sort"}>
                                            <span className='mobile_displayNone'>Trier par :</span>
                                            <select value={finder.order} onChange={(e) => { setFinder({...finder, order: e.currentTarget.value}); setFindNow(true); }} >
                                                <option value="">Choisir</option>
                                                {
                                                    AcceptedOrder.map((x, index) =>  <option key={index} value={x.value}>{x.title}</option>)
                                                }
                                            </select>
                                        </div>
                                        <div className={"sort choose"}>
                                            <FormatListBulleted /> Liste
                                        </div>
                                        <div onClick={() => setIndexedSide('map')} className={"sort carte"}>
                                            <RoomOutlined /> <sapn className="hidden_on_mobile">Carte</sapn>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="listing-item-side">
                                <Listing saveProperty={handleSave} location={finder.location} listing={listing} load={load}  />
                                <div className="paginate-index">
                                    <div className={"number"}>
                                        Afficher { listing.length } sur {totalProperties} propriétés
                                    </div>
                                    <div className="liner">
                                        <span style={{width: `${(listing.length * 100) / totalProperties}%`}}></span>
                                    </div>
                                    {
                                        totalProperties > listing.length ?
                                            <div className="selected" onClick={() => {setPage(page + 1); setFindNow(true)}} >Afficher plus</div>
                                            :
                                            ''
                                    }
                                </div>
                            </div>
                        </div>
                        <div className={"extra-side"}>
                            <div className={"extra-meta-link"}>
                                <div className={"map-visual"}>
                                    <div className={"map-block"}>
                                        {
                                            finder.boundary ?
                                                <ShowMap boundary={finder.boundary} />
                                                :
                                                <ShowMap />
                                        }
                                    </div>
                                    <div onClick={() => setIndexedSide('map')} className={"map-bottom"}>
                                        Afficher la carte
                                    </div>
                                </div>
                                {/*
                                <div className={"ads"}>
                                    Publicité
                                </div>
                                */}
                            </div>
                        </div>
                    </div>
                </div>
            </>
                :
            <MapListing setPage={setPage} setFindNow={setFindNow} page={page} toggleSide={setIndexedSide} total={totalProperties} listing={listing} />
        }
        </div>
    )
}

export default PropertyList;