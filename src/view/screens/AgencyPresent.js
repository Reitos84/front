import '../css/agency-present.css';
import {useState, useEffect} from 'react';
import AgencyService  from '../../services/api/AgencyService';
import { useParams } from "react-router-dom";
import AgencyResume from '../components/AgencyResume';

const AgencyPresent = () => {

    //Services
    const agencyService = new AgencyService();

    //Params
    const params = useParams();

    document.title = `${params.name} - Vente et Location de bien immobilier à abidjan`;

    const [agency, setAgency] = useState({});

    useEffect(() => {
        agencyService.get(params.id).then(response => {
            if(response.name === params.name){
                setAgency(response);
            }
        })
    }, []);

    return(
        <div className="agency-present">
            {
                <AgencyResume agency={agency} title={params.name} />
            }
        </div>
    )
}

export default AgencyPresent;