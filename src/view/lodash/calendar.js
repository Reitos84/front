const getCalendarDays = () => {
    let tab = [], 
        i = 1;
    while (i <= 31){
        tab.push(i);
        i++;
    }
    return tab;
}

const getCalendarMonth = () => {
    return [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre'
    ];
}

const getCalendarYears = () => {
    let tab = [],
        i = (new Date()).getFullYear() - 16,
        ends = i - 100;
    while (i > ends){
        tab.push(i);
        i--;
    }
    return tab;
}

export { getCalendarYears, getCalendarMonth, getCalendarDays }