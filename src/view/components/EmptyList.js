import loader from "../assets/todo-list.png";

const EmptyList = () => {
    return(
        <div className={"empty-list"}>
            <img src={loader} alt={"aucun résultat"}  />
            <div>
                Rien à afficher pour l'instant
            </div>
        </div>
    )
}

export default EmptyList;