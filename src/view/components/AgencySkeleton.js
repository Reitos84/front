import { Skeleton } from "react-skeleton-generator";

const AgencySkeleton = () => {
    return(
        <div className="uno-agency">
            <Skeleton.SkeletonThemeProvider color="#FAFAFA">
                <Skeleton width="100%" height="200px" />
                <div className="uno-agency-title">
                    <Skeleton width="60%" height="20px" />
                    <Skeleton width="260px" height="20px" />
                </div>
                <div className="bottom">
                    <Skeleton width="300px" height="20px" />
                    <br />
                    <br />
                </div>
            </Skeleton.SkeletonThemeProvider>
        </div>
    );
}

export default AgencySkeleton;