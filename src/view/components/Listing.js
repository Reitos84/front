import Property from "../components/Property";

import PropertySkeleton from "../components/PropertySkeleton";

const Listing = ({listing, location, load, saveProperty}) =>{

    const nbreSkeleton = [1,2,3,4,5,6,7,8,9,10];

    return(
        <div className="property-inside">
            {
                location ?
                <div className="tagged-location">
                    Propriétées à proximité de {location}...
                </div>
                :
                <></>
            }
            {
                !load ?
                    listing.map((items) => <Property savedIndex={items.iSaved} handleSave={() => { saveProperty(items)}} key={items.id} data={items} />)
                :
                    nbreSkeleton.map(elem => <PropertySkeleton key={elem} />)
            }
        </div>
    )
}

export default Listing;