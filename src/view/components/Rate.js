import {StarBorderOutlined, StarOutlined} from "@material-ui/icons";
import {useEffect, useState} from "react";

const Rate = ({rate, setRate}) => {

    const handleChangeRate = (index) => {
        setRate(index + 1);
    }

    const [display, setDisplay] = useState([]);

    useEffect(() => {
        var color = [];
        var sample = [];
        var o = 5 - rate;
        for (let i = 0; i < rate; i++) {
            color.push('a');
        }
        for (let j = 0; j < o; j++) {
            sample.push('b');
        }
        setDisplay([...color, ...sample]);
    }, [rate]);

    return(
        <>
            {display.map((x, index) =>
                x === 'a' ?
                    <StarOutlined onClick={() => handleChangeRate(index)} key={index} className={"simple colored"} />
                :
                    <StarBorderOutlined onClick={() => handleChangeRate(index)} key={index} className={"simple"} />
                )
            }
        </>
    )
}

export default Rate;