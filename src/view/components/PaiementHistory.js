const PaiementHistory = () => {
    return(
        <div className={"paiement-history"}>
            <h2 className="other-title">Liste des paiements</h2>
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
            <table>
                <thead>
                    <tr>
                        <td>Id transaction</td>
                        <td>Produit</td>
                        <td>Date</td>
                        <td>Montant</td>
                        <td>Statut</td>
                        <td>Method de paiement</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>45gh65g2</td>
                        <td className="dark">Reservation</td>
                        <td>05/01/2021</td>
                        <td className="dark">17,000 xof</td>
                        <td>Accepté</td>
                        <td>Carte Visa</td>
                    </tr>
                    <tr>
                        <td>45gh65g2</td>
                        <td className="dark">Reservation</td>
                        <td>05/01/2021</td>
                        <td className="dark">17,000 xof</td>
                        <td>Accepté</td>
                        <td>Mastercard</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default PaiementHistory;