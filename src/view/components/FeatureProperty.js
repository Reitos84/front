import React, {useState} from 'react';
import {FavoriteBorder} from "@material-ui/icons";
import {Link} from "react-router-dom";
import { thousandsSeparators } from 'currency-thousand-separator';
import SavedService from "../../services/api/SavedService";

const FeatureProperty= ({data, user, toggleLoginModal}) =>{

    const devise = process.env.REACT_APP_CURRENCY;

    const [liked, setLiked] = useState(data.iSaved ? data.iSaved : null);

    let currentPic = null;
    data.pictures.forEach(pic => {
        if(pic.isBackground){
            currentPic = pic;
        }
    });

    if(!currentPic){
        currentPic = data.pictures[0];
    }

    const savedService = new SavedService();

    const handleSaveProperty = async () => {
        if(user){
            if(liked){
                const result = await savedService.removeSave(liked);
                if(result.status === 204){
                    setLiked(false);
                }
            }else{
                const result = await savedService.saveProperty(data.id, user.id);
                if(result.id){
                    setLiked(result.id);
                }
            }
        }else{
            toggleLoginModal(true);
        }
    }

    let params = new URLSearchParams();
    params.append('title', data.title);
    params.append('owner', data.agency.name);
    params.append('location', data.area);
    params.append('price', data.price);

    return(
        <Link to={{pathname: `/property-detail/${data.code}/${ data.slug }`, search: params.toString()}} className=' col-lg-4 col-md-6 col-12'>
            <div className="proprety_items">
                <div className="proprety_items_illustration">
                    {
                        currentPic?
                        <img className={"lozad picture"} alt={currentPic.name} src={ `${process.env.REACT_APP_BASE_PATH}${currentPic.url}` }/>
                        :
                        ""
                    }
                    <div className='agent-fav'>
                        <div className='feature-agent_avatar'>
                        {
                        data.agency.avatarUrl ?
                                <img className={"lozad"} alt={data.agency.avatarUrl} src={`${process.env.REACT_APP_BASE_PATH}${data.agency.avatarUrl}`} />
                            :
                                <div style={{background: data.agency.color}}>{ data.agency.name.charAt(0) }</div>
                        }
                        </div>
                        <div onClick={(e) => { e.preventDefault(); handleSaveProperty() }} className={ liked ? 'fav mark' : 'fav'}>
                            <FavoriteBorder/>
                        </div>
                    </div>
                </div>
                <div className="proprety_items_description">
                    <div className="display-flex">
                        <div className='property-idFiels'>
                            <p className="p_id">id property:{data.code}</p>
                        </div>
                        <div className="description_btn d-flex">
                            <button className="btn_f">En vedette</button>
                            <button className="btn_r">{data.category.title}</button>
                        </div>
                    </div>
                    <h4 className='feature_property-title'>{data.title}</h4>
                    <div className="property_info dFlex">
                        <span className="feature_property-adress">{data.area}</span>
                    </div>
                    <div className="property_info">
                        <span className="price"> {devise} {thousandsSeparators(data.price)}</span>
                    </div>
                </div>                
            </div>
        </Link>
    )
}

export default FeatureProperty;