import React, {useState} from 'react';
import '../css/header.css';
import {Link }from 'react-router-dom';
import {TimelineOutlined , NotificationsOutlined, PowerSettingsNewOutlined,MenuOutlined ,CloseOutlined} from "@material-ui/icons";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../core/reducer/authSlice";
import logo from "../assets/logo/logo2.png";
const Navigation = () =>{

    const [showOptions, setShowOptions] = useState(false);
    const [sidebar, setSidebar] = useState(false);
    const showSidebar = () => setSidebar(!sidebar);
 

    const data = useSelector(state => state.auth.data);

    const dispatch = useDispatch();

    const handleLogout = () => {
        localStorage.removeItem('personnal');
        localStorage.removeItem('personnal_refresh');
        dispatch(logout());
    }               

    return(
        <div className="navigation"> 
            <nav className='desktop_navigation'>
                <div className="dFlex">
                    <Link className="logoLink" to="/" ><img src={logo}/></Link>
                    <ul className='desktop'>
                        <li><Link to="/properties/sale">A vendre</Link></li>
                        <li><Link to="/properties/rent">A louer</Link></li>
                        <li><Link to="/services">Services</Link></li>
                        <li><Link to="/agency-list">Agences</Link></li>
                        {/* <li><Link to="/blog">Blog</Link></li> */}
                    </ul>
                </div>
                <div className="menuLogin">
                    {data ?
                        <>
                            <Link to={"/evaluate"} className="saved">
                                <TimelineOutlined />
                                Evaluation de bien
                            </Link>
                            <Link to={"/account/notifications"} className="imgCycle user_notif">
                                <NotificationsOutlined />
                            </Link >
                            <div onClick={() => setShowOptions(!showOptions)} className=" imgCycle user_logo">
                                <span style={{ background: data.color }} className={"avatar"}>
                                    { data.name.charAt(0) }
                                </span>
                            </div>
                            <div className={ showOptions ? "user-options show" : "user-options"}>
                                <Link to={"/account/personnal-informations"}>
                                    Mes informations
                                </Link>
                                <hr />
                                <Link to={"/account/saved-properties"}>
                                    Propriété sauvegardées
                                </Link>
                                <Link to={"/account/security"}>
                                    Connexion
                                </Link>
                                <hr />
                                <span onClick={handleLogout}>
                                    <PowerSettingsNewOutlined />
                                    Déconnexion
                                </span>
                            </div>
                        </>
                        :
                        <>
                            <Link to={"/evaluate"} className="saved">
                                <TimelineOutlined />
                                Evaluation de bien
                            </Link>
                            <Link to={"/login"} className={"h-btn login"}>Mon compte</Link>
                            
                        </>
                    }
                    <div onClick={showSidebar} className='menu_bottom'>
                    {sidebar ? (
                        <CloseOutlined className=''/> 
                        ) : (
                        <MenuOutlined  className='' /> 
                        )}
                    </div>
                </div>
            </nav>
            <div className= {sidebar ? 'mobileSidebar active ': 'mobileSidebar'}>
                <ul className='mobile-menu'>
                    <li><Link to="/properties/sale">A vendre</Link></li>
                    <li><Link to="/properties/rent">A louer</Link></li>
                    <li><Link to="/services">Services</Link></li>
                    <li><Link to="/agency-list">Agences</Link></li>
                </ul>
                <Link to={"/evaluate"} className="saved mobile-saved">
                <TimelineOutlined />
                    Evaluation de bien
                </Link>
            </div>
        </div>
    )
}

export default Navigation;