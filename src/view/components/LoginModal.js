import {Link} from "react-router-dom";
import {
    CloseOutlined, InfoOutlined
} from "@material-ui/icons";

const LoginModal = ({state, toggle}) => {
    return(
        <div className={state ? "account-mask show" : "account-mask"}>
            <div className={"account-mask-content"}>
                <CloseOutlined onClick={() => toggle(!state)} className="m-close" />
                <div className={"diserve-content"}>
                    <div className={"icon"}>
                        <InfoOutlined />
                    </div>
                    <p>Pour effectuer cette action vous devez être connecter</p>
                    <div className={"sign-side"}>
                        <Link className={"orange"} to={"/login"}>Se connecter</Link>
                        <hr />
                        <Link to={"/register"}>S'inscrire</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginModal;