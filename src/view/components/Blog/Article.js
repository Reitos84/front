import { Schedule } from "@material-ui/icons";
import { Link } from "react-router-dom";

const Article = () => {
    return(
        <Link to={"/blog/article/belle-appartement"} className="article">
            <div className="image">
                <img src="https://images.pexels.com/photos/1732414/pexels-photo-1732414.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" />
            </div>  
            <div className="present">
                <div className="title">
                    Which green features do homes with high EPC ratings have?
                </div>
                <div className="date">
                    <Schedule />
                    25 janvier 2022
                </div>
            </div>
        </Link>
    );
}

export default Article;