import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import BlogCategoryService from "../../../services/api/BlogCategoryService";

const BlogCategories = () => {

    const service = new BlogCategoryService();

    const [list, setList] = useState([]);

    useEffect(() => {
        service.getAll().then((res) => {
            setList(res['hydra:member']);
        }, []);
    }, []);

    return(
        <div className='categories-side'>
            <div className='title'>
                Cat&eacute;gories
            </div>
            <div className="cat-list">
                {
                    list.map(x => <Link key={x.id} to={''}>{ x.title }</Link>)
                }
            </div>
        </div>
    );
}

export default BlogCategories;