import { ScheduleOutlined } from "@material-ui/icons";
import { Link } from "react-router-dom";

const PrimaryArticle = () => {
    return(
        <Link to={""} className='primary-article'>
            <div className="image-background">
                <img src="https://images.pexels.com/photos/1732414/pexels-photo-1732414.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" />
                <div className="text">
                    <div className="date">
                        <ScheduleOutlined />
                        25 juillet 2022
                    </div>
                    <div className="title">Top 10 des endroits ou vivre</div>
                </div>
            </div>
        </Link>
    );
}

export default PrimaryArticle;