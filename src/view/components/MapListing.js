import React, {useState} from "react";
import {useLocation} from "react-router-dom";
import GoogleMapReact from "google-map-react";
import '../css/map-lister.css';
import {
    ArrowBackOutlined,
    YoutubeSearchedFor
} from "@material-ui/icons";
import Pined from "./Pined";

const MapListing = ({listing, toggleSide, total, page, setPage, setFindNow}) => {

    const [index, setIndex] = useState(0);

    const link = useLocation();

    let params = new URLSearchParams(link.search);
    let boundary = params.has('boundary') && params.get('boundary').length > 0 ? JSON.parse(params.get('boundary')) : null;
    let defaultParams = {
        lat: 5.316667,
        lng: -4.033333
    };
    if(boundary && boundary.length === 2){
        defaultParams = {lat: boundary[1], lng: boundary[0]};
    }

    const defaultProps = {
        center: defaultParams,
        zoom: boundary && boundary.length === 2 ? 15 : 12,
        mapTypeId: "roadmap",
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    return(
        <div className={"wide-map-container"}>
            <div onClick={() => toggleSide('list')} className={"exit-map"}>
                <ArrowBackOutlined />
                Fermer la carte
            </div>
            {
                listing.length < total ?
                    <div onClick={() => {setPage(page + 1); setFindNow(true)}} className={"load-more"}>
                        <YoutubeSearchedFor />
                        Afficher plus
                    </div>
                :
                    <></>
            }
            <GoogleMapReact
                bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
                center={defaultProps.center}
                defaultZoom={defaultProps.zoom}
                yesIWantToUseGoogleMapApiInternals={true}
            >
                { listing.map(property => <Pined selectIndex={setIndex} index={index} draggable={false} lat={property.lat} lng={property.lng} property={property} />) }
            </GoogleMapReact>
        </div>
    )
}

export default MapListing;