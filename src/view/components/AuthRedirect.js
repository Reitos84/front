import {useSelector} from "react-redux";
import { useHistory } from "react-router-dom";

const AuthRedirect = () => {

    const history = useHistory();

    const data = useSelector(state => state.auth.data);

    if(!data){
        history.goBack();
        return(
            <></>
        );
    }else{
        return(
            <></>
        )
    }
}

export default AuthRedirect;