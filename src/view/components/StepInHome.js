import {FormatListNumberedOutlined, PhoneAndroidOutlined, SearchOutlined} from "@material-ui/icons";

const StepInHome = () => {
    return(
        <div className="container-fluid">
            <div className='m-container'>
                  <div className={"steper"}>
                      <div className={"uno-step"}>
                          <SearchOutlined />
                          <div>
                              <span>Rechercher</span>
                              Un moteur de recherche intelligent et puissant pour trouver la propriété idéale
                          </div>
                      </div>
                      <div className={"uno-step"}>
                          <FormatListNumberedOutlined />
                          <div>
                              <span>Comparer</span>
                               Des fonctionnalités pour mieux comparer vos sélections pour une meilleure analyse.
                          </div>
                      </div>
                      <div className={"uno-step"}>
                          <PhoneAndroidOutlined />
                          <div>
                              <span>Décider </span>
                              Des solutions pour facilement communiquer avec les agences et les propriétaires
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    );
}

export default StepInHome;