import {thousandsSeparators} from 'currency-thousand-separator';

const InterestProperty = ({property}) => {

    let currentPic = null;
    property.pictures.forEach(pic => {
        if(pic.isBackground){
            currentPic = pic;
        }
    });

    if(!currentPic){
        currentPic = property.pictures[0];
    }


    return(
        <div className={"property-sub"}>
            <span className={"type"}>{ property.type.title }</span>
            <div className={"picture"}>
                <img alt={currentPic.name} className={"picture"} src={ `${process.env.REACT_APP_BASE_PATH}${currentPic.url}` } />
            </div>
            <div className={"body"}>
                <div className={"title"}>{ property.title }</div>
                <div className={"price"}>{process.env.REACT_APP_CURRENCY} { thousandsSeparators(property.price) }</div>
            </div>
        </div>
    )
}

export default InterestProperty;