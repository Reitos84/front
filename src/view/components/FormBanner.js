import React, { useRef } from 'react';
import {useEffect, useState} from "react";
import '../css/formbanner.css';
import {CloseOutlined, GpsFixedOutlined, SwapVert} from "@material-ui/icons";
import TypeService from "../../services/api/TypeService";
import Geopify from "../../services/places/Geopify";

const FormBanner = () =>{
    const [selected, setSelected] = useState('sale');
    const [typeList, setTypeList] = useState([]);
    const [addresses, setAdresses] = useState([]);
    const [location, setLocation] = useState('');
    const [boundary, setBoundary] = useState('');
    const [selectedType, setSelectedType] = useState('');
    const [pieces, setPieces] = useState('');
    const [price, setPrice] = useState('');
    const [activeFullFinder, setActivateFullFinder] = useState(false);

    const typeService = new TypeService();
    const geopify = new Geopify();

    const handleChangeSelectedCategory = (value) => {
        setSelected(value);
    }

    const findArea = async (key) => {
        if(key.length > 0){
            const result = await geopify.locationSearch(key);
            setAdresses(result.features);
        }
    };

    useEffect(() => {
        typeService.getAll().then(result => {
            setTypeList(result['hydra:member']);
        });
        return ;
    }, []);

    const handleSelectPredit = (value) => {
        setLocation(value.properties.address_line1);
        setBoundary(JSON.stringify(value.geometry.coordinates));
        setActivateFullFinder(false);
    }

    const blurInput = () => {
        setTimeout(() => {
            setAdresses([]);
        }, 400);
    }

    const handleChangeType = (e) => {
        setSelectedType(e);
        if(e !== 'house' || e !== 'appartment' || e !== 'villa'){
            setPieces('');
        }
    }

    const finderRef = useRef(null);

    return(
        <div className="form_banner">
            <form method="get" action={`/properties/${selected}`} className="form">
               <div className="ckeck-box">
                    <div onClick={() => handleChangeSelectedCategory('sale')} className={selected === 'sale' ? "check-box_item active" : "check-box_item"}>
                        A Vendre
                    </div>
                    <div onClick={() => handleChangeSelectedCategory('rent')} className={selected === 'rent' ? "check-box_item active" : "check-box_item"}>
                        A Louer
                    </div>
                </div>
                <div className={ activeFullFinder ? "responsive-finder active" : "responsive-finder"}>
                    <CloseOutlined onClick={() => setActivateFullFinder(false)} className='full-closer' />
                    <div className='full-form'>
                        <input ref={finderRef} autoFocus value={location} onChange={(e) => { findArea(e.currentTarget.value); setLocation(e.currentTarget.value); }}  placeholder='Saisir un emplacement' className='' />
                    </div>
                    {
                        addresses.length > 0 && location.length > 0 ?
                        <>
                            <div className='s-separe'>
                                R&eacute;sultat
                            </div>
                            <div className='finded adresses'>
                                {
                                    addresses.map((elem, id) => {
                                        if(elem.properties.address_line1){
                                            return (
                                                <div onClick={() => handleSelectPredit(elem)} key={id} className='fd-element'>
                                                    <strong>{elem.properties.address_line1}</strong>
                                                    <span>{elem.properties.city}, {elem.properties.state_code}</span>
                                                </div>
                                            )
                                        }
                                    })
                                }
                            </div>
                        </>
                        :
                        <div className='nt-found'>
                            Aucun r&eacute;sultat pour l'instant
                        </div>
                    }
                    
                </div>
                <div className='formFields'>
                    <div className="form-group distant">
                        <label>Où ?</label>
                        <div>
                            <input onFocus={() => {
                                setActivateFullFinder(true);
                                finderRef.current.focus();
                            }} name={"location"} onBlur={blurInput} value={location} onChange={(e) => { findArea(e.currentTarget.value); setLocation(e.currentTarget.value);  }} type="text"  placeholder="Entrer une localisation" className="form-control"/>
                            <GpsFixedOutlined />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Quoi ?</label>
                        <div>
                            <select onChange={e => handleChangeType(e.currentTarget.value)} value={selectedType} name={"type"} className="form-control placeholder">
                                <option value={""}>Indifférent</option>
                                {
                                    typeList.map(type => {
                                        if(!type.category || type.category === selected){
                                            return <option key={type.id} value={type.slug}>{type.title}</option>
                                        }
                                    })
                                }
                            </select>
                            <SwapVert />
                        </div>
                    </div>
                    {
                        selectedType === 'villa' || selectedType === 'appartment' || selectedType === 'house' ?
                        <div className="form-group">
                            <label>Pièce ?</label>
                            <div>
                                <select value={pieces} onChange={e => setPieces(e.currentTarget.value)} name={"bedroom"} className="form-control placeholder">
                                    <option value="">Choisir</option>
                                    <option value="0">Studio</option>
                                    <option value="1">1 chambre</option>
                                    <option value="2">2 chambres</option>
                                    <option value="3">3 chambres</option>
                                    <option value="4">4 chambres</option>
                                    <option value="5">5 chambres</option>
                                </select>
                                <SwapVert />
                            </div>
                        </div>
                        :
                        <></>
                    }
                    <div style={{display: "none"}}>
                        <input type={"text"} name="boundary" defaultValue={boundary}/>
                    </div>
                    <div className="form-group">
                        <label>Budget max</label>
                        <div>
                            <input type={"number"} onChange={e => setPrice(e.currentTarget.value)} name="priceMax" value={price}/>
                        </div>
                    </div>
                    <button className="banner-formBtn">Rechercher</button>
                </div>
            </form>

        </div>


    )
}

export default FormBanner;