import { Link } from "react-router-dom";

const UnavaillableProperty = () => {
    return(
        <div className={"houses-content"}>
            <div className="unaivalable">
                <div className="text-side">
                    <div className="title">Propriété non trouvée</div>
                    <div className="explain">
                        Cette propriété n'est plus disponible ou a été supprimer par l'annonceur.
                    </div>
                    <div className="link">
                        <Link to={"/properties/sale"}>Consulter les propriétés à vendre à Abidjan</Link>
                        <Link to={"/properties/rent"}>Consulter les propriétés à louer à Abidjan</Link>
                    </div>
                </div>
                <div className=""></div>
            </div>
        </div>
    )
}

export default UnavaillableProperty;