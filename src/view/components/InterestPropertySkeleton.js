import { Skeleton } from "react-skeleton-generator";

const InterestPropertySkeleton = () => {
    return(
        <div className={"property-sub"}>
            <Skeleton.SkeletonThemeProvider color="#FAFAFA">
                <Skeleton height="300px" width="100%" />
                <Skeleton height="25px" width="80%" />
                <Skeleton height="20px" width="80%" />
            </Skeleton.SkeletonThemeProvider>
        </div>
    )
}

export default InterestPropertySkeleton;