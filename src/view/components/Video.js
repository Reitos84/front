import React from 'react';
import { Link } from 'react-router-dom';
import explore from "../assets/explore.jpg";
//import video from "../assets/video360.MOV";

const Video = () =>{
    return(
        <div className="compagneBg  video mb-5">
            <img src={explore} className={'img-fluid'} alt={'explore'} />
            <div className="compagneBg-gradient"></div>
            {/* <video src={video} autoPlay loop muted className='img-fluid'></video> */}
            <div className="b-textBox">
                <h2 className="sectionTitle white ">
                    Explorez votre prochaine <br></br>
                    <span>maison depuis votre position</span>
                </h2>
                <button className="campagneLink">Disponible bientôt</button>
            </div>
        </div>
    )
}

export default Video;