import { Skeleton } from "react-skeleton-generator";

const FeaturePropertySkeleton  = () => {
    return(
        <div className="col-lg-4 mb-5">
        <Skeleton.SkeletonThemeProvider color="#FAFAFA" className=' '>
            <div className={"feature-skeleton-content"}>
                <div className={"feature-skeleton-header"}>
                    <div className={"path"}>
                        <Skeleton width="100%" height="300px" borderRadius="2px" />
                    </div>
                </div>
                <div className={"feature-skeleton-description"}>
                    <div className={"skeleton-ligneRow"}>
                        <Skeleton width="100%" height="28px" marginTop="2px" borderRadius="2px" />
                       
                    </div>
                </div>
            </div>
        </Skeleton.SkeletonThemeProvider>
        </div>
    )
}                      

export default FeaturePropertySkeleton ;