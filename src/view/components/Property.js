import {
    BathtubOutlined,
    FavoriteBorderOutlined,
    SingleBedOutlined, SquareFootOutlined,
    FavoriteOutlined, RoomOutlined
} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {thousandsSeparators} from 'currency-thousand-separator';
import * as moment from "moment";
import 'moment/locale/fr';

const Property = ({data, savedIndex, handleSave}) => {

    let currentPic = null;
    data.pictures.forEach(pic => {
        if(pic.isBackground){
            currentPic = pic;
        }
    });

    if(!currentPic){
        currentPic = data.pictures[0];
    }

    let params = new URLSearchParams();
    params.append('title', data.title);
    params.append('owner', data.agency.name);
    params.append('location', data.area);
    params.append('price', data.price);


    let len = 0;

    return(
        <Link to={{pathname: `/property-detail/${data.code}/${ data.slug }`, search: params.toString()}} >
            <div className={"property-block"}>
                <div className={"agency-avatar"}>
                    {
                        data.agency.avatarUrl ?
                        <img className={"lozad"} alt={data.agency.avatarUrl} src={`${process.env.REACT_APP_BASE_PATH}${data.agency.avatarUrl}`} />
                        :
                        <div style={{background: data.agency.color}}>{ data.agency.name.charAt(0) }</div>
                    }
                </div> 
                <div className={"type"}>
                    {data.type.title}
                </div>
                <div className={"pictures-elem"}>
                    <div className={"pictures-long"}>
                        {
                            currentPic ?
                                <img alt={currentPic.name} className={"picture lozad"} src={ `${process.env.REACT_APP_BASE_PATH}${currentPic.url}` } />
                            :
                                ""
                        }
                    </div>
                    <div className={"pictures-small"}>
                        {
                            data.pictures.map(pic =>{
                                if(pic && pic.id !== currentPic.id && len < 2){
                                    len++;
                                    return <img key={pic.id} alt={pic.name} className={"picture lozad"} src={ `${process.env.REACT_APP_BASE_PATH}${pic.url}` } />
                                }
                            })
                        }
                    </div>
                </div>
                <div className={"property-body"}>
                    <div className={"id"}>
                        Id propriété : { data.code }
                    </div>
                    <div className={"title"}>
                        { data.title }
                    </div>
                    <div className={"location"}>
                        <RoomOutlined /> { data.area }
                    </div>
                    <div className={"sub-element"}>
                        {
                            data.bedroom ?
                                <div>
                                    <SingleBedOutlined />
                                    {data.bedroom > 1 ? `${data.bedroom} chambres` : `${data.bedroom} chambre` }
                                </div>
                                :
                                <></>
                        }
                        {
                            data.shower ?
                                <div>
                                    <BathtubOutlined />
                                    {data.shower > 1 ? `${data.shower} douches` : `${data.shower} douche` }
                                </div>
                                :
                                <></>
                        }
                        {
                            data.space ?
                                <div>
                                    <SquareFootOutlined />
                                    {data.space} m<sup>2</sup>
                                </div>
                                :
                                <></>
                        }
                    </div>
                    <div className={"desc"}>
                        { data.description.substring(0, 180) }
                        { data.description.length > 180 ? '...' : '' }
                    </div>
                    <div className={"date"}>
                        Ajouté { moment(data.createdAt).calendar() } par { data.agency.name }
                    </div>
                    <div className={"property-bottom"}>
                        <div className={"cost"}>
                            { `${process.env.REACT_APP_CURRENCY} ${thousandsSeparators(data.price)}` }
                            { data.frequency ? `/${data.frequency}` : '' }
                        </div>
                        <div onClick={(e) => { e.preventDefault(); handleSave(); }} className={"love"}>
                            {
                                savedIndex ?
                                    <FavoriteOutlined className="colored-blue" />
                                    :
                                    <FavoriteBorderOutlined className="colored-blue" />
                            }

                        </div>
                    </div>
                </div>
            </div>
            
        </Link>
    )
}

export default Property;