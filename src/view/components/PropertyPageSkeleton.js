import { Skeleton } from "react-skeleton-generator";

const PropertyPageSkeleton = () => {
    return(
        <Skeleton.SkeletonThemeProvider color="#FAFAFA">
            <div className={"houses-content"}>
                <div className={"house-header"}>
                    <div className={"path"}>
                        <Skeleton width="30%" height="17px" borderRadius="10px" />
                    </div>
                    <div className={"house-sub-divisions"}>
                        <div>
                            <div className={"house-title"}>
                                <Skeleton width="100%" height="35px" borderRadius="10px" />
                            </div>
                            <div className={"house-address"}>
                                <Skeleton width="100%" height="20px" borderRadius="10px" />
                            </div>
                            <div className={"sub-divisions-elements"}>
                                <Skeleton width="700px" height="25px" borderRadius="10px" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"house-content"}>
                    <div className={"house-picture-big"}>
                        <Skeleton width="100%" height="400px" borderRadius="10px" />
                    </div>
                    <div style={{marginTop: "40px"}} className={"house-details"}>
                        <div className="house-body">
                            <Skeleton width="100%" height="1000px" borderRadius="10px" />
                        </div>
                        <div className="agent-contact">
                            <Skeleton width="100%" height="400px" borderRadius="10px" />
                        </div>
                    </div>
                </div>
            </div>
        </Skeleton.SkeletonThemeProvider>
    )
}

export default PropertyPageSkeleton;