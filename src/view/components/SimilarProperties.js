import Similar from "./Similar";
import {useEffect, useState} from "react";
import { useSelector } from "react-redux";
import PropertyService from "../../services/api/PropertyService";
import SavedService from "../../services/api/SavedService";

const SimilarProperties = ({data, handleRequestLogin}) => {

    const [list, setList] = useState([]);

    const propertyService = new PropertyService();
    const savedService = new SavedService();
    const user = useSelector(state => state.auth.data);

    const handleSaveProperty = async (property) => {
        if(user){
            if(property.iSaved){
                const result = await savedService.removeSave(property.iSaved);
                if(result.status === 204){
                    setList(list.map(x => {
                        if(x.id === property.id){
                            x.iSaved = null;
                        }
                        return x;
                    }));
                }
            }else{
                const result = await savedService.saveProperty(property.id, user.id);
                if(result.id){
                    setList(list.map(x => {
                        if(x.id === property.id){
                            property.iSaved = result.id;
                            return property;
                        }
                        return x;
                    }));
                }
            }
        }else{
            handleRequestLogin(true);
        }
    }

    useEffect(() => {
        let locale = {
            lat: parseFloat(data.lat.toFixed(2)),
            lng: parseFloat(data.lng.toFixed(2))
        };
        propertyService.filterProperty(data.category.id, {type: data.type.id}, 1, locale.lat, locale.lng).then((result) => {
            if(result['hydra:totalItems'] > 0){
                setList(result['hydra:member'].slice(0, 4));
            }
        });
    }, []);

    return(
        list.length > 0 ?
            <div className={"similar-properties"}>
                <div className={"house-title-list with-arrow"}>
                    <div className={"title"}>Propriétés similaires</div>
                </div>
                <div  className={'similar-slide'}>
                    {
                        list.map(x => {
                            if(x.id !== data.id){
                                return <Similar save={handleSaveProperty} key={x.id} data={x} />
                            }
                        })
                    }
                </div>
            </div>
        :
            <></>
    )
}

export default SimilarProperties;