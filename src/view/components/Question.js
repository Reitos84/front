import {AddCircleOutlineOutlined, RemoveCircleOutlineOutlined} from "@material-ui/icons";
import nl2br from "react-nl2br";

const Question = ({faq, index, handleSelect}) => {



    return(
        <div className={ index === faq.id ? "help extends" : "help"}>
            <div className="top">
                <div className="title">
                    { faq.question }
                </div>
                {
                    index === faq.id ?
                        <RemoveCircleOutlineOutlined onClick={handleSelect} />
                    :
                        <AddCircleOutlineOutlined onClick={handleSelect} />
                }
            </div>
            <div className={"desc"}>
                { nl2br(faq.answer) }
            </div>
        </div>
    )
}

export default Question;