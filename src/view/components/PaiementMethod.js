import {CloseOutlined} from "@material-ui/icons";
import visa from "../assets/logo/visa.svg";
import mastercard from "../assets/logo/mastercard.svg";
import {useEffect, useState} from "react";
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import loader from '../assets/loader/785.svg';
import spinner from '../assets/loader/Spinner.gif';
import UserService from "../../services/api/UserService";
import CardService from "../../services/api/CardService";
import { useDispatch, useSelector } from "react-redux";
import { updateInfos } from "../../core/reducer/authSlice";


const PaiementMethod = () => {

    const userService = new UserService();
    const cardService = new CardService(); 
    const user = useSelector(state => state.auth.data);
    const dispatch = useDispatch();

    const [card, setCard] = useState({
        cvc: '',
        expiry: '',
        focus: '',
        name: '',
        number: '',
    });
    const [postalCode, setPostalCode] = useState('');
    const [addCard, setAddCard] = useState(false);
    const [message, setMessage] = useState('');
    const [load, setLoad] = useState(false);
    const [issuer, setIssuer] = useState('');
    const [cardList, setCardList] = useState([]);
    const [listLoad, setListLoad] = useState(true);
    const [findagain, setFindagain] = useState(0);

    const handleInputFocus = (e) => {
        setCard({...card, focus: e.target.name });
    }

    const handleInputChange = (e) => {
        let { name, value } = e.target;
        if(e.target.name === 'number'){
            if(card.number.length === 4 && value.length === 5){
                value = `${value.substring(0, 4)} ${value.substring(4, 5)}`;
            }
            if(card.number.length === 9 && value.length === 10){
                value = `${value.substring(0, 9)} ${value.substring(9, 10)}`;
            }
            if(card.number.length === 14 && value.length === 15){
                value = `${value.substring(0, 14)} ${value.substring(14, 15)}`;
            }
        }
        if(e.target.name === 'expiry'){
            if(card.expiry.length === 2 && value.length === 3){
                if(!value.includes('/')){
                    value = `${value.substring(0, 2)}/${value.substring(2, 3)}`;
                }
            }
            if(value.length > 5){
                value = card.expiry
            }
        }
        if(e.target.name === 'cvc'){
            if(value.length > 3){
                value = card.cvc
            }
        }
        if(value.length < 20){
            setCard({...card, [name]: value });
        }
    }

    const handleSaveCard = async () => {
        if(Object.values(card).every(field => field.length > 0)){
            setMessage('');
            setLoad(true);
            if(postalCode.length > 0){
                userService.editProfile({
                    postalcode: postalCode
                }, user.id).then(result => {
                    dispatch(updateInfos(result));
                });
            }

            let result = await cardService.verifyCard(card.number);
            if(result['hydra:member'].length > 0){
                setMessage('Cette carte est déjà associé à un compte')
            }else{
                cardService.saveCard({...card, issuer: issuer}, user.id).then(data => {
                    if(data.id > 0){
                        setLoad(false);
                        setCard({
                            cvc: '',
                            expiry: '',
                            focus: '',
                            name: '',
                            number: '',
                        });
                        setAddCard(false);
                        setPostalCode('');
                        setFindagain(findagain + 1);
                    }
                });
            }
        }else{
            setMessage('Veillez remplir le formulaire');
        }
    }

    const handleCardChange = (e) => {
        setIssuer(e.issuer);
    }

    useEffect(async () => {
        let result = await cardService.getMyCard(user.id);
        if(result['hydra:member'].length > 0){
            setCardList(result['hydra:member']);
        }
        setListLoad(false);
    }, [findagain]);

    return(
        <div className="method-block">
            <div className={ addCard ? "account-mask show" : "account-mask"}>
                <div className={"account-mask-content widest"}>
                    <CloseOutlined onClick={() => setAddCard(!addCard)} className={"m-close"} />
                    <h3>Ajouter un mode de paiement</h3>
                    <div className={"acepted-cards"}>
                        <img src={visa} alt={"visa"} />
                        <img src={mastercard} alt={"mastercard"}  />
                    </div>
                    <div className={"simple-flex"}>
                        <div>
                            <div className={"box"}>
                                <input
                                    type={"text"}
                                    placeholder={"Nom sur la carte"}
                                    name={"name"}
                                    value={card.name}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                />
                            </div>
                            <div className={"box"}>
                                <label>Informations de la carte</label>
                                <input
                                    type={"text"}
                                    placeholder={"Numéro de la carte"}
                                    name={"number"}
                                    value={card.number}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                />
                            </div>
                            <div className={"box none area"}>
                                <input
                                    type={"text"}
                                    placeholder={"Expiration"}
                                    name={"expiry"}
                                    value={card.expiry}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                />
                                <input
                                    type={"text"}
                                    placeholder={"Cryptogramme"}
                                    name={"cvc"}
                                    value={card.cvc}
                                    onChange={handleInputChange}
                                    onFocus={handleInputFocus}
                                />
                            </div>
                            <br />
                            <div className={"box"}>
                                <input onChange={(e) => setPostalCode(e.target.value)} value={postalCode} type={"text"} placeholder={"Code Postal (facultatif)"} />
                            </div>
                        </div>
                        <Cards
                            cvc={card.cvc}
                            expiry={card.expiry}
                            focused={card.focus}
                            name={card.name}
                            number={card.number}
                            acceptedCards={['visa', 'mastercard']}
                            callback={handleCardChange}
                        />
                    </div>
                    <div className="message">
                        {message}
                    </div>
                    <button onClick={handleSaveCard} className={"saveBtn"}>
                        { load ? <img src={loader} alt={"Chargement en cours"} /> : '' }
                        Enregistrer
                    </button>
                </div>
            </div>
            <div className={"add-paiment-method"}>
                <p>
                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                </p>
                <div className={"card-list"}>
                    {
                        listLoad ? <img alt="loader" className={"spinner"} src={spinner} /> : ''
                    }
                    {
                        cardList.map(unocard => <div className="card-display">
                            <img src={unocard.issuer === 'visa' ? visa : mastercard} alt={"card-logo"} />
                            <div className={"card-details"}>
                                Fini par: **** **** **** { unocard.number.substr(-4) }
                                <span>Nom sur la carte : {unocard.name}</span>
                                <span className="expiry">{unocard.expiry}</span>
                            </div>
                        </div>)
                    }
                </div>
                <button onClick={() => setAddCard(!addCard)} className={"saveBtn tiny"}>Ajouter un mode de paiement</button>
            </div>
        </div>
    )
}

export default PaiementMethod;