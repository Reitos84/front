import { Skeleton } from "react-skeleton-generator";
const PropertySkeleton = () => {
    return(
        <Skeleton.SkeletonThemeProvider color="#FAFAFA">
            <div className={"property-block"}>
                <div className={"pictures-elem"}>
                    <div className={"pictures-long"}>
                        <Skeleton width="98%" height="100%" />
                    </div>
                    <div className={"pictures-small"}>
                        <Skeleton width="100%" height="50%" />
                        <Skeleton width="100%" height="50%" />
                    </div>
                </div>
                <div className={"property-body"}>
                    <Skeleton width="30%" height="17px" borderRadius="10px" />
                    <Skeleton width="100%" height="19px" borderRadius="10px" />
                    <Skeleton width="100%" height="19px" borderRadius="10px" />
                    <Skeleton width="100%" height="100px" borderRadius="10px" />
                    <Skeleton width="60%" height="15px" borderRadius="10px" />
                    <Skeleton width="40%" height="25px" borderRadius="10px" />
                </div>
            </div>
        </Skeleton.SkeletonThemeProvider>
    )
}

export default PropertySkeleton;