import React from 'react';
import {Link} from "react-router-dom";
import { ArrowRightAlt } from "@material-ui/icons";
import commune from "../assets/abidjan.png";

const LocationCard = ({data, indexed}) =>{
    let params = new URLSearchParams();
    params.append('location', data.area);
    params.append('p', (Math.random().toFixed(2) * 100));

    return(
        <Link to={{pathname: '/search', search: params.toString()}} className={ indexed ? "col-midle" : "col-top"}>
            <div className='locationInfo'>
                <p className="commune_name">{ data.area }</p>
                <span className="nbr_properties">+ { data.total } proprietés</span>
            </div>
            {
                indexed ? 
                    <img 
                        alt="Comune feature image" 
                        className="commune_img" 
                        src={`${process.env.REACT_APP_BASE_PATH}${data.image}`} 
                        />
                :
                    ''
            }
            <span className="linkBtn">Voir plus <ArrowRightAlt/></span>
        </Link>
    )
}

export default LocationCard;