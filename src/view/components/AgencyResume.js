import '../css/featureStyle.css';
import "../css/agency.css";
import {
    MailOutline,
    CheckCircle,
    PhoneAndroidOutlined,
    LocalPhone
} from "@material-ui/icons";
import parnaireOne from "../assets/partenaire.png";
import PropertyService from '../../services/api/PropertyService';
import { useEffect,useState } from 'react';
import { useParams } from 'react-router-dom';
import { Link } from "react-router-dom";
import Property from './Property';
import EmptyList from './EmptyList';
import PropertySkeleton from "../components/PropertySkeleton";
import defaultImage from "../assets/agency-image.jpg";
import nl2br from "react-nl2br";

const AgencyResume = ({agency, title}) => {
    const propertyService = new PropertyService();
    const [listing, setListing] = useState([]);
    const [totalAgencyProperty, setTotalAgencyProperty] = useState(0);
    const [page, setPage]= useState(1);
    const [load, setLoad] = useState(true);


    const params = useParams();
    const agencyId = params.id;
    const agencyName = params.name;

    useEffect(() =>{
        setLoad(true);
        propertyService.getAllForAgency(agencyId, page).then((result) => {
            if(page === 1){
                setListing(result['hydra:member']);
                setTotalAgencyProperty(result['hydra:totalItems']);
            }else{
                setListing([...listing, ...result['hydra:member']]);
            }
            setLoad(false);
        })
    }, [page]);

    let certifiedIcon = '';
    if(agency.isCertified){
        certifiedIcon = <CheckCircle className='certifiedIcon' alt="Certifié" title="Certifié" />
    }

    return(
        <>
        <div className="agency-header">
            <div className='gardienStyle for-agency'>
                <div className="path">
                    <Link to={""}>Accueil</Link> | <Link to={"/agency-list"}>Agences</Link>
                </div>
                <div className={"big-text"}>
                    <h1>
                        { certifiedIcon }
                        {title}
                    </h1>
                </div>
            </div>
            {
                agency ?
                    agency.avatarUrl ?
                        <img alt={agencyName} className=" agencyAvatar" src={`${process.env.REACT_APP_BASE_PATH}${agency.avatarUrl}`}  />
                        :
                        <img alt={title} className=" agencyAvatar" src={defaultImage}  />
                    :
                <></>
            }
        </div>
        <div className="m-container">
            <div className='agence_site_items'>
                <div className='agence_site_main'>
                    <div className="agency-information">
                        <h2 className='sectionTitle agency-resume'> Tous savoir sur {agencyName}</h2>
                        <div className="desciption-texte">
                            <p>
                               { nl2br(agency.comment) }
                            </p>
                        </div>
                        <div className='agency_properties-list'>
                           <h2 className='sectionTitle agency-resume mt-5'>Nos propriétés</h2>
                           <div className='a-listing'>
                                {
                                    load ?
                                    <>
                                        <PropertySkeleton/>
                                        <PropertySkeleton/>
                                        <PropertySkeleton/>
                                        <PropertySkeleton/>
                                    </>
                                    :
                                        listing.length > 0 ?
                                            listing.map( y =>
                                                <Property
                                                    key={y.id}
                                                    data={y}
                                                />
                                            )
                                    :
                                        <EmptyList />
                                }
                           </div>
                           <div className={listing.length == 0 ?"pagination-index_none":"paginate-index"}>
                                <div className={"number"}>
                                    Afficher { listing.length } sur { totalAgencyProperty } propriétés
                                </div>
                                <div className="liner">
                                    <span style={{ width: `${(listing.length / totalAgencyProperty ) * 100}%` }}></span>
                                </div>
                                {
                                    listing.length < totalAgencyProperty ?
                                        <div onClick={() => setPage(page + 1)} className="selected">Afficher plus</div>
                                    :
                                        ""
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className='agence_site_info'>
                    <div className='contact_info'>
                        <div className='contact_info__liste'>
                           <MailOutline /><a href={`mailto:${agency.email}`}>{agency.email}</a>
                        </div>
                        {
                            agency.mobilephone ?
                                <div className='contact_info__liste'>
                                  <PhoneAndroidOutlined /><a target={`_blank`} href={`https://wa.me/${agency.mobilephone}`}>{agency.mobilephone}</a>
                                </div>
                            :
                                <></>
                        }
                        {
                          agency.officephone ?
                            <div className='contact_info__liste'>
                               <LocalPhone /><a href={`tel:agency.officephone`}>{agency.officephone}</a>
                            </div>
                          :
                            <></>
                        }
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default AgencyResume;
