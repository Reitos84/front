import { CheckCircle, PhoneOutlined } from "@material-ui/icons";
import { Link } from "react-router-dom";
import defaultImage from "../assets/agency-image.jpg";

const AgencyElement = ({data}) => {
    let certifiedIcon = '';
    if(data.isCertified){
        certifiedIcon = <CheckCircle />
    }

    return(
        <div className="uno-agency">
            <span className="count">{data.propertyTotal} propriété{ data.propertyTotal > 1 ? 's' : '' }</span>
            {
                data.avatarUrl ?
                    <img alt={data.name} src={ data.avatarUrl ? `${process.env.REACT_APP_BASE_PATH}${data.avatarUrl}` : defaultImage }  />
                :
                    <div style={{
                        backgroundColor: data.color 
                    }} className="avatar-text">
                        { data.name.charAt(0) }
                    </div>
            }
            <div className="uno-agency-title">
                <span>
                    { certifiedIcon }
                    { data.name }
                </span>
                { data.localisation }
            </div>
            <div className="bottom">
                <Link className="btn" to={`/agency/${data.name}/${data.id}`}>
                    Voir le profile
                </Link>
                <a className="call" href={`tel:${data.mobilephone}`}>
                    <PhoneOutlined />
                </a>
            </div>
        </div>
    )
}

export default AgencyElement;