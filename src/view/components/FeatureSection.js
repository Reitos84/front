import React,{useEffect, useState} from 'react';
import FeatureService from '../../services/api/FeatureService.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/featureStyle.css';
import FeatureProperty from './FeatureProperty';
import FeaturePropertySkeleton from "../components/FeaturePropertySkeleton";
import {useSelector} from "react-redux";
import PropertyService from '../../services/api/PropertyService';
import LoginModal from "./LoginModal";

const FeatureSection = ({currentId}) =>{

    const user = useSelector(state => state.auth.data);

    // services
    const featureService = new FeatureService();
    const propertyService = new PropertyService();

    //States
    const [listing, setListing] = useState([]);
    const [load, setLoad] = useState(true);
    const nbreSkeleton =[1,2,3,4,5,6];
    const [requestLogin, setRequestLogin] = useState(false);

    useEffect(() => {
        featureService.getAll().then(result => {
            if(result['hydra:totalItems'] >= 6){
                setListing([...result['hydra:member'].slice(0, 6)]);
            }else{
                if(result['hydra:totalItems'] > 0){
                    propertyService.getLatest().then(comp => {
                        let arr = [...result['hydra:member']];
                        for (let index = 0; index < comp['hydra:member'].length; index++) {
                            if(arr.length < 6){
                                let me = comp['hydra:member'][index];
                                if(result['hydra:member'].every(x => x.property.id !== me.id)){
                                    arr.push(me);
                                }
                            }
                        }
                        setListing([...arr]);
                    });
                }else{
                    propertyService.getLatest().then(comp => {
                        setListing([...result['hydra:member'], ...comp['hydra:member'].slice(0, 6)]);
                    });
                }
            } 
            setLoad(false);
        });
    },[]);

    useEffect(() => {
        if(listing.length > 0){
            let elements = [];
            listing.forEach(x => {
                if(x['@type'] === 'Featured'){
                    elements.push(x.id);
                }
            });
            if(elements.length > 0){
                featureService.addPrint(elements);
            }
        }
    }, [listing]);

    return(
        <>
            <LoginModal state={requestLogin} toggle={setRequestLogin} />
            <div className="container-fluid">
                <div className='m-container'>
                    <h2 className='sectionTitle'> <span className='smallTitle'>Des propriétés</span> <span>Rien que pour vous</span></h2>
                    <div className='row'>
                        {
                            !load ?
                                listing.map( (data, index) =>
                                <FeatureProperty
                                    user={user}
                                    toggleLoginModal={() => setRequestLogin(!requestLogin)}
                                    key={index}
                                    data={data['@type'] === 'Featured' ? data.property : data}
                                />)
                                :
                                nbreSkeleton.map(elem => <FeaturePropertySkeleton key={elem} />)
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

export default FeatureSection;