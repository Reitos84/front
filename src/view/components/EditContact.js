import {CloseOutlined} from "@material-ui/icons";
import {useState} from "react";
import UserService from "../../services/api/UserService";
import loader from "../assets/loader/785.svg";
import {useSelector, useDispatch} from "react-redux";
import {updateInfos} from "../../core/reducer/authSlice";

const EditContact = ({show, close}) => {

    const user = useSelector(state => state.auth.data) ?? {};
    const userService = new UserService();

    const dispatch = useDispatch();

    const [contact, setContact] = useState(user.mobilephone ? user.mobilephone : '');
    const [sendCode, setSendCode] = useState(false);
    const [load, setLoad] = useState(false);
    const [code, setCode] = useState('');
    const [message, setMessage] = useState('');
    const [retry, setRetry] = useState(null);

    const sendSms = () => {
        setLoad(true);
        userService.sendConfirmSms(contact).then(res => {
            if(res.success){
                setSendCode(true);
                setRetry(60);
                setInterval((e) => {
                    setRetry(retry - 1);
                    if(retry === 0){
                        clearInterval(e);
                    }
                }, 1000);
            }else{
                if(res.message === "Invalid number"){
                    setMessage("Veillez saisir un contact valide");
                }else{
                    setMessage('Une erreur s\'est produite, essayer a nouveau');
                }
            }
            setLoad(false);
        });
    }

    const submitCode = () => {
        setLoad(true);
        userService.codeVerify(code).then(result => {
            if(result.success){
                userService.editProfile({
                    mobilephone: contact
                }, user.id).then(data => {
                    if(data.id){
                        dispatch(updateInfos(data));
                        close();
                    }else{
                        setLoad(false);
                        setMessage('Une erreur s\'est produite, verifiez vos informations');
                    }
                });
            }else{
                setLoad(false);
                setMessage('Le code de confirmation est incorrect');
            }
        });
    }

    return(
        <div className={show ? "account-mask show" : "account-mask"}>
            <div className={"account-mask-content"}>
                <CloseOutlined onClick={close} className={"close"} />
                <h3>Ajouter / Modifier votre contact</h3>
                {
                    sendCode === false ?
                        <>
                            <div className={"box"}>
                                <label>Saisir votre contact</label>
                                <input placeholder={"+225xxxxxxxxxx"} type={"text"} name={"contact"} value={contact} onChange={e => setContact(e.currentTarget.value)}  />
                            </div>
                        </>
                        :
                        <>
                            <p>
                                Un code a été envoyé à { contact }.
                            </p>
                            <div className={"box"}>
                                <label>Code de vérification</label>
                                <input placeholder={"xxxx"} type={"text"} value={code} onChange={e => setCode(e.currentTarget.value)}  />
                            </div>
                            <p>
                                Vous n'avez pas reçu, <strong className={retry > 0 ? '' : "blue"} onClick={sendSms}>Rééssayez { retry > 0 ? retry : '' }</strong>
                            </p>
                        </>
                }
                <div className={"message"}>
                    { message }
                </div>
                <button onClick={sendCode ? submitCode : sendSms} className={"btn blue"}>
                    {
                        load ? <img alt={"veillez patienter"} src={loader} /> : ''
                    }
                    {
                        sendCode ? 'Valider' : 'Enregistrer'
                    }
                </button>
            </div>
        </div>
    );
}

export default EditContact;