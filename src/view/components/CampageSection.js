import campagne from "../assets/campagnie.jpg";
import {Link }from 'react-router-dom';
import {ArrowForward, ArrowForwardOutlined} from "@material-ui/icons";

const CampageSection=() =>{
    return(

        <div className="compagneBg mb-5">
            <img src={campagne} alt="concept de campagne" className="img-fluid"/>
            <div className="compagneBg-gradient"></div>
            <div className="m-container">

                <div className="b-textBox">
                    <h2 className="sectionTitle white ">
                        Acheter des propriétés dans <br></br>
                        <span>un building en construction</span>
                    </h2>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. <br/>Vitae quo consequatur illum eaque officiis facere dicta a, alias nulla recusandae voluptas accusantium ad illo consectetur!</p>
                    <Link to="" className="campagneLink">
                        Voir les promotions <ArrowForwardOutlined />
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default CampageSection;
