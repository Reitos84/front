import  '../css/banner.css';
import FormBanner from './FormBanner';


const Banner = () =>{
    return(
        <div className="banner">
            <div className="banner_content">
                <div className="text-intro">
                {/* <h1 className={'big home'}><span>Trouvez votre</span> <span>prochain</span> <span className="text-color">endroit idéal</span> <span>pour vivre</span></h1> */}
                <h1 className={'big home'}><span>A la recherche d'un bien</span> <span className="text-color">Immobilier?</span></h1>
                    <p className='banner-para'>Vous êtes au bon endroit.</p>
                </div>
                <FormBanner />
            </div>
        </div>
    )
}

export default Banner;