import { Skeleton } from "react-skeleton-generator";

const NotificationSkeleton = () => {
    return(
        <Skeleton.SkeletonThemeProvider color="#C5C5C5">
            <Skeleton width="1000" height="18" />
        </Skeleton.SkeletonThemeProvider>
    )
}

export default NotificationSkeleton;