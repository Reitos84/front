import {useSelector} from "react-redux";
import { useHistory } from "react-router-dom";

const RedirectWhenConnected = () => {

    const history = useHistory();

    const data = useSelector(state => state.auth.data);

    if(data){
        if(history.length > 2){
            history.goBack();
        }else{
            window.location = '../';
        }
        return(
            <></>
        );
    }else{
        return(
            <></>
        )
    }
}

export default RedirectWhenConnected;