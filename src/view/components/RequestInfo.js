import {CloseOutlined} from "@material-ui/icons";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateInfos} from "../../core/reducer/authSlice";
import UserService from "../../services/api/UserService";
import loader from "../assets/loader/785.svg";

const RequestInfo = () => {

    const user = useSelector(state => state.auth.data);
    const [show, setShow] = useState(user && !user.isAsked);

    const [gender, setGender] = useState('');
    const [message, setMessage] = useState('');
    const [mobilephone, setMobilephone] = useState('');
    const [load, setLoad] = useState(false);
    const [code, setCode] = useState('');
    const [retry, setRetry] = useState(0);
    const [email, setEmail] = useState('');

    const [sendCode, setSendCode] = useState(false);

    const [presentSide, setPresentSide] = useState('request');

    const userService = new UserService();

    const dispatch = useDispatch();

    useEffect( () => {
        if(sendCode){
            sendSmsCode();
        }
    }, [sendCode]);

    const interval = () => {
        setInterval(() => {
            setRetry(retry - 1);
            if(retry === 0){
                clearInterval(this)
            }
        }, 1000);
    }

    const sendSmsCode = async () => {
        setRetry(false);
        userService.sendConfirmSms(mobilephone).then(res => {
            if(res.success){
                setPresentSide('confirm');
            }else{
                if(res.message === "Invalid number"){
                    setMessage("Veillez saisir un contact valide");
                }else{
                    setMessage('Une erreur s\'est produite, essayer a nouveau');
                }
            }
        });
        setRetry(60);
        interval();
    }

    const handleChangeInfos = () => {
        setLoad(true);
        if(mobilephone.length > 0){
            setMessage('');
            let action = true;
            let infos = {
                gender: gender,
                isAsked: true
            };
            if(user && (!user.email || user.email === ' ')) {
                if(email.length > 0){
                    infos.email = email;
                }else{
                    action = false;
                }
            }
            if(action){
                userService.editProfile(infos, user.id).then(data => {
                    if(data.id){
                        dispatch(updateInfos(data));
                        setSendCode(true);
                        setLoad(false);
                    }else{
                        setLoad(false);
                        setMessage('Une erreur s\'est produite, verifiez vos informations');
                    }
                }).catch(e => {
                    setLoad(false);
                    setMessage('Une erreur s\'est produite, verifiez vos informations');
                });
            }else{
                setLoad(false);
                setMessage('Veillez bien remplir le formulaire');
            }
        }else{
            setLoad(false);
            setMessage('Veillez bien remplir le formulaire');
        }
    }

    const verifyCode = async () => {
        setLoad(true);
        let result = await userService.codeVerify(code);
        if(result.success){
            userService.editProfile({
                mobilephone: mobilephone
            }, user.id).then(data => {
                if(data.id){
                    dispatch(updateInfos({...user, mobilephone: mobilephone}));
                    handleClose();
                }else{
                    setLoad(false);
                    setMessage('Une erreur s\'est produite, verifiez vos informations');
                }
            });
        }else{
            setLoad(false);
            setMessage('Le code de confirmation est incorrect');
        }
    }

    const handleClose = () => {
        setShow(!show);
        dispatch(updateInfos({...user, isAsked: true}));
    }

    return(
        <div className={ show ? "account-mask show" : "account-mask"}>
            {
                presentSide !== 'confirm' ?
                    <div className={"account-mask-content"}>
                        <CloseOutlined onClick={handleClose} className="m-close" />
                        <h3>Completez votre profil</h3>
                        <p>

                        </p>
                        {
                            user && (!user.email || user.email === ' ') ?
                                <div className={"box"}>
                                    <label>Email</label>
                                    <input name={"email"} value={email} onChange={(e) => setEmail(e.currentTarget.value)} type={"text"} />
                                </div>
                            :
                                <></>
                        }
                        {
                            user && !user.gender ?
                                <div className={"box"}>
                                    <label>Genre</label>
                                    <select value={gender} onChange={(e) => setGender(e.currentTarget.value)}>
                                        <option value={""}>Selectionner une valeur</option>
                                        <option value={"homme"}>Homme</option>
                                        <option value={"femme"}>Femme</option>
                                    </select>
                                </div>
                            :
                                <></>
                        }
                        <div className={"box"}>
                            <label>Numéro de téléphone</label>
                            <input name={"contact"} onChange={(e) => setMobilephone(e.currentTarget.value)} value={mobilephone} type={"text"} placeholder={"+225xxxxxxxxxx"} />
                        </div>
                        <div className={"message"}>{ message }</div>
                        <button onClick={handleChangeInfos} className={"btn blue"}>
                            {
                                load ? <img alt={"veillez patienter"} src={loader} /> : ''
                            }
                            Enregistrer
                        </button>
                    </div>
                :
                    <div className={"account-mask-content"}>
                        <CloseOutlined onClick={() => setShow(!show)} className="m-close" />
                        <h3>Confirmer le numéro de téléphone</h3>
                        <p>
                            Un code a été envoyer par sms au <strong>{ mobilephone }</strong>, consultez votre messagerie
                        </p>
                        <div className={"box"}>
                            <label>Code de confirmation</label>
                            <input onChange={(e) => setCode(e.currentTarget.value)} value={code} type={"text"} placeholder={"XXXX"} />
                        </div>
                        <div className={"message"}>{ message }</div>
                        <button onClick={verifyCode} className={"btn blue"}>
                            {
                                load ? <img alt={"veillez patienter"} src={loader} /> : ''
                            }
                            Confirmer
                        </button>
                        <br />
                        <br />
                        {
                            retry ?
                                <p>
                                    Vous n'avez pas reçu, <strong className={retry > 0 ? '' : "blue"} onClick={sendSmsCode}>Rééssayez { retry > 0 ? retry : '' }</strong>
                                </p>
                            :
                                ""
                            }

                    </div>
            }
        </div>
    )
}

export default RequestInfo;