import {
    FavoriteBorderOutlined,
    RoomOutlined,
    SingleBedOutlined,
    BathtubOutlined,
    SquareFootOutlined,
    FavoriteOutlined
} from "@material-ui/icons";
import {Link} from "react-router-dom";
import {thousandsSeparators} from "currency-thousand-separator";

const Similar = ({data, save}) => {

    let currentPic = null;
    data.pictures.forEach(pic => {
        if(pic.isBackground){
            currentPic = pic;
        }
    });

    if(!currentPic){
        currentPic = data.pictures[0];
    }

    let params = new URLSearchParams();
    params.append('title', data.title);
    params.append('owner', data.agency.name);
    params.append('location', data.area);
    params.append('price', data.price);

    return(
        <Link to={{pathname: `/property-detail/${data.code}/${ data.slug }`, search: params.toString()}} className={"similar-element"}>
            <div className={"similar-type"}>
                {data.type.title}
            </div>
            {
                currentPic ?
                    <img className={"preview"} alt={"picture-current"} src={`${process.env.REACT_APP_BASE_PATH}${currentPic.url}`} />
                    :
                    ""
            }
            <div className={"similar-body"}>
                <div className={"agency-logo"}>
                    {
                        data.agency.avatarUrl ?
                        <img className={"lozad"} alt={data.agency.avatarUrl} src={`${process.env.REACT_APP_BASE_PATH}${data.agency.avatarUrl}`} />
                        :
                        <div style={{background: data.agency.color}}>{ data.agency.name.charAt(0) }</div>
                    }
                </div>
                <div className={"id"}>ID Propriété: { data.code }</div>
                <div className={"title"}>{ data.title }</div>
                <div className={"location"}>
                    <RoomOutlined />
                    {
                        data.area.substring(0, 33)
                    }
                    { data.area.length > 33 ? '...' : '' }
                </div>
                <div className={"similar-sample"}>
                    {
                        data.bedroom ?
                            <div className={"similar-amenities"}>
                                <SingleBedOutlined />
                                {data.bedroom > 1 ? `${data.bedroom} chambres` : `${data.bedroom} chambre` }
                            </div>
                            :
                            <></>
                    }
                    {
                        data.shower ?
                            <div className={"similar-amenities"}>
                                <BathtubOutlined />
                                {data.shower > 1 ? `${data.shower} douches` : `${data.shower} douche` }
                            </div>
                            :
                            <></>
                    }
                    {
                        data.space ?
                            <div className={"similar-amenities"}>
                                <SquareFootOutlined />
                                {data.space} m<sup>2</sup>
                            </div>
                            :
                            <></>
                    }
                </div>
                <div className={"similar-footer"}>
                    <div className={"price"}>
                        { `${process.env.REACT_APP_CURRENCY} ${thousandsSeparators(data.price)}` }
                        { data.frequency ? `/${data.frequency}` : '' }
                    </div>
                    <div onClick={(e) => {
                        e.preventDefault();
                        save(data);
                    }} className={data.iSaved ? "heart active" : "heart"}>
                        {
                            data.iSaved ?
                                <FavoriteOutlined />
                            :
                                <FavoriteBorderOutlined />
                        }
                    </div>
                </div>
            </div>
        </Link>
    )
}

export default Similar;