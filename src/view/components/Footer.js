import { Facebook, Instagram, Twitter, LinkedIn } from '@material-ui/icons';
import React from 'react';
import { Link } from "react-router-dom";
import '../css/footer.css';

const Footer = () =>{
    return(
       <section className="footer">
            <div className="footer-container">
                <div className="row">
                    <div className="col-lg-3 mb-5">
                        <h1 className="logoStyle">Abidjan Habitat <span>.</span></h1>
                        <div>Abidjan, cocody Rue 34</div>
                        <a href="mailto:support@abidjanhabitat.com" >support@abidjanhabitat.com</a>
                        <div className="media">
                            <a href="https://mobile.facebook.com/abidjanimmobiliers/?_rdc=1&_rdr">
                                <Facebook />
                            </a>
                            <a href="https://www.instagram.com/abidjanhabitat1">
                                <Instagram />
                            </a>
                            <a href="https://twitter.com/abidjanhabitat1?s=11&t=pv5Z1W9EwZSb6HhlPp_9vg">
                                <Twitter />
                            </a>
                            <a href="https://www.linkedin.com/company/abidjan-habitat">
                                <LinkedIn />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <h6>Compagnie</h6>
                        <ul>
                            <li><Link to="/about-us">A propos</Link></li>
                            <li><Link to="/values">Nos valeurs</Link></li>
                            <li><Link to="/policy">Politiques de confidentialités</Link></li>
                            <li><Link to="/users-conditions">Termes et conditions d'utilisation</Link></li>
                            <li><Link to="/legal-notice">Mentions Légales</Link></li>
                            <li><Link to="/help-center">Faqs</Link></li>
                        </ul>
                    </div>
                    <div className="col-lg-3">
                         <h6>Services</h6>
                        <ul>
                            <li><Link to="/project-marketing">Commercialisation de Projet</Link></li>
                            <li><Link to="/relocation-service">Services de relocalisation</Link></li>
                            <li><Link to="/investissements">Investissement Immobilier</Link></li>
                            <li><Link to="/property-management">Gestion immobilière</Link></li>
                            <li><Link to="/representation-sales">Représentations de ventes</Link></li>
                        </ul>
                    </div>
                    <div className="col-lg-3">
                        <h6>Ressources</h6>
                        <ul>
                            <li><a target={"_blank"} href={process.env.REACT_APP_LEGAL_FILE}>Les lois sur la vente et la location </a></li>
                            <li><a target={"_blank"} href={process.env.REACT_APP_FORMATION_LINK}>Formation et Diplôme</a></li>
                            <li><a target={"_blank"} href={process.env.REACT_APP_AGENCY_LINK}>Espace agence</a></li>
                            <li><Link to="/feedback">Donner votre avis</Link></li>
                        </ul>
                    </div>
                </div>
            </div>
       </section>
    )
}

export default Footer;