import React, { Component } from "react";
import Slider from "react-slick";
import parnaireOne from "../assets/partenaire.png";
// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default class SwipeToSlide extends Component {
    render() {
      const settings = {
        className: "center",
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 5,
        swipeToSlide: true,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 2000,
        responsive:[
            {
                breakpoint: 1700,
                settings: {
                  slidesToShow: 4
                }
            },

            {
                breakpoint: 1300,
                settings: {
                  slidesToShow: 3
                }
            },

            {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2
                }
            },

            
            {
                breakpoint: 576,
                settings: {
                  slidesToShow: 1
                }
            }
        ]
      };
      return (
          <div>
            <div className="Container_fluid mt-5">
            <div className="m-container margin-top">
                <Slider {...settings}>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                    <div className="bg-color">
                        <img src={parnaireOne }/>
                    </div>
                </Slider>
            </div>
        </div>
        </div>

      );
    }
  }


