import {Link} from "react-router-dom";
import {BathtubOutlined, CloseOutlined, SingleBedOutlined, SquareFootOutlined} from "@material-ui/icons";
import {thousandsSeparators} from "currency-thousand-separator";

const Pined = ({property, selectIndex, index}) => {

    let currentPic = null;
    property.pictures.forEach(pic => {
        if(pic.isBackground){
            currentPic = pic;
        }
    });

    if(!currentPic){
        currentPic = property.pictures[0];
    }

    let params = new URLSearchParams();
    params.append('title', property.title);
    params.append('owner', property.agency.name);
    params.append('location', property.area);
    params.append('price', property.price);

    const lostIndex = (e) => {
        e.preventDefault();
        selectIndex(0);
    }

    return (
        <div className={index === property.id ? "on-map selected" : "on-map"}>
            {
                index === property.id ?
                    <Link to={{pathname: `/property-detail/${property.code}/${ property.slug }`, search: params.toString()}} className={"property-thumbnail"}>
                        <CloseOutlined onClick={lostIndex} className={"close"} />
                        {
                            currentPic ?
                                <img alt={currentPic.name} className={"picture"} src={ `${process.env.REACT_APP_BASE_PATH}${currentPic.url}` } />
                                :
                                ""
                        }
                        <div className={"property-thumbnail-details"}>
                            <div className={"title"}>
                                { property.title.substring(0, 26) }
                                { property.title.length > 24 ? '...' : '' }
                            </div>
                            <div className={"sub-element"}>
                                {
                                    property.bedroom ?
                                        <div>
                                            <SingleBedOutlined />
                                            {property.bedroom > 1 ? `${property.bedroom} chambres` : `${property.bedroom} chambre` }
                                        </div>
                                        :
                                        <></>
                                }
                                {
                                    property.shower ?
                                        <div>
                                            <BathtubOutlined />
                                            {property.shower > 1 ? `${property.shower} douches` : `${property.shower} douche` }
                                        </div>
                                        :
                                        <></>
                                }
                                {
                                    property.space ?
                                        <div>
                                            <SquareFootOutlined />
                                            {property.space} m<sup>2</sup>
                                        </div>
                                        :
                                        <></>
                                }
                            </div>
                            <div className={"price"}>
                                { `${process.env.REACT_APP_CURRENCY} ${thousandsSeparators(property.price)}` }
                                { property.frequency ? `/${property.frequency}` : '' }
                            </div>
                        </div>
                    </Link>
                    :
                    <></>
            }
            <div onClick={() => selectIndex(property.id)} className={"dot"}>
                <span className={"pined"} >
                    <span />
                </span>
            </div>
        </div>
    );
}

export default Pined;