import React from 'react';
import GoogleMapReact from "google-map-react";

const ShowMap = ({boundary}) =>{



    let defaultParams = {
        lat: 5.316667,
        lng: -4.033333
    };
    if(boundary){
        defaultParams = {lat: boundary.lat, lng: boundary.lng};
    }

    const defaultProps = {
        center: defaultParams,
        zoom: boundary ? 13.3 : 11,
        mapTypeId: "roadmap",
        styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    };

    const createCircle = ({map, maps}) => {
        return new maps.Circle({
            strokeColor: '#343c6a',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.1,
            map,
            center: defaultParams,
            radius: 2000,
        });
    }
    
    return(
        <GoogleMapReact
            bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
            center={defaultProps.center}
            defaultZoom={defaultProps.zoom}
            yesIWantToUseGoogleMapApiInternals={true}
            onGoogleApiLoaded={boundary ? createCircle : ''}
        >
        </GoogleMapReact>
    )
}

export default ShowMap ;