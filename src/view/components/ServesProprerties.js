import React,{useState, useEffect} from 'react';
import LocationService from '../../services/api/LocationService';
import LocationCard from './LocationCard';
import '../css/servesProperties.css';


const ServesProperties = () =>{
    const locationService = new LocationService();
    const [listing, setListing] =  useState([]);

    useEffect (async () => {
        let result = await locationService.getLocation();
        setListing(result);
    }, []);

    return(
        <section>
            <div className="container-fluid">
                <div className='m-container'>
                <h2 className='sectionTitle serve-title'> <span className='smallTitle'>Trouvez votre maison</span> <span>La ou vous voulez</span></h2>
                    <div className="row">
                        {
                            listing.length > 5 ?
                            <div className="location_row">
                                <div className="row-1">
                                    <LocationCard data={listing[1]} />
                                    <LocationCard data={listing[2]}  />
                                </div>
                                <div className="row-2">
                                    <LocationCard indexed={true} data={listing[0]} />
                                </div>
                                <div className="row-1">
                                    <LocationCard data={listing[3]} />
                                    <LocationCard data={listing[4]}  />
                                </div>
                            </div>
                            :
                                ''
                        }
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ServesProperties;