import React, { useState, useEffect } from 'react';
import FeaturesFaqService from '../../services/api/FeatureFaqService';
import NewsletterService from '../../services/api/NewsletterService';
import '../css/faq.css';
import {ArrowForward, CheckCircleOutlined} from "@material-ui/icons";
import {Link} from "react-router-dom";

const Faq = () => {

    const featuresFaqService = new FeaturesFaqService();
    const newsletterService = new NewsletterService();

    const [list, setList] = useState([]);
    const [selected, setSelected ] = useState(null);
    const [email, setEmail] = useState('');
    const [successSendFaq, setSuccessSendFaq] = useState(false);

    const toggle = (i) =>{ 
        if(selected === i){
            return setSelected(null)
        }
        setSelected(i);
    }

    const addOnFaq = async () => {
        try{
            let query = await newsletterService.get(email);
            if(!query['hydra:totalItems']){
                let sender = await newsletterService.post(email);
                if(sender.id){
                    setSuccessSendFaq(true);
                    setEmail('');
                }
            }else{
                setSuccessSendFaq(true);
                setEmail('');
            }
        }catch(err){

        }
    }

    useEffect(() => {
        featuresFaqService.getAll().then((result) => {
            setList(result['hydra:member']);
        });
    },  []);

    return(
        <div className='container-fluid'>
            <div className={successSendFaq ? "success-process display" : "success-process"}>
                <div className={"succes-process-content"}>
                    <div className={"icon"}>
                        <CheckCircleOutlined />
                    </div>
                    <div className={"tit"}>Réussie</div>
                    <div>Merci de vous inscrire au newsletters, nous allons vous informer régulièrement des mises à jour, offre et promotion.</div>
                    <button onClick={() => setSuccessSendFaq(false)} className={"btn"}>Continuer</button>
                </div>
            </div>
            <div className='m-container'>
                <h2 className="sectionTitle flex">
                    Questions fréquemment posées
                    <Link className={'other'} to={'/help-center'}>
                        Voir plus <ArrowForward />
                    </Link>
                </h2>
                <ul className="faq_list">
                       {list.map((item, i)=>(
                            <li key={i} className="faq_list-item item">
                                <div className="question-title" onClick={()=> toggle(i)}>
                                   <h3 className="">{item.question}</h3>
                                   <span>{selected === i ? '-':'+'}</span>
                                </div>
                                 <div className={selected === i ? 'answers show':'answers'}>
                                       <p>
                                           {item.answer}
                                       </p>
                                 </div>
                           </li>
                       ))}
                </ul>
                {/* NEWSLETTER */}
                <div className="messageBox">
                <div className="row">
                    <div className="col-lg-6">
                        <h3>Recevez les dernières nouvelles et offres spéciales pour vous</h3>
                    </div>
                    <div className="col-lg-6">
                        <div className="question-form">
                            <input value={email} onChange={(e) => setEmail(e.currentTarget.value)} className="" type="email" placeholder="e-mail" />
                            <button onClick={addOnFaq}>Envoyer</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}
export default Faq;