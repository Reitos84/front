import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/splide.min.css';
import {CloseOutlined} from "@material-ui/icons";

const SlideShow = ({images, toggleSlide}) => {

    return(
        <div className={"slide-show"}>
            <CloseOutlined onClick={toggleSlide} className={"close"} />
            <div className={"sliders"}>
                <Splide
                    options={ {
                        rewind: true,
                        width : 800,
                        gap   : '1rem',
                    } }
                >
                    {
                        images.map((img, index) => <SplideSlide key={index}>
                            <div className={"slide-element"}>
                                <img src={`${process.env.REACT_APP_BASE_PATH}${img.thumbnail}`} alt=""/>
                            </div>
                        </SplideSlide> )
                    }

                </Splide>
            </div>
        </div>
    )
}

export default SlideShow;