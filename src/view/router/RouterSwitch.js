import { Switch, Route } from "react-router";
import Home from "../screens/Home";
import PropertyList from "../screens/PropertyList";
import Navigation from "../components/Navigation";
import Footer from "../components/Footer";
import AuthRedirect from "../components/AuthRedirect";
import Login from "../screens/Login";
import Register from "../screens/Register";
import Notification from "../screens/user/Notification";
import Saved from "../screens/user/Saved";
import Knowledge from "../screens/Knowledge";
import Profile from "../screens/user/Profile";
import Security from "../screens/user/Security";
import ResetPassword from "../screens/user/ResetPassword";
import Password from "../screens/Password";
import PropertyPage from "../screens/PropertyPage";
import InterestPage from "../screens/InterestPage";
import AgencyList from "../screens/AgencyList";
import AgencyPresent from "../screens/AgencyPresent";
import ErrorRepport from "../screens/ErrorRepport";
import RedirectWhenConnected from "../components/RedirectWhenConnected";
import Gestion from "../screens/Gestion";
import UsersConditions from "../screens/UsersConditions";
import Policy from "../screens/Policy";
import Investments  from "../screens/Investments";
import Evaluate from "../screens/Evaluate";
import About from "../screens/About";
import Investor from "../screens/Investor";
import ApplyGestion from "../screens/ApplyGestion";
import Search from "../screens/Search";
import Feedback from "../screens/Feedback";
import Meetup from "../screens/Meetup";
import Relocation from "../screens/Relocation";
import SellerRepresentation from "../screens/SellerRepresentation";
import Contact from "../screens/Contact";
import ProjectMarketing from "../screens/ProjectMarketing";
import Services from "../screens/Services";
import LegalNotice from "../screens/LegalNotice";
import Values from "../screens/Values";
import Blog from "../screens/Blog";
import ArticlePage from "../screens/ArticlePage";

const RouterSwitch = () => {
    return(
        <>
            <Switch>
                <Route path="/properties/:categorySlug">
                    <div>
                        <Navigation />
                        <PropertyList />
                        <Footer />
                    </div>
                </Route>
                <Route path="/investissements">
                    <div>
                        <Navigation />
                        <Investments />
                        <Footer />
                    </div>
                </Route>
                <Route path="/services">
                    <div>
                        <Navigation />
                        <Services/>
                        <Footer />
                    </div>
                </Route>
                <Route path="/relocation-service">
                    <div>
                        <Navigation/>
                        <Relocation/>
                        <Footer/>
                    </div>
                </Route>
                <Route path="/representation-sales">
                    <div>
                        <Navigation/>
                        <SellerRepresentation/>
                        <Footer/>
                    </div>
                </Route>
                <Route path="/blog/article/:slug">
                    <div>
                        <Navigation/>
                        <ArticlePage />
                        <Footer/>
                    </div>
                </Route>
                <Route path="/blog">
                    <div>
                        <Navigation/>
                        <Blog />
                        <Footer/>
                    </div>
                </Route>
                <Route path="/investor">
                    <div>
                        <Navigation />
                        <Investor />
                        <Footer />
                    </div>
                </Route>
                <Route path="/feedback">
                    <div>
                        <Navigation />
                        <Feedback />
                        <Footer />
                    </div>
                </Route>
                <Route path="/meetup">
                    <div>
                        <Navigation />
                        <Meetup />
                        <Footer />
                    </div>
                </Route>
                <Route path="/search/:category?">
                    <div>
                        <Navigation />
                        <Search />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/account/notifications"}>
                    <>
                        <Navigation />
                        <AuthRedirect />
                        <Notification />
                        <Footer />
                    </>
                </Route>
                <Route path={"/account/saved-properties"}>
                    <>
                        <Navigation />
                        <AuthRedirect />
                        <Saved />
                        <Footer />
                    </>
                </Route>
                <Route path={"/account/personnal-informations"}>
                    <>
                        <Navigation />
                        <AuthRedirect />
                        <Profile />
                        <Footer />
                    </>
                </Route>
                <Route path={"/account/security"}>
                    <>
                        <Navigation />
                        <AuthRedirect />
                        <Security />
                        <Footer />
                    </>
                </Route>
                <Route path={"/help-center"}>
                    <>
                        <Navigation />
                        <Knowledge />
                        <Footer />
                    </>
                </Route>
                <Route path={"/evaluate"}>
                    <>
                        <Navigation />
                        <Evaluate />
                        <Footer />
                    </>
                </Route>
                <Route path={"/project-marketing"}>
                    <>
                        <Navigation />
                        <ProjectMarketing/>
                        <Footer />
                    </>
                </Route>
                <Route path={"/property-detail/:code/:slug?"}>
                    <div>
                        <Navigation />
                        <PropertyPage />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/error-report/:code/:property"}>
                    <div>
                        <Navigation />
                        <ErrorRepport />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/login"}>
                    <RedirectWhenConnected />
                    <Login />
                </Route>
                <Route path={"/contact"}>
                    <>
                        <Navigation />
                        <Contact/>
                        <Footer />
                    </>
                </Route>
                <Route path={"/register"}>
                    <RedirectWhenConnected />
                    <Register />
                </Route>
                <Route path={"/reset-password"}>
                    <RedirectWhenConnected />
                    <ResetPassword />
                </Route>
                <Route path={"/password"}>
                    <RedirectWhenConnected />
                    <Password />
                </Route>
                <Route path={"/interest/:code/:slug/:agency"}>
                    <div>
                        <Navigation />
                        <InterestPage />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/agency-list"}>
                    <div>
                        <Navigation />
                        <AgencyList />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/about-us"}>
                    <div>
                        <Navigation />
                        <About/>
                        <Footer />
                    </div>
                </Route>

                <Route path={"/values"}>
                    <div>
                        <Navigation />
                        <Values/>
                        <Footer />
                    </div>
                </Route>
                <Route path={"/agency/:name/:id"}>
                    <div>
                        <Navigation />
                        <AgencyPresent />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/property-management/apply"}>
                    <div>
                        <Navigation />
                        <ApplyGestion />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/property-management"}>
                    <div>
                        <Navigation />
                        <Gestion />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/users-conditions"}>
                    <div>
                        <Navigation />
                        <UsersConditions />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/policy"}>
                    <div>
                        <Navigation />
                        <Policy />
                        <Footer />
                    </div>
                </Route>
                <Route path={"/legal-notice"}>
                    <div>
                        <Navigation />
                        <LegalNotice />
                        <Footer />
                    </div>
                </Route>
                <Route path="/">
                    <div className={"website"}>
                        <Navigation />
                        <Home />
                    </div>
                </Route>
            </Switch>
        </>
    )
}

export default RouterSwitch;