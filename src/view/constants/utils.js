export const rentRange = [
    {
        value: [0,70000],
        title: '0 - 50000'
    },
    {
        value: [70000,150000],
        title: '70000 - 150000'
    },
    {
        value: [150000,250000],
        title: '150000 - 250000'
    },
    {
        value: [250000,400000],
        title: '250000 - 400000'
    },
    {
        value: [400000,700000],
        title: '400 000 - 700 000'
    },
    {
        value: [700000,1000000],
        title: '700 000 - 1 000 000'
    },
    {
        value: [1000000,5000000],
        title: '1 000 000 - 5 000 000'
    },
    {
        value: [5000000, ''],
        title: '5000000 - +'
    }
];

export const saleRange = [
    {
        value: [0,5000000],
        title: '0 - 5 000 000'
    },
    {
        value: [5000000,15000000],
        title: '5 000 000 - 15 000 000'
    },
    {
        value: [15000000,45000000],
        title: '15 000 000 - 45 000 000'
    },
    {
        value: [45000000,90000000],
        title: '45 000 000 - 90 000 000'
    },
    {
        value: [90000000,150000000],
        title: '90 000 000 - 150 000 000'
    },
    {
        value: [150000000,500000000],
        title: '150 000 000 - 500 000 000'
    },
    {
        value: [500000000,''],
        title: '500 000 000 - +'
    }
];

export const NumberOfRoom = [
    1,2,3,4,5,'Plus'
]

export const AcceptedOrder = [
    {
        title: "Récents",
        value: 'recent'
    },
    {
        title: "Pris le plus haut",
        value: "priceDown"
    },
    {
        title: "Prix le plus bas",
        value: "priceUp"
    }
]

export const experiences = [
    'Je ne fais que commencer',
    'J\'ai déjà acheté un bien immobilier comme investissement'
];

export const qualifier = [
    'Oui (Individu a valeur nette élevée)',
    'Non'
];

export const investCalendar = [
    '3 mois',
    '6 mois',
    '9 mois',
    '12 mois',
    '+ de 12 mois'
];

export const prizes = [
    '0 - 200,000',
    '200,000 – 500,000',
    '500,000 -1 million',
    '1 million – 5 million',
    '5 million - 10 million',
    '10 million - 20 million',
    '20 million - 50 million',
    '50 million - 100 million',
    '+ de 100 million'
];

export const acceptedRoom = [
    {
        value: 0,
        label: 'Studio'
    },
    {
        value: 1,
        label: '1 chambre'
    },
    {
        value: 2,
        label: '2 chambres'
    },
    {
        value: 3,
        label: '3 chambres'
    },
    {
        value: 4,
        label: '4 chambres'
    },
    {
        value: 5,
        label: '5 chambres'
    },
    {
        value: 'plus',
        label: '+ de 5 chambres'
    }
]