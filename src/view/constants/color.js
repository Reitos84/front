export const primary = "#43AA8B";
export const primaryLight = "#43aa8b27";
export const profileColors = [
    "#023e8a",
    "#e63946",
    "#6b705c",
    "#fb8500",
    "#2a9d8f",
    "#264653",
    "#001219",
    "#ae2012",
    "#f72585",
    "#606c38",
    "#7209b7",
    "#5a189a",
    "#272640",
    "#641220",
    "#3d0e61",
    "#114b5f",
    "#2e294e",
    "#46414b",
    "#7161ef"
]