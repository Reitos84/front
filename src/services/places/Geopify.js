class Geopify {

    key = '';

    constructor(){
        this.key = process.env.REACT_APP_GEOPIFY;
    }

    async neightborhood(lat, lng, category){
        let query = await fetch(`https://api.geoapify.com/v2/places?categories=${category}&filter=circle:${lng},${lat},2000&bias=proximity:${lng},${lat}&lang=fr&limit=5&apiKey=${this.key}`);
        return await query.json();
    }

    async locationSearch(key){
        let query = await fetch(`https://api.geoapify.com/v1/geocode/autocomplete?text=${key}&limit=10&filter=countrycode:ci&lang=fr&apiKey=${this.key}`);
        return await query.json();
    }
}

export default Geopify;