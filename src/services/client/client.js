import axios from "axios";
import { store } from "../../core/store/store";
import { logout } from "../../core/reducer/authSlice";
import LocalStorageService from "../utils/LocalStorageService";

const basePath = process.env.REACT_APP_BASE_API_URL;

const client = axios.create({
    baseURL: basePath
});

let localStorageService = new LocalStorageService();

const refreshToken = async () => {
    let Refresh = localStorageService.getItem('personnal_refresh');
    if(!Refresh){
        localStorage.removeItem('personnal');
        localStorage.removeItem('personnal_refresh');
        store.dispatch(logout());
    }
    const rClient = axios.create({
        baseURL: basePath 
    });
    try {
        let query = await rClient.post('/token/refresh', {
            refresh_token: Refresh
        });
        let data = query.data;
        localStorage.setItem('personnal', data.token);
        localStorage.setItem('personnal_refresh', data.refresh_token);
        return data.token;
    }catch (err){
        localStorage.removeItem('personnal');
        localStorage.removeItem('personnal_refresh');
        store.dispatch(logout());
    }
}


client.interceptors.request.use(
    config => {
        const token = localStorage.getItem('personnal');
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        // config.headers['Content-Type'] = 'application/json';
        return config;
    },
    error => {
        Promise.reject(error)
    }
);

let access_token = '';

client.interceptors.response.use((response) => {
    return response;
}, async (error) => {
    const originalRequest = error.config;
    if(error.response.status === 401){
        if(!access_token){
            if(!originalRequest._retry){
                originalRequest._retry = 1;
                if(!access_token){
                    access_token = await refreshToken();
                }
                client.defaults.headers.common['Authorization'] = "Bearer " + access_token;
                return client(originalRequest);
            }else{
                localStorage.removeItem('personnal');
                localStorage.removeItem('personnal_refresh');
                store.dispatch(logout());
            }
        }else{
            client.defaults.headers.common['Authorization'] = "Bearer " + access_token;
            return client(originalRequest);
        }
    }else{
        return error;
    }
});

export default client;