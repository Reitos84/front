import Service from "./Service";

class FeatureService extends Service{
    async getAll() {
        let query = await this.client.get(`/featureds?page=1`);
        return query.data;
    }

    async addPrint(data){
        let query = await this.client.post(`/featureds/print`, data);
        return query.data;
    }

    async getForProperty(property){
        let params = new URLSearchParams();

        params.append('isActive', 1);
        params.append('property', property);

        let query = await this.client.get(`/featureds?${params.toString()}`);
        return query.data;
    }

    async update(data, index){
        let query = await this.client.put(`/featureds/${index}`, data);
        return query.data;
    }
}
export default FeatureService;