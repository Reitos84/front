import Service from "./Service";

class FaqService extends Service{
    
    async getAll(page=1){
        let query = await this.client.get(`faqs?page=${page}`);
        return query.data;
    }

    async search(key, page=1){
        let query = await this.client.get(`faqs?page=${page}&type=${key}`);
        return query.data;
    }

    async getCategory(){
        let query = await this.client.get(`faq_categories?page=1`);
        return query.data;
    }
}

export default FaqService;