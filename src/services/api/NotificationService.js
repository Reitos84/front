import Service from "./Service";

class NotificationService extends  Service{

    async getMyNotification(person) {
        let query = await this.client.get(`messages?user=${person}`);
        return query.data;
    }

}

export default NotificationService;