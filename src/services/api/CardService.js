import Service from "./Service";

class CardService extends Service{

    async saveCard(data, index){
        data = {
            ...data, 
            type: 'card', 
            owner: this.formatUserPath(index)
        };

        let query = await this.client.post('/paiement_methods', data);
        return query.data;
    }

    async verifyCard(cardNumber){
        let query = await this.client.get(`/paiement_methods?number=${cardNumber}`);
        return query.data;
    }

    async getMyCard(index){
        let query = await this.client.get(`/paiement_methods?owner=${index}`);
        return query.data;
    }
}

export default CardService;