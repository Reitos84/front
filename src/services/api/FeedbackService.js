import Service from "./Service";

class FeedbackService extends Service {
    async post(data){
        return this.client.post(`feedback`, data);
    }
}

export default FeedbackService;