import Service from "./Service";

class LocationService extends Service{
    async getLocation(){
        let query = await this.client.get(`/properties/location?page=1`);
        return query.data;
    }
}

export default LocationService;