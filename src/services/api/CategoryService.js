import Service from "./Service";

class CategoryService extends Service{
    async getWithSlug(slug){
        let query = await this.client.get(`/categories?slug=${slug}`);
        return query.data;
    }
}

export default CategoryService;