import Service from "./Service";

class FeaturesFaqService extends Service{
    
    async getAll(){
        let query = await this.client.get(`/faqs?page=1&isFeatured=1`);
        return query.data;
    }
}

export default FeaturesFaqService;