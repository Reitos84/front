import Service from "./Service";

class TypeService extends Service{

    allType = null;

    async getAll(){
        if(this.allType){
            return this.allType;
        }else{
            let query = await this.client.get('/property_types');
            this.allType = query.data;
            return query.data;
        }
    }

    async getWithType(index){

    }
}

export default TypeService;