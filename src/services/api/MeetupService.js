import Service from "./Service";

class MeetupService extends Service{
    async post(data){
        return this.client.post(`meetups`, data);
    }
}

export default MeetupService;