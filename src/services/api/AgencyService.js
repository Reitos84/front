import Service from "./Service";

class AgencyService extends Service{

    async getAgency(page=1){
        let query = await this.client.get(`users?page=${page}&roles=ROLE_AGENCY&isEnabled=1`);
        return await query.data;
    }

    async searchAgency(name, page) {
        let query = await this.client.get(`users?page=${page}&name=${name}&roles=ROLE_AGENCY&isEnabled=1`);
        return await query.data;
    }

    async get(id){
        let query = await this.client.get(`users/${id}`);
        return query.data;
    }
}

export default AgencyService;