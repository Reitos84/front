import Service from "./Service";

class InterestService extends Service{
    
    async iAlreadySendInterest(propertyId, email){
        return await this.client.get(`/interests?page=1&property=${propertyId}&email=${email}&done=${false}`);
    }

    async postInterest(data){
        data = {
            ...data,
            done: false
        };
        return await this.client.post(`/interests`, data);
    }

}

export default InterestService;