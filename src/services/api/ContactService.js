import Service from "./Service";

class ContactService extends Service{
    post(data){
        return this.client.post('/contacts', data);
    }
}

export default ContactService;