import Service from "./Service";

class GestureService extends Service{
    post(data){
        return this.client.post('/gestures', data);
    }
}

export default GestureService;