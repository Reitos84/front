import Service from "./Service";

class UserService extends Service{
    async emailIsExist(email){
        let query = await this.client.get(`/users?username=${email}`);
        return query.data;
    }

    async register(data){
        return await this.client.post('/users', data);
    }

    async Provide(token){
        this.client.defaults.headers.common.Authorization = `Bearer ${token}`;
        const response = await this.client.get(`/provide`, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
        return await response.data;
    }

    async connect(username, password){
        try{
            const response = await this.client.post('/login', {
                username: username,
                password: password
            });
            return response.data;
        }catch(error){
            return {
                error: true,
                message: 'Votre email ou mot de passe est incorrect'
            };
        }
    }

    async editProfile(details, index){
        const response = await this.client.put(`/users/${index}`, details);
        return await response.data;
    }

    async changePassword(password){
        const query = await this.client.post('/change/password', {password: password});
        return await query.data;
    }

    async sendConfirmSms(contact){
        let params = new URLSearchParams();
        params.append('contact', contact);

        const query = await this.client.get(`/send/confirm?${params.toString()}`);
        return await query.data;
    }

    async codeVerify(code){
        let query = await this.client.get(`/verify/code/${code}`);
        return query.data;
    }
}

export default UserService;