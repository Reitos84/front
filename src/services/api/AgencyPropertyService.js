import Service from "./Service";

class AgencyPropertyService extends Service{
    
    async getAll(page=1,agence){
        let query = await this.client.get(`/properties?page=${page}&agency=${agence}`);
        return query.data;
    }

}

export default AgencyPropertyService;