import Service from "./Service";

class BlogCategoryService extends Service{
    async getAll(){
        let query = await this.client.get('/blog_categories');
        return query.data;
    }
}

export default BlogCategoryService;