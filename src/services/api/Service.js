import client from "../client/client";

class Service{

    client = null;

    constructor() {
        this.client = client;
    }

    formatUserPath(index){
        return `${process.env.REACT_APP_USER_PATH}${index}`;
    }
}

export default Service;