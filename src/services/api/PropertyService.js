import Service from "./Service";

class PropertyService extends Service{

    async getWithSlug(code) {
        let query = await this.client.get(`/properties?code=${code}`);
        return query.data; 
    }

    async filterProperty(category, finder, page, lat=null, lng=null){
        let query = await this.client.get(this.queryFactory(category, finder, page, lat, lng));
        return query.data;
    }

    queryFactory(category, finder, page, lat=null, lng=null){
        let url = `/properties?deleted=false&status=disponible&category=${category}&page=${page}`;
        if(lat || lng){
            if(lat){
                url += `&lat[between]=${lat[0]}..${lat[0]}`;
            }
            if(lng){
                url += `&lng[between]=${lng[0]}..${lng[1]}`;
            }
        }
        if(finder.type){
            url += `&type=${finder.type}`;
        }
        if(finder.bedroom){
            url += `&bedroom=${finder.bedroom}`;
        }
        if(finder.kitchen){
            url += `&saloon=${finder.kitchen}`;
        }
        if(finder.shower){
            url += `&saloon=${finder.shower}`;
        }
        if(finder.priceMin && finder.priceMax){
            url += `&price[between]=${finder.priceMin}..${finder.priceMax}`;
        }else{
            if(finder.priceMin){
                url += `&price[gt]=${finder.priceMin}`;
            }
            if(finder.priceMax){
                url += `&price[lt]=${finder.priceMax}`;
            }
        }
        if(finder.order){
            if(finder.order === 'recent'){
                url += `&order[createdAt]=desc`;
            }else if(finder.order === 'priceDown'){
                url += `&order[price]=desc`;
            }else if(finder.order === 'priceUp'){
                url += `&order[price]=asc`;
            }
        }
        return url;
    }

    async getLatest(){
        let query = await this.client.get(`/properties?deleted=false&status=disponible&page=1&order[createdAt]=desc`);
        return query.data;
    }

    async getAllProperty(page=1){
        let query = await this.client.get(`/properties?page=${page}`);
        return query.data;
    }

    async setViewed(index){
        let query = await this.client.get(`/property/view/${index}`);
        return query.data;
    }

    async getWithCategory(page,categoryId){
        let query = await this.client.get(`/properties?page=${page}&category=${categoryId}`);
        return query.data;
    } 

    async getWithQuery (categoryId,data){
        let query = await this.client.get(`/properties?category=${categoryId}&type=${data.type}&city=${data.search}&price=${data.price}`);
        return query.data;
    }

    async getAddress(key){
        let query = await this.client.get(`/properties/address?key=${key}`);
        return query.data;
    }

    async getForAgency(index){
        let query = await this.client.get(`/properties?agency=${index}`);
        return query.data;
    }

    async getAllForAgency(agence, page=1){
        let query = await this.client.get(`/properties?page=${page}&agency=${agence}`);
        return query.data;
    }
}

export default PropertyService;
