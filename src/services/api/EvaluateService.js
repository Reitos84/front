import Service from "./Service";

class EvaluateService extends Service{
    async post(data){
        return this.client.post(`/estimates`, data);
    }
}

export default EvaluateService;