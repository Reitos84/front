import Service from "./Service";

class InvestorService extends Service{
    async post(data){
        return this.client.post(`investors`, data);
    }
}

export default InvestorService;