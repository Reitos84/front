import Service from "./Service";

class SavedService extends Service{

    async getAll(index, page){
        let query = await this.client.get(`saveds?page=${page}&client=${index}`);
        return query.data;
    }

    async saveProperty(propertyId, userId){
        let query = await this.client.post(`saveds`, {
            client: `/api/users/${userId}`,
            property: `/api/properties/${propertyId}`
        });
        return query.data;
    }

    async removeSave(index) {
        return await this.client.delete(`/saveds/${index}`);
    }
}

export default SavedService;