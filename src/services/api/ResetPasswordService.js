import Service from "./Service";

class ResetPasswordService extends Service{
    async requestEmail(email){
        const query = await this.client.post('/reset_password', {
            email: email,
            side: 'client'
        });
        return query.data;
    }

    async changePassword(data){
        const query = await this.client.post('/reset_password/resolve', data);
        return query.data;
    }

    async verifyParams(data){
        const query = await this.client.post('/reset_password/check', data);
        return query.data;
    }
}

export default ResetPasswordService;