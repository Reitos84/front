import Service from "./Service";

class PropertyAlerteService extends Service{
    async post(data){
        return this.client.post('/property_alertes', data);
    }

    async getAlerte(filters){
        let keys = Object.keys(filters);
        let values = Object.values(filters);

        let params = new URLSearchParams();
    
        keys.forEach((x, key) => {
            params.append(x, values[key]);
        });
        return this.client.post(`/property_alertes?${params.toString()}`);
    }
}

export default PropertyAlerteService;