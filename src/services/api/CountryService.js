import Service from "./Service";

class CountryService extends Service {
    async get(){
        const query = this.client.get('/countries');
        return query;
    }
}

export default CountryService;