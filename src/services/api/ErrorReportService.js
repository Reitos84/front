import Service from "./Service";

class ErrorReportService extends Service {
    async post(data){
        let query = await this.client.post('/property_reports', {...data, provide: false});
        return query.data;
    }
}

export default ErrorReportService;