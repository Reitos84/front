import Service from './Service';

class NewsletterService extends Service {
    
    async post(email){
        let query = await this.client.post('/newsletters', {email: email});
        return query.data;
    }

    async get(email){
        let params = new URLSearchParams();
        params.append('email', email);

        let query = await this.client.get(`/newsletters?${params.toString()}`);
        return query.data;
    }
}

export default NewsletterService;
