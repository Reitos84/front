import { createStore, combineReducers } from "@reduxjs/toolkit";
import {persistReducer, persistStore} from "redux-persist";
import storage from "redux-persist/lib/storage";
import MainReducer from "../reducer/mainSlice";
import AuthSlice from "../reducer/authSlice";

const persistConfig = {
    key: 'root',
    storage,
};

const combinerReducers = combineReducers({
    main: MainReducer,
    auth: AuthSlice
});

const persistedReducer = persistReducer(persistConfig, combinerReducers);

let store =  createStore(
    persistedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
let persistor = persistStore(store);
export {store, persistor};