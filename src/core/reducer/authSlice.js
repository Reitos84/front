import {createSlice} from "@reduxjs/toolkit";

export const authSlice = createSlice({
    name: "auth",
    initialState: {
        data: null,
        token: null,
        tokenRevalidate: null
    },
    reducers: {
        pushToken: (state, data) => {
            state.token = data.payload.token;
            state.tokenRevalidate = data.payload.revalidator;
        },
        login: (state, data) => {
            state.data = data.payload;
        },
        updateInfos: (state, data) => {
            state.data = {...data.payload};
        },
        logout: (state) => {
            state.data = null;
            state.token = null;
            state.tokenRevalidate = null;
        }
    }
});

export const { login, logout, pushToken, updateInfos } = authSlice.actions;
export default authSlice.reducer;