import { createSlice } from "@reduxjs/toolkit";
export const mainSlice = createSlice({
    name: 'main',
    initialState: {
        value1: null,
        accountMessage: null
    },
    reducers: {
        assignAccountMessage: (state, data) => {
            state.accountMessage = data.payload;
        },
        removeAccountMessage: (state) => {
            state.accountMessage = null;
        }
    }
})

export const { assignAccountMessage, removeAccountMessage } = mainSlice.actions
export default mainSlice.reducer;