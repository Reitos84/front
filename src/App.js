import React from 'react';
import './App.css';
import {BrowserRouter as Router}from 'react-router-dom';
import RouterSwitch from './view/router/RouterSwitch';
import RequestInfo from './view/components/RequestInfo';

function App() {
    return (
        <>
            <RequestInfo />
            <Router forceRefresh={true}>
                <RouterSwitch />
            </Router>
        </>

    );
}

export default App;
